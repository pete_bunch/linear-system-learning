\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{IEEEtrantools}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{bibentry}
\usepackage{graphicx}
\usepackage[caption=false,font=footnotesize]{subfig} \captionsetup[subfloat]{labelformat=empty}
\nobibliography*


%%% BEAMER SET UP %%%
\setbeamertemplate{frametitle}
  {\begin{centering}\smallskip
   \insertframetitle\par
   \smallskip\end{centering}}
\setbeamertemplate{itemize item}{$\bullet$}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \scriptsize\sf\color{black!60}%
        \quad\insertframenumber
    }%
    \hfill
}

% Define some colors:
\definecolor{DarkFern}{HTML}{407428}
\definecolor{DarkCharcoal}{HTML}{4D4944}
\colorlet{Fern}{DarkFern!85!white}
\colorlet{Charcoal}{DarkCharcoal!85!white}
\colorlet{LightCharcoal}{Charcoal!50!white}
\colorlet{AlertColor}{orange!80!black}
\colorlet{DarkRed}{red!70!black}
\colorlet{DarkBlue}{blue!70!black}
\colorlet{DarkGreen}{green!70!black}

% Use the colors:
\setbeamercolor{title}{fg=Fern}
\setbeamercolor{frametitle}{fg=Fern}
\setbeamercolor{normal text}{fg=Charcoal}
\setbeamercolor{block title}{fg=black,bg=Fern!25!white}
\setbeamercolor{block body}{fg=black,bg=Fern!25!white}
\setbeamercolor{alerted text}{fg=AlertColor}
\setbeamercolor{itemize item}{fg=Charcoal}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Macros

\newcommand{\ti}{n}
\newcommand{\timax}{N}

\newcommand{\lsd}{d}
\newcommand{\ls}[1]{x_{#1}}
\newcommand{\ob}[1]{y_{#1}}
\newcommand{\tn}[1]{u_{#1}}
\newcommand{\on}[1]{v_{#1}}

\newcommand{\lgmtm}{A}
\newcommand{\lgmom}{C}
\newcommand{\lgmtvr}{Q}
\newcommand{\lgmovr}{R}
\newcommand{\spindm}{B}
\newcommand{\lgmtnm}{G}

\newcommand{\lmtm}[1]{F_{#1}}
\newcommand{\lmom}[1]{H_{#1}}
\newcommand{\lmtvr}[1]{Q_{#1}}
\newcommand{\lmovr}[1]{R_{#1}}

\newcommand{\den}{p}
\newcommand{\tden}{\pi}
\newcommand{\normalden}[3]{\mathcal{N}\left(#1\middle|#2,#3\right)}
\newcommand{\bernoullidist}[2]{\mathcal{BER}\left(#1\middle|#2\right)}
\newcommand{\dirac}[2]{\delta_{#1}(#2)}
\DeclareMathOperator{\vectorise}{Vec}
\DeclareMathOperator{\trace}{Tr}
\newcommand{\half}{\frac{1}{2}}
\newcommand{\reals}{\mathbb{R}}
\newcommand{\naturals}{\mathbb{N}}

\newcommand{\param}[1]{\theta_{#1}}
\newcommand{\tmvec}{a}
\newcommand{\spvec}{b}
\newcommand{\lsmat}[1]{X_{#1}}
\newcommand{\lsmata}[1]{X_{a,#1}}
\newcommand{\lsmatb}[1]{X_{b,#1}}
\newcommand{\tmvecpriormn}{m_{a,0}}
\newcommand{\tmvecpriorvr}{P_{a,0}}
\newcommand{\tmvecmn}{m_{a}}
\newcommand{\tmvecvr}{P_{a}}
\newcommand{\aprec}{\Omega_a}
\newcommand{\ainf}{\lambda_a}
\newcommand{\bprec}[1][]{\Omega_{b,#1}}
\newcommand{\binf}[1][]{\lambda_{b,#1}}
\newcommand{\spprior}{\phi}

\newcommand{\cpseq}{\Xi}
\newcommand{\cpt}[1]{\tau_{#1}}
\newcommand{\cpi}{k}
\newcommand{\numspan}{K}
\newcommand{\ccp}[1]{\kappa(#1)}
\newcommand{\fidx}[1]{{f}_{#1}}
\newcommand{\lidx}[1]{{l}_{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[Structured Linear Systems]{Bayesian Learning of Structured Linear Systems}
\author[P. Bunch]{Pete Bunch}
\institute[CUED SigProC]{Cambridge University Engineering Department\\ Signal Processing \& Laboratory}
\date{20th October 2014}



\begin{document}

\begin{frame}
 \titlepage 
\end{frame}

\begin{frame}{The Plan}
 \begin{itemize}
  \item Plain old linear systems
  \item Gibbs sampling
  \item Sparsity
  \item Changepoints
  \item Degeneracy
 \end{itemize}
\end{frame}

\begin{frame}{Linear Systems}
 \begin{IEEEeqnarray*}{rCl}
  \ls{\ti} & = & \lgmtm \ls{\ti-1} + \tn{\ti} \\
  \ob{\ti} & = & \lgmom \ls{\ti}   + \on{\ti}      .
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rCl}
  \tn{\ti} & \sim & \normalden{\tn{\ti}}{0}{\lgmtvr} \\
  \on{\ti} & \sim & \normalden{\on{\ti}}{0}{\lgmovr}
 \end{IEEEeqnarray*}
 \vspace{0cm}
 \begin{itemize}
  \item Focus on learning transition model: $\lgmtm$ and $\lgmtvr$.
  \item No parametric assumptions.
 \end{itemize}
\end{frame}

\begin{frame}{Gibbs Sampling}
 \begin{IEEEeqnarray*}{lCl}
 \text{Unknowns:}  & \quad & \ls{1:\timax}, \lgmtm, \lgmtvr \\
 \text{Posterior:} & \quad & \den(\ls{1:\timax},\lgmtm,\lgmtvr|\ob{1:\timax})
 \end{IEEEeqnarray*}
 \begin{itemize}
  \item Sample $\ls{1:\timax} \sim \den(\ls{1:\timax}|\lgmtm,\lgmtvr,\ob{1:\timax})$
  \item Sample $\lgmtm \sim \den(\lgmtm|\lgmtvr,\ls{1:\timax},\ob{1:\timax})$
  \item Sample $\lgmtvr \sim \den(\lgmtvr|\lgmtm,\ls{1:\timax},\ob{1:\timax})$
 \end{itemize}
 \hrule
 \bibentry{Wills2012}
\end{frame}

\begin{frame}{Gibbs Sampling -- States}
 \section{Sampling from $\den(\ls{1:\timax}|\lgmtm,\lgmtvr,\ob{1:\timax})$}
 \begin{IEEEeqnarray*}{rCl}
  \den(\ls{1:\timax}|\lgmtm,\lgmtvr,\ob{1:\timax}) & = & \prod_{\ti=1}^{\timax} \den(\ls{\ti}|\lgmtm,\lgmtvr,\ls{\ti+1},\ob{1:\ti})
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rCl}
  \den(\ls{\ti}|\lgmtm,\lgmtvr,\ls{\ti+1},\ob{1:\ti}) & \propto & \den(\ls{\ti}|\lgmtm,\lgmtvr,\ob{1:\ti}) \den(\ls{\ti+1}|\lgmtm,\lgmtvr,\ls{\ti})
 \end{IEEEeqnarray*}
 \begin{itemize}
  \item Standard Kalman filter
  \item Backward simulation
 \end{itemize}
\end{frame}

\begin{frame}{Gibbs Sampling -- Transition Matrix}
 \begin{IEEEeqnarray*}{rCl}
 \den(\lgmtm|\lgmtvr, \ls{1:\timax}, \ob{1:\timax}) & = & \den(\lgmtm|\lgmtvr, \ls{1:\timax}) \nonumber \\
  & \propto & \den(\ls{1:\timax}|\lgmtm, \lgmtvr) \den(\lgmtm)     .
\end{IEEEeqnarray*}
\begin{IEEEeqnarray*}{rCl}
 \den(\ls{1:\timax}|\lgmtm, \lgmtvr) & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lgmtm\ls{\ti-1})^T \lgmtvr^{-1} (\ls{\ti}-\lgmtm\ls{\ti-1}) \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lsmat{\ti-1}\tmvec)^T \lgmtvr^{-1} (\ls{\ti}-\lsmat{\ti-1}\tmvec) \right\} \nonumber \\
\end{IEEEeqnarray*}
\begin{IEEEeqnarray*}{rCcCl}
 \lsmat{\ti} & = & I \otimes \ls{\ti}^T & = & \begin{bmatrix} \ls{\ti}^T & 0 & \dots \\ 0 & \ls{\ti}^T & \dots \\ 0 & 0 & \ddots \end{bmatrix}     ,
\end{IEEEeqnarray*}
\begin{IEEEeqnarray*}{rCl}
 \tmvec & = & \vectorise(\lgmtm^T)     ,
\end{IEEEeqnarray*}
\end{frame}

\begin{frame}{Gibbs Sampling -- Covariance Matrix}
 \begin{IEEEeqnarray*}{rCl}
  \den(\lgmtvr|\lgmtm, \ls{1:\timax}, \ob{1:\timax}) & = & \den(\lgmtvr|\lgmtm, \ls{1:\timax}) \nonumber \\
   & \propto & \den(\ls{1:\timax}|\lgmtm, \lgmtvr) \den(\lgmtvr)     .
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rCl}
  \den(\ls{1:\timax}|\lgmtm, \lgmtvr) & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lgmtm\ls{\ti-1})^T \lgmtvr^{-1} (\ls{\ti}-\lgmtm\ls{\ti-1}) \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \trace\left[ \lgmtvr^{-1} \sum_{\ti} (\ls{\ti}-\lgmtm\ls{\ti-1})(\ls{\ti}-\lgmtm\ls{\ti-1})^T \right] \right\} \nonumber \\ 
 \end{IEEEeqnarray*}
\end{frame}

\begin{frame}{Some Results}
 \begin{figure}
 \centering
 \subfloat[$A$]{\includegraphics[width=0.4\columnwidth]{F.pdf}}\\
 \subfloat[$Q$]{\includegraphics[width=0.4\columnwidth]{Q.pdf}}
 \subfloat[$|Q|$]{\includegraphics[width=0.4\columnwidth]{detQ.pdf}}
 \end{figure}
\end{frame}

\begin{frame}{Sparse Transition Matrix}
  \begin{IEEEeqnarray*}{rCl}
  \ls{\ti} & = & (\lgmtm \otimes \spindm) \ls{\ti-1} + \tn{\ti} 
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rCl}
 \left[\spindm\right]_{i,j} & = & b_{i,j}
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rCl}
  \den(b_{i,j}) & = & \bernoullidist{b_{i,j}}{\spprior} \\
  & = & \prod_i \spprior^{b_{i,j}} (1-\spprior)^{1-b_{i,j}}
 \end{IEEEeqnarray*}
\end{frame}

\begin{frame}{Gibbs Sampling -- Sparse Transition Matrix}
 \begin{IEEEeqnarray*}{rCcCl}
  (\lgmtm \circ \spindm) \ls{\ti-1} & = & \lsmatb{\ti-1} \tmvec & = & \lsmata{\ti-1} \spvec
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rClCrCl}
  \lsmata{\ti} & = & I \otimes (\lgmtm \ls{\ti})^T & \qquad & \lsmatb{\ti} & = & I \otimes (\spindm \ls{\ti})^T
 \end{IEEEeqnarray*}
 \begin{IEEEeqnarray*}{rClCrCl}
  \tmvec & = & \vectorise(\lgmtm^T) & \qquad \spvec & = & \vectorise(\spindm^T) 
 \end{IEEEeqnarray*}
\begin{IEEEeqnarray*}{l}
 \den(\spvec_{i}|\spvec_{-i},\tmvec,\ls{1:\timax}) \propto \\
  \qquad \begin{cases}
   \spprior \exp\left\{ -\half \left[ 2 \sum_{j \ne i} \spvec_{j} \bprec[ij] + \bprec[ii] - 2 \binf[i] \right] \right\} & \spvec_{i}=1 \\
   (1-\spprior) & \spvec_{i}=0
  \end{cases}     
\end{IEEEeqnarray*}
$\bprec$ and $\binf$ are sufficient statistics.
\end{frame}

\begin{frame}{Some Results}
 \begin{figure}
 \centering
 \subfloat[$A$]{\includegraphics[width=0.4\columnwidth]{sparseF.pdf}}
 \subfloat[$B$]{\includegraphics[width=0.4\columnwidth]{sparseB.pdf}}\\
 \subfloat[$Q$]{\includegraphics[width=0.4\columnwidth]{sparseQ.pdf}}
 \subfloat[$|Q|$]{\includegraphics[width=0.4\columnwidth]{sparsedetQ.pdf}}
 \end{figure}
\end{frame}

\begin{frame}{Changepoints}
\begin{itemize}
 \item Set of changepoints: $\cpseq = \{ \cpt{1}, \cpt{2}, \dots, \cpt{\numspan-1} \}$
 \item Separate linear system model in each span
 \item Reversible jump MCMC to handle changes in number of changepoints
 \item Acceptance probability falls with state dimension.
 \item Solution: Annealed importance sampling reversible jump MCMC (AIS-RJ-MCMC) {\tiny $\leftarrow$ what a mouthfull!}
 \item Requires an ``overlapping model'' Kalman filter
\end{itemize}
\hrule
\bibentry{Karagiannis2013}
\end{frame}

\begin{frame}{Degeneracy}
 \begin{IEEEeqnarray*}{rCl}
  \ls{\ti} & = & \lgmtm \ls{\ti-1} + \lgmtnm \tn{\ti} \\
  \tn{\ti} & \sim & \normalden{\tn{\ti}}{0}{I} \\
 \end{IEEEeqnarray*}
 \begin{itemize}
  \item $\lgmtnm$ not full rank $\Rightarrow$ $\lgmtvr=\lgmtnm\lgmtnm^T$ not invertible.
  \item Sample $\lgmtnm$ directly. Gaussian prior.
  \item Gibbs sampler failure -- too much correlation
  \item Use pseudo-observations to facilitate mixing
  \item Add ``noise'' to state sequence. Needs to be filtered so analytic sampling still possible.
 \end{itemize}
 \hrule
 \bibentry{Fearnhead2014} 
\end{frame}

\begin{frame}{Summary}
 \begin{itemize}
  \item Bayesian learning of basic linear systems easy using Gibbs sampling.
  \item Further structure can be accommodated: sparsity, degeneracy, changepoint.
  \item Other features to add: state dimension, rank, stability
 \end{itemize}
\end{frame}

\begin{frame}{}
 
\end{frame}


\bibliographystyle{alpha}
\nobibliography{/users/pete/Dropbox/PhD/Cleanbib}
\end{document}
