\documentclass{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{IEEEtrantools}
\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{hyperref}

\usepackage{subfig}

\usepackage{tikz}
\usepackage{pgfplots}
 \usetikzlibrary{plotmarks}
 \pgfplotsset{compat=newest}
 \pgfplotsset{plot coordinates/math parser=false}
 \usepgfplotslibrary{external}
 \tikzexternalize[prefix=tikz/]

\newcommand{\ti}{n}
\newcommand{\timax}{N}

\newcommand{\lsd}{d}
\newcommand{\ls}[1]{x_{#1}}
\newcommand{\ob}[1]{y_{#1}}
\newcommand{\tn}[1]{u_{#1}}
\newcommand{\on}[1]{v_{#1}}

\newcommand{\lgmtm}{A}
\newcommand{\lgmom}{C}
\newcommand{\lgmtvr}{Q}
\newcommand{\lgmovr}{R}
\newcommand{\spindm}{B}

\newcommand{\lmtm}[1]{F_{#1}}
\newcommand{\lmom}[1]{H_{#1}}
\newcommand{\lmtvr}[1]{Q_{#1}}
\newcommand{\lmovr}[1]{R_{#1}}




\newcommand{\den}{p}
\newcommand{\tden}{\pi}
\newcommand{\normalden}[3]{\mathcal{N}\left(#1\middle|#2,#3\right)}
\newcommand{\bernoullidist}[2]{\mathcal{BER}\left(#1\middle|#2\right)}
\newcommand{\dirac}[2]{\delta_{#1}(#2)}
\DeclareMathOperator{\vectorise}{Vec}
\newcommand{\half}{\frac{1}{2}}
\newcommand{\reals}{\mathbb{R}}
\newcommand{\naturals}{\mathbb{N}}

\newcommand{\param}[1]{\theta_{#1}}
\newcommand{\tmvec}{a}
\newcommand{\spvec}{b}
\newcommand{\lsmat}[1]{X_{#1}}
\newcommand{\lsmata}[1]{X_{a,#1}}
\newcommand{\lsmatb}[1]{X_{b,#1}}
\newcommand{\tmvecpriormn}{m_{a,0}}
\newcommand{\tmvecpriorvr}{P_{a,0}}
\newcommand{\tmvecmn}{m_{a}}
\newcommand{\tmvecvr}{P_{a}}
\newcommand{\aprec}{\Omega_a}
\newcommand{\ainf}{\lambda_a}
\newcommand{\bprec}[1][]{\Omega_{b,#1}}
\newcommand{\binf}[1][]{\lambda_{b,#1}}
\newcommand{\spprior}{\phi}

\newcommand{\cpseq}{\Xi}
\newcommand{\cpt}[1]{\tau_{#1}}
\newcommand{\cpi}{k}
\newcommand{\numspan}{K}
\newcommand{\ccp}[1]{\kappa(#1)}
\newcommand{\fidx}[1]{{f}_{#1}}
\newcommand{\lidx}[1]{{l}_{#1}}

\newcommand{\mcmckern}[1]{K_{#1}}
\newcommand{\mc}[1]{^{[#1]}}
\newcommand{\mcnew}{^*}
\newcommand{\ap}{\alpha}
\newcommand{\setto}{\leftarrow}
\newcommand{\impden}[1]{q_{#1}}

\newcommand{\at}{t}
\newcommand{\atmax}{T}
\newcommand{\anfact}[1]{\gamma_{#1}}
\newcommand{\antden}[1]{\rho_{#1}}
\newcommand{\ac}[1]{^{\{#1\}}}

\newcommand{\ankfprd}[1]{\hat{\eta}_{#1}}
\newcommand{\ankfupd}[1]{\eta_{#1}}
\newcommand{\ncprd}[1]{\hat{Z}_{#1}}
\newcommand{\ncupd}[1]{Z_{#1}}
\newcommand{\lsprdmn}[1]{\hat{m}_{#1}}
\newcommand{\lsprdvr}[1]{\hat{P}_{#1}}
\newcommand{\lsupdmn}[1]{m_{#1}}
\newcommand{\lsupdvr}[1]{P_{#1}}


\title{Gibbs Sampling for Sparse Linear Systems with Changepoints}
\author{Pete Bunch}
\date{April 2014}

%%% Environments %%%
\newenvironment{meta}[0]{\color{red} \em}{}

\begin{document}

\maketitle

\section{Introduction}
Motivated by inference of gene regulatory systems, we seek efficient methods to estimate a distribution over the structure of a linear state space model. Gibbs sampling provides an efficient mechanism to achieve this goal. We begin with a simple case --- learning the unknown transition matrix. We then extend this to learning the transition matrix subject to a number of unknown changepoints; both the times of the changepoints and the transition matrix for each section must be jointly learnt. This is a difficult problem, since changing the sequence of changepoints must also be accompanied by a corresponding change in the transition matrices. Additionally, we modify the transition matrix so as to have a sparse structure. This is achieved by associating a binary variable with each entry in the matrix indicating whether it is zero or not. By learning a posterior distribution over these indicator variables, we are able to assess directly the probability of each interaction being significant.

I think the material in sections 4, 5 and 6 is wholly novel.


\section{Linear State Space Models}
A linear state space model obeys the following system equations,
%
\begin{IEEEeqnarray}{rCl}
 \ls{\ti} & = & \lgmtm \ls{\ti-1} + \tn{\ti} \\
 \ob{\ti} & = & \lgmom \ls{\ti}   + \on{\ti}      .
\end{IEEEeqnarray}
%
In addition, we assume the state disturbance and observation noise variables have a Gaussian distribution,
%
\begin{IEEEeqnarray}{rCl}
 \tn{\ti} & \sim & \normalden{\tn{\ti}}{0}{\lgmtvr} \\
 \on{\ti} & \sim & \normalden{\on{\ti}}{0}{\lgmovr}
\end{IEEEeqnarray}
%
When the system parameters are known ($\param{} = \left\{\lgmtm, \lgmom, \lgmtvr, \lgmovr\right\}$), inference may be carried out exactly using a Kalman filter \cite{Kalman1960} and Rauch-Tung-Striebel (RTS) smoother \cite{Rauch1965}. Specifically, we can calculate $\den(\ls{1:\timax}|\ob{1:\timax}, \param{})$ analytically, evaluate the marginal likelihood, $\den(\ob{1:\timax}|\param{})$, and also sample from $\den(\ls{1:\timax}|\ob{1:\timax}, \param{})$ using the forward-filtering-backward-sampling method \cite{Chib1996}.

Our objective is to infer the transition matrix $\lgmtm$ from a sequence of observations $\ob{1:\timax}$, i.e. to calculate or estimate $\den(\lgmtm|\ob{1:\timax})$.


\section{Fixed Non-Sparse Transition Matrix}
First we assume that unknown transition matrix is constant over the period of time spanned by the observations, and that it is dense (i.e. terms are not expected to be exactly $0$). In addition, we assume a Gaussian prior distribution on the elements of the matrix. Although this may be achieved using the matrix normal distribution \cite{Wills2012}, it is equivalent to vectorise the matrix and then use an ordinary, familiar multivariate normal. We write,
%
\begin{IEEEeqnarray}{rCl}
 \tmvec & = & \vectorise(\lgmtm^T)     ,
\end{IEEEeqnarray}
%
and then the prior is,
%
\begin{IEEEeqnarray}{rCl}
 \den(\tmvec) & = & \normalden{\tmvec}{\tmvecpriormn}{\tmvecpriorvr}     .
\end{IEEEeqnarray}


\subsection{The Sampling Scheme}
We can use MCMC to generate samples from the desired posterior distribution. This is achieved by targeting the joint posterior $\den(\ls{1:\timax}, \lgmtm|\ob{1:\timax})$ using Gibbs sampling. The state conditional is sampled using the Kalman filter and backward sampling. The transition matrix conditional is calculated and sampled as follows,
%
\begin{IEEEeqnarray}{rCl}
 \den(\tmvec|\ls{1:\timax}, \ob{1:\timax}) & = & \den(\tmvec|\ls{1:\timax}) \nonumber \\
  & \propto & \den(\ls{1:\timax}|\tmvec) \den(\tmvec)     .
\end{IEEEeqnarray}
%
\begin{IEEEeqnarray}{rCl}
 \den(\ls{1:\timax}|\tmvec) & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lgmtm\ls{\ti-1})^T \lgmtvr^{-1} (\ls{\ti}-\lgmtm\ls{\ti-1}) \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lsmat{\ti-1}\tmvec)^T \lgmtvr^{-1} (\ls{\ti}-\lsmat{\ti-1}\tmvec) \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \left[ \tmvec^T \underbrace{\left(\sum_{\ti} \lsmat{\ti-1}^T\lgmtvr^{-1}\lsmat{\ti-1}\right)}_{\aprec} \tmvec - 2\tmvec^T \underbrace{\left(\sum_{\ti} \lsmat{\ti-1}^T\lgmtvr^{-1}\ls{\ti}\right)}_{\ainf} \right] \right\} \nonumber      ,
\end{IEEEeqnarray}
%
where
%
\begin{IEEEeqnarray}{rCl}
 \lsmat{\ti} & = & I \otimes \ls{\ti}^T \nonumber \\
  & = & \begin{bmatrix} \ls{\ti}^T & 0 & \dots \\ 0 & \ls{\ti}^T & \dots \\ 0 & 0 & \ddots \end{bmatrix}     ,
\end{IEEEeqnarray}
%
where $\otimes$ is the Kronecker product, such that,
%
\begin{IEEEeqnarray}{rCl}
 \lgmtm\ls{\ti-1} & = & \lsmat{\ti-1}\tmvec     .
\end{IEEEeqnarray}

Let's just check this with a $2\times2$ example...
%
\begin{IEEEeqnarray}{rCl}
 A x & = & \begin{bmatrix} a_{11} & a_{12} \\ a_{21} & a_{22} \end{bmatrix} \begin{bmatrix} x_{1} \\ x_{2} \end{bmatrix} \nonumber \\
     & = & \begin{bmatrix} a_{11} x_{1} + a_{12} x_{2} \\ a_{21} x_{1} + a_{22} x_{2} \end{bmatrix} \nonumber \\
     & = & \begin{bmatrix} x_{1} & x_{2} & 0 & 0 \\ 0 & 0 & x_{1} & x_{2} \end{bmatrix} \begin{bmatrix} a_{11} \\ a_{12} \\ a_{21} \\ a_{22} \end{bmatrix} \nonumber \\
     & = & X a     .
\end{IEEEeqnarray}

The matrix $\aprec$ may seem daunting. If $\lgmtm$ is a $\lsd\times\lsd$ matrix, then $\aprec$ will be $\lsd^2\times\lsd^2$. However, because of the block-diagonal structure of $\lsmat{\ti}$, $\aprec$ will itself be block-diagonal, meaning that efficient methods for storage and calculation may be used. The computational are memory requirements are no more onerous than if we worked with matrix normal distributions as in \cite{Wills2012}.

Finally, the transition matrix conditional distribution is thus Gaussian,
%
\begin{IEEEeqnarray}{rCl}
 \den(\tmvec|\ls{1:\timax}) & = & \normalden{\tmvec}{\tmvecmn}{\tmvecvr} \\
 \tmvecvr & = & \left( \tmvecpriorvr^{-1} + \aprec \right)^{-1} \\
 \tmvecmn & = & \tmvecvr \left( \ainf + \tmvecpriorvr^{-1}\tmvecpriormn \right)     .
\end{IEEEeqnarray}

A Gibbs sampler may now be run, drawing alternately from $\den(\ls{1:\timax}, \tmvec|\ob{1:\timax}$ and $\den(\tmvec|\ls{1:\timax}, \ob{1:\timax})$. Discarding the state values leaves us with a sample approximation to the desired posterior, $\den(\tmvec|\ob{1:\timax})$.

We can view two consecutive steps of the Gibbs sampler as a single MCMC kernel $\tmvec\mc{m} \sim \mcmckern{}(\cdot|\tmvec\mc{m-1})$ with invariant distribution $\den(\tmvec|\ob{1:\timax})$. This will be a useful interpretation as a component of sampling schemes for more complex models.


\section{Fixed Sparse Transition Matrix}
In many real systems, the transition matrix is likely to be \emph{sparse}, i.e. a number of the entries are exactly $0$. This structure may be built into the model by representing the transition matrix as the Hadamard (entrywise) product of two matrices, $\lgmtm \circ \spindm$, where the entries of $\lgmtm$ are real numbers, as before, and the entries of $\spindm$ are binary numbers indicating the relevance of each entry. The sampling scheme can then be extended to infer both $\lgmtm$ and $\spindm$. First introduce an equivalently vectorised form for $\spindm$,
%
\begin{IEEEeqnarray}{rCl}
 \spvec & = & \vectorise(\spindm^T)      .
\end{IEEEeqnarray}
%
Using this note,
%
\begin{IEEEeqnarray}{rCl}
 (\lgmtm \circ \spindm) \ls{\ti-1} & = & \lsmatb{\ti-1} \tmvec  \\
                                   & = & \lsmata{\ti-1} \spvec       ,
\end{IEEEeqnarray}
%
where,
%
\begin{IEEEeqnarray}{rCl}
 \lsmata{\ti} & = & I \otimes (\lgmtm \ls{\ti})^T \\
 \lsmatb{\ti} & = & I \otimes (\spindm \ls{\ti})^T      .
\end{IEEEeqnarray}

Again, lets check these for the $2\times2$ case,
%
\begin{IEEEeqnarray}{rCl}
 (A \circ B) x & = & \begin{bmatrix} a_{11} b_{11} & a_{12} b_{12} \\ a_{21} b_{21} & a_{22} b_{22} \end{bmatrix} \begin{bmatrix} x_{1} \\ x_{2} \end{bmatrix} \nonumber \\
     & = & \begin{bmatrix} a_{11} b_{11} x_{1} + a_{12} b_{12} x_{2} \\ a_{21} b_{21} x_{1} + a_{22} b_{22} x_{2} \end{bmatrix} \nonumber \\
     & = & \begin{bmatrix} b_{11} x_{1} & b_{12} x_{2} & 0 & 0 \\ 0 & 0 & b_{21} x_{1} & b_{22} x_{2} \end{bmatrix} \begin{bmatrix} a_{11} \\ a_{12} \\ a_{21} \\ a_{22} \end{bmatrix} \nonumber \\
     & = & X_b a \nonumber \\
     & = & \begin{bmatrix} a_{11} x_{1} & a_{12} x_{2} & 0 & 0 \\ 0 & 0 & a_{21} x_{1} & a_{22} x_{2} \end{bmatrix} \begin{bmatrix} b_{11} \\ b_{12} \\ b_{21} \\ b_{22} \end{bmatrix} \nonumber \\
     & = & X_a b \nonumber      .
\end{IEEEeqnarray}

As before, we use a Gaussian conjugate prior for the $\tmvec$. For $\spvec$, we introduce an independent Bernoulli prior on each element,
%
\begin{IEEEeqnarray}{rCl}
 \den(\spvec) & = & \prod_i \bernoullidist{\spvec_{i}}{\spprior} \nonumber \\
              & = & \prod_i \spprior^{\spvec_{i}} (1-\spprior)^{1-\spvec_{i}}     .
\end{IEEEeqnarray}


\subsection{The Sampling Scheme}
Sampling $\den(\tmvec|\spvec,\ls{1:\timax})$ follows exactly as before, but with $\lsmat{\ti}$ replaced by $\lsmatb{\ti}$ throughout. The sparsity indicator conditional is calculated and sampled as follows,
%
\begin{IEEEeqnarray}{rCl}
 \den(\spvec|\tmvec,\ls{1:\timax}, \ob{1:\timax}) & = & \den(\spvec|\tmvec,\ls{1:\timax}) \nonumber \\
  & \propto & \den(\ls{1:\timax}|\tmvec,\spvec) \den(\spvec)     .
\end{IEEEeqnarray}
%
\begin{IEEEeqnarray}{rCl}
 \den(\ls{1:\timax}|\tmvec,\spvec) & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lgmtm\ls{\ti-1})^T \lgmtvr^{-1} (\ls{\ti}-\lgmtm\ls{\ti-1}) \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \sum_{\ti} (\ls{\ti}-\lsmata{\ti-1}\spvec)^T \lgmtvr^{-1} (\ls{\ti}-\lsmata{\ti-1}\spvec) \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \left[ \spvec^T \underbrace{\left(\sum_{\ti} \lsmata{\ti-1}^T\lgmtvr^{-1}\lsmata{\ti-1}\right)}_{\bprec} \spvec - 2\spvec^T \underbrace{\left(\sum_{\ti} \lsmata{\ti-1}^T\lgmtvr^{-1}\ls{\ti}\right)}_{\binf} \right] \right\} \nonumber \\
  & \propto & \exp\left\{ -\half \left[ \sum_{i,j} \spvec_{i}\spvec_{j}\bprec[ij] - 2 \sum_i \spvec_{i}\binf[i] \right] \right\}      .
\end{IEEEeqnarray}
%
If we write $\spvec_{-i}$ for the elements of $\spvec$ excluding $\spvec_{i}$, then,
%
\begin{IEEEeqnarray}{rCl}
 \den(\spvec_{i}|\spvec_{-i},\tmvec,\ls{1:\timax}) & \propto & 
  \begin{cases}
   \spprior \exp\left\{ -\half \left[ 2 \sum_{j \ne i} \spvec_{j} \bprec[ij] + \bprec[ii] - 2 \binf[i] \right] \right\} & \spvec_{i}=1 \\
   (1-\spprior) & \spvec_{i}=0
  \end{cases}     .
\end{IEEEeqnarray}
%
For efficient calculation, note that,
%
\begin{IEEEeqnarray}{rCl}
 2 \sum_{j \ne i} \spvec_{j} \bprec[ij] + \bprec[ii] & = & 2 \bprec[i] \cdot \hat{\spvec} \nonumber      ,
\end{IEEEeqnarray}
%
where $\bprec[i]$ is the ($i$)th column (or row) of $\bprec$, and $\hat{\spvec}$ is $\spvec$ with the ($i$)th element replaced by $\half$. We can perform Gibbs sampling for $\spvec$ by sampling the elements in turn from this conditional. This could done in a fixed or a random order.

Note that when an element of $\spvec$ is set to $0$, then the corresponding element in $\lgmtm$ is sampled from its prior.



\section{Linear Systems with Changepoints}
Next we consider a more complex scenario, where the parameters of the linear system change and a number of instantaneous changepoint times,
%
\begin{IEEEeqnarray}{rCl}
 \cpseq & = & \{ \cpt{1}, \cpt{2}, \dots, \cpt{\numspan-1} \} \nonumber       ,
\end{IEEEeqnarray}
%
where $\cpt{m}\in\naturals$ and $1 < \cpt{1} < \cpt{2} < \dots < \cpt{\numspan-1} < \timax+1$. For notational convenience we write $\cpt{0}=1$ and $\cpt{\numspan}=\timax+1$. Note that $\numspan$ is the number of spans between changepoints, and is one greater than the number of changepoints themselves. This is also unknown. We have now to estimate both the times of these changepoints as well as the system parameters for each intervening span. For $\cpt{\cpi-1}\leq\ti<\cpt{\cpi}$, the system parameters are $\param{\cpi}=\{\lmtm{\cpi},\lmtvr{\cpi},\lmom{\cpi},\lmovr{\cpi}\}$, and we write the entire set of models as,
%
\begin{IEEEeqnarray}{rCl}
 \param{} & = & \{\param{1}, \param{2}, \dots, \param{\numspan}\} \nonumber      .
\end{IEEEeqnarray}

We assume an independent prior for the parameters in each span. The posterior distribution which we would like to estimate is,
%
\begin{IEEEeqnarray}{rCcCl}
 \tden(\cpseq, \param{}) & = & \den(\cpseq, \param{} | \ob{1:\timax}) & = & \den(\cpseq|\ob{1:\timax}) \prod_{\cpi=1}^{\numspan} \den(\param{\cpi}|\ob{\cpt{\cpi-1}:\cpt{\cpi}-1}) \nonumber     .
\end{IEEEeqnarray}
%
We can target this using MCMC. In particular, since the number of changepoints is unknown, we will need to use reversible jump MCMC (RJMCMC) \cite{Green1995}. Because of the conditional independence, we can use all of the efficient sampling operations introduced in earlier sections to target $\den(\param{\cpi}|\ob{\cpt{\cpi-1}:\cpt{\cpi}-1})$. However, efficient MCMC moves for altering the changepoint sequence are more challenging to formulate.

There are two simple Metropolis-Hastings schemes which could be applied to this problem in order to update $\cpseq$ (and thus implicitly $\numspan$). These are defined in algorithms~\ref{alg:mwg} and~\ref{alg:jmh}.

\begin{algorithm}
\begin{algorithmic}[1]
 \REQUIRE Current state of the chain $\left\{ \cpseq\mc{m}, \param{}\mc{m} \right\}$.
 \STATE Propose a new value $\cpseq\mcnew \sim \impden{\cpseq}(\cpseq|\param{}\mc{m},\cpseq\mc{m})$.
 \STATE Calculate acceptance probability\\ $\ap = \min\left\{1, \frac{ \tden(\cpseq\mcnew,\param{}\mc{m}) }{ \tden(\cpseq\mc{m},\param{}\mc{m}) } \frac{ \impden{\cpseq}(\cpseq\mc{m}|\param{}\mc{m},\cpseq\mcnew) }{ \impden{\cpseq}(\cpseq\mcnew|\param{}\mc{m},\cpseq\mc{m}) } \right\}$.
 \STATE With probability $\ap$ set $\cpseq\mc{m+1}\setto\cpseq\mcnew$.\\ Otherwise set $\cpseq\mc{m+1}\setto\cpseq\mc{m}$.
 \STATE Set $\param{}\mc{m+1}\setto\param{}\mc{m}$.
 \RETURN New state of the chain $\left\{ \cpseq\mc{m+1}, \param{}\mc{m+1} \right\}$.
\end{algorithmic}
\caption{Metropolis-within-Gibbs.}
\label{alg:mwg}
\end{algorithm}

\begin{algorithm}
\begin{algorithmic}[1]
 \REQUIRE Current state of the chain $\left\{ \cpseq\mc{m}, \param{}\mc{m} \right\}$.
 \STATE Propose a new value $\cpseq\mcnew \sim \impden{\cpseq}(\cpseq|\cpseq\mc{m})$.
 \STATE Propose a new value $\param{}\mcnew \sim \impden{\param{}}(\param{}|\cpseq\mcnew)$.
 \STATE Calculate acceptance probability\\ $\ap = \min\left\{1, \frac{ \tden(\cpseq\mcnew,\param{}\mcnew) }{ \tden(\cpseq\mc{m},\param{}\mc{m}) } \frac{ \impden{\param{}}(\param{}\mc{m}|\cpseq\mc{m}) \impden{\cpseq}(\cpseq\mc{m}|\cpseq\mcnew) }{ \impden{\param{}}(\param{}\mcnew|\cpseq\mcnew) \impden{\cpseq}(\cpseq\mcnew|\cpseq\mc{m}) } \right\}$.
 \STATE With probability $\ap$ set $\cpseq\mc{m+1}\setto\cpseq\mcnew$ and $\param{}\mc{m+1}\setto\param{}\mcnew$.\\ Otherwise set $\cpseq\mc{m+1}\setto\cpseq\mc{m}$ and $\param{}\mc{m+1}\setto\param{}\mc{m}$.
 \RETURN New state of the chain $\left\{ \cpseq\mc{m+1}, \param{}\mc{m+1} \right\}$.
\end{algorithmic}
\caption{Joint Metropolis-Hastings.}
\label{alg:jmh}
\end{algorithm}

We could use \emph{Metropolis-within-Gibbs}, in which $\param{}$ is left unchanged. The problem with this scheme is that the fixed value of $\param{}$ may be poorly matched to the newly proposed value of $\cpseq$, in which case the acceptance rate will be low unless $\impden{\cpseq}(\cpseq|\cpseq\mc{m})$ is tightly concentrated around $\cpseq\mc{m}$. This means that we can only make small adjustments to the changepoint sequence, and the mixing of the Markov chain is likely to be poor.

Alternatively we could use \emph{Joint Metropolis-Hastings}, in which a new value is proposed simultaneously for $\param{}$. For the models we consider, there is no simple and sufficiently close approximation of $\tden(\param{}|\cpseq)$ which can be used for $\impden{\param{}}(\param{}|\cpseq)$, and hence the acceptance rates are again low. The frustrating thing here is that we can sample from a very efficient Markov kernel $\mcmckern{}(\cdot|\param{},\cpseq{})$ with invariant distribution $\tden(\param{}|\cpseq)$ (by first sampling a state sequence, then sampling a new set of model parameters conditional on that sequence), but there is no density associated with this kernel and hence $\ap$ cannot be evaluated.

\subsection{Annealed Metropolis Hastings}

One solution which allows us to exploit the efficiency of our Markov kernel for $\param{}$ is to use annealed importance sampling RJMCMC (AISRJ) \cite{Karagiannis2013}. The principle is to  define a smooth sequence of distributions which take us from the old to the new conditional distribution for $\param{}$. These can be targeted using similarly annealed versions of $\mcmckern{}(\cdot|\param{},\cpseq{})$, allowing us to gradually transition to the new changepoint sequence. The resulting algorithm yields tractable acceptance probabilities, and has the attractive property that in the limit as the number of annealing steps becomes large, the behaviour approaches that of the optimal Markov chain on the marginal $\tden(\cpseq)$.

Although the algorithm presented here is a particular case of the general framework devised by \cite{Karagiannis2013}, we investigate particular efficient choices for the various parameters, namely the annealing sequence and the associated Markov kernels, and derive appropriate sampling mechanisms.

First define the sequence of annealed conditional target distributions for $\at \in [0,\atmax]$,
%
\begin{IEEEeqnarray}{rCl}
 \antden{\at}(\param{}|\cpseq,\cpseq\mcnew) & \propto & \int \tden(\ls{1:\timax}, \param{}, \cpseq)^{1-\anfact{\at}} \tden(\ls{1:\timax}, \param{} \cpseq\mcnew)^{\anfact{\at}} d\ls{1:\timax} \nonumber     ,
\end{IEEEeqnarray}
%
where, $\tden(\ls{1:\timax}, \param{}, \cpseq)$ is the joint posterior including the state sequence as well, $\{\anfact{\at}\}$ is an increasing sequence with $\anfact{0}=0$ and $\anfact{\atmax}=1$. For example, we could use,
%
\begin{IEEEeqnarray}{rCl}
 \anfact{\at} & = & \frac{\at}{\atmax} \nonumber      .
\end{IEEEeqnarray}
%
Let $\mcmckern{\at}(\cdot|\param{},\cpseq{},\cpseq{}\mcnew)$ be an MCMC kernel which targets $\antden{\at}$, such that,
%
\begin{IEEEeqnarray}{rCl}
 \antden{\at}(\param{}|\cpseq,\cpseq\mcnew) \mcmckern{\at}(\param{}\mcnew|\param{},\cpseq{},\cpseq{}\mcnew) & = & \antden{\at}(\param{}\mcnew|\cpseq,\cpseq\mcnew) \mcmckern{\at}(\param{}|\param{}\mcnew,\cpseq{},\cpseq{}\mcnew) \nonumber     .
\end{IEEEeqnarray}
%
The resulting procedure for an AISRJ move is shown in algorithm~\ref{alg:aisrj}. To prove detailed balance it is necessary that,
%
\begin{IEEEeqnarray}{c}
 \tden(\cpseq,\param{}) \impden{\cpseq}(\cpseq\mcnew|\cpseq) \prod_{\at=1}^{\atmax} \mcmckern{\at}(\param{}\ac{\at}|\param{}\ac{\at-1},\cpseq,\cpseq\mcnew) \ap(\param{}\to\param{}\mcnew) \nonumber \\
 \qquad = \tden(\cpseq\mcnew,\param{}\mcnew) \impden{\cpseq}(\cpseq|\cpseq\mcnew) \prod_{\at=1}^{\atmax} \mcmckern{\at}(\param{}\ac{\at-1}|\param{}\ac{\at},\cpseq,\cpseq\mcnew) \ap(\param{}\mcnew\to\param{})     ,
\end{IEEEeqnarray}
%
which is straightforward, and a special case of the proof presented by \cite{Karagiannis2013}.

\begin{algorithm}
\begin{algorithmic}[1]
 \REQUIRE Current state of the chain $\left\{ \cpseq\mc{m}, \param{}\mc{m} \right\}$.
 \STATE Propose a new value $\cpseq\mcnew \sim \impden{\cpseq}(\cpseq|\cpseq\mc{m})$.
 \STATE Set $\param{}\ac{0}\setto\param{}\mc{m}$.
 \FOR{$\at=1$ \TO $\atmax$}
  \STATE Sample $\param{}\ac{\at}\sim\mcmckern{\at}(\param{}|\param{}\ac{\at-1},\cpseq\mc{m},\cpseq\mcnew)$.
 \ENDFOR
 \STATE Set $\param{}\mcnew\setto\param{}\ac{\atmax}$.
 \STATE Calculate acceptance probability\\ $\ap(\param{}\to\param{}\mcnew) = \min\left\{1, \frac{ \tden(\cpseq\mcnew,\param{}\mcnew) }{ \tden(\cpseq\mc{m},\param{}\mc{m}) } \frac{ \impden{\cpseq}(\cpseq\mc{m}|\cpseq\mcnew) }{ \impden{\cpseq}(\cpseq\mcnew|\cpseq\mc{m}) } \times \prod_{\at=1}^{\atmax} \frac{\antden{\at}(\param{}\ac{\at-1}|\cpseq,\cpseq\mcnew)}{\antden{\at}(\param{}\ac{\at}|\cpseq,\cpseq\mcnew)} \right\}$.
 \STATE With probability $\ap(\param{}\to\param{}\mcnew)$ set $\cpseq\mc{m+1}\setto\cpseq\mcnew$ and $\param{}\mc{m+1}\setto\param{}\mcnew$.\\ Otherwise set $\cpseq\mc{m+1}\setto\cpseq\mc{m}$ and $\param{}\mc{m+1}\setto\param{}\mc{m}$.
 \RETURN New state of the chain $\left\{ \cpseq\mc{m+1}, \param{}\mc{m+1} \right\}$.
\end{algorithmic}
\caption{Annealed Reversible Jump Metropolis-Hastings.}
\label{alg:aisrj}
\end{algorithm}

\subsection{Annealed Sequences}
To implement the algorithm described in the previous section, we must be able to evaluate $\antden{\at}$ and sample from $\mcmckern{\at}$. In this section we show how this may be achieved using modified Kalman filter equations. The joint posterior over states, parameters and the changepoint sequence can be expanded using Bayes rule as,
%
\begin{IEEEeqnarray}{rCl}
 \tden(\ls{1:\timax}, \param{}, \cpseq) & = & \den(\ls{1:\timax}, \param{}, \cpseq | \ob{1:\timax}) \nonumber \\
 & \propto & \den(\cpseq) \prod_{\cpi=1}^{\numspan} \den(\param{\cpi}|\cpseq) \prod_{\ti=1}^{\timax} \den(\ob{\ti}|\ls{\ti},\param{\ccp{\ti}}) \den(\ls{\ti}|\ls{\ti-1}, \param{\ccp{\ti}}) \nonumber     ,
\end{IEEEeqnarray}
%
where $\ccp{\ti} = \max\{\cpi : \cpt{\cpi-1} \leq \ti\}$ is the index of the model in effect at time instant $\ti$. Hence, the sequence of annealing distributions are,
%
\begin{IEEEeqnarray}{rCl}
 \antden{\at}(\param{}|\cpseq,\cpseq\mcnew) & \propto & \int \tden(\ls{1:\timax}, \param{} | \cpseq)^{1-\anfact{\at}} \tden(\ls{1:\timax}, \param{} | \cpseq\mcnew)^{\anfact{\at}} d\ls{1:\timax} \nonumber \\
 & \propto & \prod_{\cpi=1}^{\numspan} \den(\param{\cpi}|\cpseq)^{1-\anfact{\at}} \den(\param{\cpi}|\cpseq\mcnew)^{\anfact{\at}} \prod_{\ti=1}^{\timax} \int \den(\ob{\ti}|\ls{\ti},\param{\ccp{\ti}})^{1-\anfact{\at}} \den(\ls{\ti}|\ls{\ti-1}, \param{\ccp{\ti}})^{1-\anfact{\at}} \nonumber \\
 & & \qquad \qquad \qquad \times \den(\ob{\ti}|\ls{\ti},\param{\ccp{\ti}\mcnew})^{\anfact{\at}} \den(\ls{\ti}|\ls{\ti-1}, \param{\ccp{\ti}\mcnew})^{\anfact{\at}} d\ls{\ti} \nonumber     .
\end{IEEEeqnarray}
%
The integral may be evaluated using a recursion akin to a Kalman filter. Define,
%
\begin{IEEEeqnarray}{rCl}
 \ankfprd{\ti}(\ls{\ti}) & = & \ncprd{\ti} \normalden{\ls{\ti}}{\lsprdmn{\ti}}{\lsprdvr{\ti}} \nonumber \\
 \ankfupd{\ti}(\ls{\ti}) & = & \ncupd{\ti} \normalden{\ls{\ti}}{\lsupdmn{\ti}}{\lsupdvr{\ti}} \nonumber      .
\end{IEEEeqnarray}

{\meta Work out the recursions for the full model.}



, where the transition density is proportional to $\den(\ls{\ti}|\ls{\ti-1}, \param{\ccp{\ti}})^{1-\anfact{\at}} \den(\ls{\ti}|\ls{\ti-1}, \param{\ccp{\ti}\mcnew})^{\anfact{\at}}$, and the likelihood to $\den(\ob{\ti}|\ls{\ti},\param{\ccp{\ti}})^{1-\anfact{\at}} \den(\ob{\ti}|\ls{\ti},\param{\ccp{\ti}\mcnew})^{\anfact{\at}}$.










% \newcommand{\tden}{\pi}
% \newcommand{\etden}{\tilde{\pi}}
% \newcommand{\pden}[1]{q_{#1}}
% \newcommand{\aprden}{\eta}
% \newcommand{\lhard}{\theta}
% \newcommand{\leasy}{x}
% \newcommand{\laux}{z}
% \newcommand{\uhard}{\Theta}
% \newcommand{\ueasy}{X}
% \newcommand{\uaux}{Z}
% \newcommand{\nc}{C}

% \section{MCMC proposals within MCMC}
% In this section we introduce an MCMC scheme designed for efficient sampling from a particular type of distribution. We'll use a proper measure theoretic formulation. The notation in this section is not consistent with those before and after. Suppose our target distribution is,
% %
% \begin{IEEEeqnarray}{rCl}
%  \tden(d\lhard, d\leasy) & = & \tden(d\leasy|\lhard) \tden(d\lhard)     ,
% \end{IEEEeqnarray}
% %
% where $\leasy$ is an ``easy'' variable to sample, in that we have a Markov kernel $\mcmckern(d\leasy|\leasy, \lhard)$ which has $\tden(d\leasy|\lhard)$ as its invariant distribution and which mixes quickly. In addition, assume $\tden(d\leasy|\lhard)$ is known and not mixed, so that we have either $\tden(d\leasy|\lhard) = \tden(\leasy|\lhard)$ (discrete) or $\tden(d\leasy|\lhard) = \tden(\leasy|\lhard) d\leasy$ (continuous). However, $\leasy$ may have a high dimension, the conditional posterior $\tden(d\leasy|\lhard)$ is not easily approximated, and $\mcmckern$ does not have a density. In contrast, $\lhard$ is a ``hard'' variable to sample, for which no efficient fast-mixing Markov kernel exists.
% 
% The reason we're considering this type of distribution is that it exactly describes the problem we're going to encounter in the next section, when we introduce changepoints to our linear state space model. The hard variable is the sequence of changepoint times and the easy variable is the transition matrices for each section.
% 
% There are two simple Metropolis-Hastings schemes which could be applied to this problem in order to update $\lhard$. These are defined in algorithms~\ref{alg:mwg} and~\ref{alg:jmh}.
% 
% \begin{algorithm}
% \begin{algorithmic}[1]
%  \REQUIRE Current state of the chain $\left\{ \uhard\mc{m}, \ueasy\mc{m} \right\}$.
%  \STATE Propose a new value $\uhard\mcnew \sim \pden{\lhard}(d\lhard|\ueasy\mc{m},\uhard\mc{m})$.
%  \STATE Calculate acceptance probability\\ $\ap = \min\left\{1, \frac{ \tden(\ueasy\mc{m}|\uhard\mcnew) \tden(d\uhard\mcnew) }{ \tden(\ueasy\mc{m}|\uhard\mc{m}) \tden(d\uhard\mc{m}) } \frac{ \pden{\lhard}(d\uhard\mc{m}|\ueasy\mc{m},\uhard\mcnew) }{ \pden{\lhard}(d\uhard\mcnew|\ueasy\mc{m},\uhard\mc{m}) } \right\}$.
%  \STATE With probability $\ap$ set $\uhard\mc{m+1}\setto\uhard\mcnew$.\\ Otherwise set $\uhard\mc{m+1}\setto\uhard\mc{m}$.
%  \STATE Set $\ueasy\mc{m+1}\setto\ueasy\mc{m}$.
%  \RETURN New state of the chain $\left\{ \uhard\mc{m+1}, \ueasy\mc{m+1} \right\}$.
% \end{algorithmic}
% \caption{Metropolis-within-Gibbs.}
% \label{alg:mwg}
% \end{algorithm}
% 
% \begin{algorithm}
% \begin{algorithmic}[1]
%  \REQUIRE Current state of the chain $\left\{ \uhard\mc{m}, \ueasy\mc{m} \right\}$.
%  \STATE Propose a new value $\uhard\mcnew \sim \pden{\lhard}(d\lhard|\uhard\mc{m})$.
%  \STATE Propose a new value $\ueasy\mcnew \sim \pden{\leasy}(d\leasy|\uhard\mcnew)$.
%  \STATE Calculate acceptance probability\\ $\ap = \min\left\{1, \frac{ \tden(\ueasy\mcnew|\uhard\mcnew) \tden(d\uhard\mcnew) }{ \tden(\ueasy\mc{m}|\uhard\mc{m}) \tden(d\uhard\mc{m}) } \frac{ \pden{\leasy}(\ueasy\mc{m}|\uhard\mc{m}) \pden{\lhard}(d\uhard\mc{m}|\uhard\mcnew) }{ \pden{\leasy}(\ueasy\mcnew|\uhard\mcnew) \pden{\lhard}(d\uhard\mcnew|\uhard\mc{m}) } \right\}$.
%  \STATE With probability $\ap$ set $\uhard\mc{m+1}\setto\uhard\mcnew$ and $\ueasy\mc{m+1}\setto\ueasy\mcnew$.\\ Otherwise set $\uhard\mc{m+1}\setto\uhard\mc{m}$ and $\ueasy\mc{m+1}\setto\ueasy\mc{m}$.
%  \RETURN New state of the chain $\left\{ \uhard\mc{m+1}, \ueasy\mc{m+1} \right\}$.
% \end{algorithmic}
% \caption{Joint Metropolis-Hastings.}
% \label{alg:jmh}
% \end{algorithm}
% 
% We could use \emph{Metropolis-within-Gibbs}, in which $\ueasy$ is left unchanged. The problem with this scheme is that the fixed value of $\ueasy$ may be poorly matched to the newly proposed value of $\uhard$, in which case the acceptance rate will be low unless $\pden{\lhard}(d\lhard|\uhard\mc{m})$ is tightly concentrated around $\uhard\mc{m}$.
% 
% Alternatively we could use \emph{Joint Metropolis-Hastings}, in which a new value is proposed simultaneously for $\ueasy$. For the models we consider, there is no simple and sufficiently close approximation of $\tden(d\leasy|\lhard)$ which can be used for $\pden{\leasy}(d\leasy|\lhard)$, and hence the acceptance rates are again low.
% 
% The intuitive thing to do here is to first propose $\uhard\mcnew \sim \pden{\lhard}(d\lhard|\uhard\mc{m})$, and then use a few steps of sampling from our fast-mixing kernel $\mcmckern$ as a mechanism for proposing $\ueasy$. And this is exactly what we do.
% 
% 
% \subsection{Wait... What? Is that valid?}
% At first, such a scheme looks highly suspicious, and it seems dubious that it will have the correct invariant distribution. The obvious problem is that we cannot evaluate the probability (or density) corresponding to the proposal which we appear to need in the acceptance ratio. We circumvent this difficulty by considering the following extended target distribution over an augmented space,
% %
% \begin{IEEEeqnarray}{rCl}
%  \etden(d\lhard, d\leasy, d\laux) & = & \frac{1}{\nc} \tden(d\lhard) \tden(\leasy|\lhard) \aprden(d\laux) \mcmckern(d\leasy|\laux, \lhard)     ,
% \end{IEEEeqnarray}
% %
% where $\laux$ is an auxiliary variable with an artificial prior $\aprden$, and the normalising constant is
% %
% \begin{IEEEeqnarray}{rCl}
%  \nc & = & \int \int \int \tden(d\lhard) \tden(\leasy|\lhard) \aprden(d\laux) \mcmckern(d\leasy|\laux, \lhard)     .
% \end{IEEEeqnarray}
% 
% The steps of the proposal are defined in algorithm~\ref{alg:mcmcwmcmcproposal}.
% 
% \begin{algorithm}
% \begin{algorithmic}[1]
%  \REQUIRE Current state of the chain $\left\{ \uhard\mc{m}, \ueasy\mc{m}, \uaux\mc{m} \right\}$.
%  \STATE Propose a new value $\uhard\mcnew \sim \pden{\lhard}(d\lhard|\uhard\mc{m})$.
%  \STATE Propose a new value $\ueasy\mcnew \sim \mcmckern(d\leasy|\ueasy\mc{m}, \uhard\mcnew)$.
%  \STATE Set $\uaux\mcnew \setto \ueasy\mc{m}$.
% \end{algorithmic}
% \caption{Proposal for MCMC-within-MCMC}
% \label{alg:mcmcwmcmcproposal}
% \end{algorithm}
% 
% Setting $\uaux\mcnew$ may be viewed as ``sampling'' from the Dirac measure $\dirac{\ueasy\mc{m}}{d\laux}$. Hence, the acceptance ratio is,
% %
% \begin{IEEEeqnarray}{rCl}
%  \frac{ \tden(d\uhard\mcnew) \tden(\ueasy\mcnew|\uhard\mcnew) \aprden(d\uaux\mcnew) \mcmckern(d\ueasy\mcnew|\uaux\mcnew, \uhard\mcnew) }{ \tden(d\uhard\mc{m}) \tden(\ueasy\mc{m}|\uhard\mc{m}) \aprden(d\uaux\mc{m}) \mcmckern(d\ueasy\mc{m}|\uaux\mc{m}, \uhard\mc{m}) } \nonumber \\
%  \qquad \times \frac{ \pden{\lhard}(d\uhard\mc{m}|\uhard\mcnew) \mcmckern(d\ueasy\mc{m}|\ueasy\mcnew, \uhard\mc{m}) \dirac{\ueasy\mcnew}{d\uaux\mc{m}} }{ \pden{\lhard}(d\uhard\mcnew|\uhard\mc{m}) \mcmckern(d\ueasy\mcnew|\ueasy\mc{m}, \uhard\mcnew) \dirac{\ueasy\mc{m}}{d\uaux\mcnew} }
% \end{IEEEeqnarray}


\bibliographystyle{IEEEtran}
\bibliography{/users/pete/Dropbox/PhD/OTbib}
\end{document}