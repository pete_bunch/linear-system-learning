% Clean up
clear all
close all
clc

rng(0)

% Load data
fh = fopen('sensorLog_20150427T150418.txt');
data = textscan(fh, '%u64%s%f%f%f%f%f%f');

% Sensor indexes
ori_idx = strcmp(data{2}, 'ORI');
gyr_idx = strcmp(data{2}, 'RAWGYR');

% Time stamps
min_time = min(data{1});
time_stamps = data{1} - min_time;
ori_time = time_stamps(ori_idx);
gyr_time = time_stamps(gyr_idx);

% Data
ori_data = [data{3}(ori_idx), data{4}(ori_idx), data{5}(ori_idx), data{6}(ori_idx)];
gyr_data = [data{3}(gyr_idx), data{4}(gyr_idx), data{5}(gyr_idx)];

% Plot
figure, plot(ori_time, ori_data');
figure, plot(gyr_time, gyr_data');

% Chop the bit we want
start_time = 25000; time_length = 30000;

keep = (ori_time>start_time)&(ori_time<(start_time+time_length));
ori_time = ori_time(keep)-start_time;
ori_data(~keep,:) = [];

keep = (gyr_time>start_time)&(gyr_time<(start_time+time_length));
gyr_time = gyr_time(keep)-start_time;
gyr_data(~keep,:) = [];

% Interpolate onto the same time grid
interp_gyr_data = interp1(single(gyr_time), gyr_data(:,3), single(ori_time));

% Add some noise
debias_ori_data = ori_data(:,4) + normrnd(0,0.1,size(ori_data(:,4)));

% Make vectors
t = ori_time;
y = [debias_ori_data, interp_gyr_data];

% Remove last point
t(end) = [];
y(end,:) = [];

% Plot the data we're going to use
figure, plot(t, y(:,1));
figure, plot(t, y(:,2));

% Save data to csv file
csvwrite('processed-data.csv', y);

% figure, hold on
% plot(abs(fft(y(:,1))))
% plot(abs(fft(y(:,2))))

