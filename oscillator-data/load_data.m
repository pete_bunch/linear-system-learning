% Clean up
clear all
close all
clc

% Load data
fh = fopen('sensorLog_20150427T070149.txt');
data = textscan(fh, '%u64%s%f%f%f%f%f%f');

% Sensor indexes
acc_idx = strcmp(data{2}, 'ACC');
gyr_idx = strcmp(data{2}, 'RAWGYR');

% Time stamps
min_time = min(data{1});
time_stamps = data{1} - min_time;
acc_time = time_stamps(acc_idx);
gyr_time = time_stamps(gyr_idx);

% Data
acc_data = [data{3}(acc_idx), data{4}(acc_idx), data{5}(acc_idx)];
gyr_data = [data{3}(gyr_idx), data{4}(gyr_idx), data{5}(gyr_idx), data{6}(gyr_idx), data{7}(gyr_idx), data{8}(gyr_idx)];

% Plot
figure, plot(acc_time, acc_data');
figure, plot(gyr_time, gyr_data');

% Chop the bit we want
start_time = 3480; time_length = 10000;

keep = (acc_time>start_time)&(acc_time<(start_time+time_length));
acc_time = acc_time(keep)-start_time;
acc_data(~keep,:) = [];

keep = (gyr_time>start_time)&(gyr_time<(start_time+time_length));
gyr_time = gyr_time(keep)-start_time;
gyr_data(~keep,:) = [];

% Interpolate onto the same time grid
interp_gyr_data = interp1(single(gyr_time), gyr_data(:,2), single(acc_time));

% Remove bias
debias_acc_data = acc_data(:,2) - mean(acc_data(:,2));

% Make vectors
t = acc_time;
y = [debias_acc_data, interp_gyr_data];

% Remove last point
t(end) = [];
y(end,:) = [];

% Plot the data we're going to use
figure, plot(t, y(:,1));
figure, plot(t, y(:,2));

% Save data to csv file
csvwrite('processed-data.csv', y(:,1));
