% Clean up
clear all
close all
clc

% Load data
fh = fopen('sensorLog_20150427T150052.txt');
data = textscan(fh, '%u64%s%f%f%f%f%f%f');

% Sensor indexes
mag_idx = strcmp(data{2}, 'MAG');
gyr_idx = strcmp(data{2}, 'GYR');

% Time stamps
min_time = min(data{1});
time_stamps = data{1} - min_time;
mag_time = time_stamps(mag_idx);
gyr_time = time_stamps(gyr_idx);

% Data
mag_data = [data{3}(mag_idx), data{4}(mag_idx), data{5}(mag_idx)];
gyr_data = [data{3}(gyr_idx), data{4}(gyr_idx), data{5}(gyr_idx)];

% Plot
figure, plot(mag_time, mag_data');
figure, plot(gyr_time, gyr_data');

% Chop the bit we want
start_time = 6530; time_length = 30000;

keep = (mag_time>start_time)&(mag_time<(start_time+time_length));
mag_time = mag_time(keep)-start_time;
mag_data(~keep,:) = [];

keep = (gyr_time>start_time)&(gyr_time<(start_time+time_length));
gyr_time = gyr_time(keep)-start_time;
gyr_data(~keep,:) = [];

% Interpolate onto the same time grid
interp_gyr_data = interp1(single(gyr_time), gyr_data(:,3), single(mag_time));

% % Remove bias
% debias_mag_data = mag_data(:,3) - mag_data(end,3);
head_mag_data = atan2(mag_data(:,2), mag_data(:,1));

% Make vectors
t = mag_time;
y = [head_mag_data , interp_gyr_data];

% Remove last point
t(end) = [];
y(end,:) = [];

% Plot the data we're going to use
figure, plot(t, y(:,1));
figure, plot(t, y(:,2));

% Save data to csv file
csvwrite('processed-data.csv', y);

figure, hold on, plot(abs(fft(y(:,1)))), plot(abs(fft(y(:,2))))
