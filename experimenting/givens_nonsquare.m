clup

% Parameters
d = 7;
r = 4;

% Make an orthogonal matrix
X = randn(d,d);
[Q,~] = qr(X*X');
Q = Q(:,1:r);

Qr = eye(r);

Qg = Q;
Qg
cnt = 0;

% Row space
Ur = eye(r);
Qrow = eye(r);
for rr = r:-1:1
    for cc = 1:rr-1
        
        cnt = cnt + 1;
        
        v = Qg(rr,[rr,cc])';
        [G,~] = planerot(v);
        U = eye(r);
        U([cc,rr],[cc,rr]) = G;
        
        Qg = Qg*U;
        Ur = U'*Ur;
        Qrow = U'*Qrow;
        
        Qg
        
    end
end

% Cross rotations
Uc = eye(d);
Qcross = eye(d);
for cc = 1:r
    for rr = d:-1:r+1
        
        cnt = cnt + 1;
        
        v = Qg([cc,rr],cc);
        [G,~] = planerot(v);
        U = eye(d);
        U([cc,rr],[cc,rr]) = G;
        
        Qg = U*Qg;
        Uc = Uc*U';
        Qcross = Qcross*U';
        
        Qg
        
    end
end

E = Qg;

Qr = Uc*E*Ur

max(max(abs(E-[eye(r); zeros(d-r,r)])))
max(max(abs(Q-Qr)))


% Qg = Q;
% for rr = 1:d
%     for cc = 1:min(r,rr-1)
%         
%         Qg
%         
%         v = Qg([cc,rr],cc);
%         [G,~] = planerot(v);
%         U = eye(d);
%         U([cc,rr],[cc,rr]) = G;
%         
%         Qg = U*Qg;
%         
%     end
% end
% 
% Qg