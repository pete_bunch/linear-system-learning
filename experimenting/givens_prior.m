clup

d = 8;
r = 4;
N = 10000;

cross_givens_array = zeros(N,r*(d-r));
row_givens_array = zeros(N,r*(r-1)/2);
sign_array = zeros(N,r);

for ii = 1:N
    
    % Random SO(d) matrix
    U = RandOrthMat(d);
%     X = randn(d,d);
    %%%%%
%     U = orth(X);
%     [U,~] = qr(X);
%     [U,~,~] = svd(X);
%     [U,~] = eig(X*X');
    %%%%%
    U(:,r+1:d) = [];
    
    Ured = U;
    Uc = eye(d);
    Ur = eye(r);

    % Row space
    cnt = 0;
    for rr = r:-1:1
        for cc = 1:rr-1
            
            cnt = cnt + 1;
            
            v = Ured(rr,[rr,cc])';
            [G,~] = planerot(v);
            R = eye(r);
            R([cc,rr],[cc,rr]) = G;
            
            Ured = Ured*R;
            row_givens_array(ii,cnt) = atan(G(1,2)/G(1,1));
            
            Ur = R'*Ur;
            
        end
    end
    
    % Cross rotations
    cnt = 0;
    for cc = 1:r
        for rr = d:-1:r+1
            
            cnt = cnt + 1;
            
            v = Ured([cc,rr],cc);
            [G,~] = planerot(v);
            R = eye(d);
            R([cc,rr],[cc,rr]) = G;
            
            Ured = R*Ured;
            cross_givens_array(ii,cnt) = atan(G(1,2)/G(1,1));
            
            Uc = Uc*R';
            
        end
    end
    
    % Sign ambiguities
    E = Ured;
    sign_array(ii,:) = diag(E)';
    
    % Check it worked
    assert(max(max(abs(Uc*E*Ur-U)))<1E-6);
    
end

%%

close all

figure
gg = 0;
for rr = r:-1:1
    for cc = 1:rr-1
        gg = gg + 1;
        idx = (rr-1)*r+cc;
        subplot(r,r,idx), hold on
        
        [f,x]=hist(row_givens_array(:,gg),100);
        bar(x,f/trapz(x,f));
        th = -pi/2:0.01:pi/2;
        power = cc-1;
        git disp( power )
        pdf = cos(th).^power;
        pdf = pdf/(0.01*sum(pdf));
        plot(th,pdf,'r');
        
    end
end

figure
gg = 0;
for cc = 1:r
    for rr = d:-1:r+1
        gg = gg + 1;
        subplot(r,d-r,gg), hold on
        
        [f,x]=hist(cross_givens_array(:,gg),100);
        bar(x,f/trapz(x,f));
        th = -pi/2:0.01:pi/2;
        pdf = cos(th).^(d-1-rr+cc);%(rr-cc-1);
        pdf = pdf/(0.01*sum(pdf));
        plot(th,pdf,'r');
        
    end
end

figure
for ii = 1:r
    subplot(1,r,ii)
    hist(sign_array(:,ii))
end
