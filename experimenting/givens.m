clup

% Parameters
d = 7;
r = 3;

% Make an orthogonal matrix
X = randn(d,d);
[Q,~] = qr(X*X');

Qr = eye(d);
Us = cell(1,d*(d-1)/2);

% Qf = Q;
% Q(:,r+1:end) = [];

Qg = Q;

Qg
cnt = 0

% Null space
for rr = d:-1:r+1
    for cc = rr-1:-1:r+1
        
        cnt = cnt + 1;
        
        v = Qg(rr,[rr,cc])';
        [G,~] = planerot(v);
        U = eye(d);
        U([cc,rr],[cc,rr]) = G;
        
        Qg = Qg*U;
        Qr = U'*Qr;
        Us{cnt} = U';
        
        Qg
        
    end
end

% Row space
for rr = r:-1:1
    for cc = 1:rr-1
        
        cnt = cnt + 1;
        
        v = Qg(rr,[rr,cc])';
        [G,~] = planerot(v);
        U = eye(d);
        U([cc,rr],[cc,rr]) = G;
        
        Qg = Qg*U;
        Qr = U'*Qr;
        Us{cnt} = U';
        
        Qg
        
    end
end

% Cross rotations
% for rr = d:-1:r+1
%     for cc = r:-1:1
for cc = r:-1:1
    for rr = d:-1:r+1
        
        cnt = cnt + 1;
        
        v = Qg(rr,[rr,cc])';
        [G,~] = planerot(v);
        U = eye(d);
        U([cc,rr],[cc,rr]) = G;
        
        Qg = Qg*U;
        Qr = U'*Qr;
        Us{cnt} = U';
        
        Qg
        
    end
end

E = Qg;

Qr = E*Qr

max(max(abs(Q-Qr)))

% Qg = Q;
% for rr = 1:d
%     for cc = 1:min(r,rr-1)
%         
%         Qg
%         
%         v = Qg([cc,rr],cc);
%         [G,~] = planerot(v);
%         U = eye(d);
%         U([cc,rr],[cc,rr]) = G;
%         
%         Qg = U*Qg;
%         
%     end
% end
% 
% Qg