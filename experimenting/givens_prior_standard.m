clup

d = 7;
r = 7;
N = 10000;

num_rot = d*(d-1)/2;%r*(d-r);

givens_array = zeros(N,num_rot);

for ii = 1:N
    
    % Random SO(d) matrix
    X = randn(d,d);
    [U,~,~] = svd(X);
    U(:,r+1:d) = [];
    
%     givens_list = [];
    Ured = U;
    
    % Normal QR
    cnt = 0;
    for cc = 1:d
        for rr = cc+1:d
            
            cnt = cnt + 1;
            
            v = Ured([cc,rr],cc);
            [G,~] = planerot(v);
            R = eye(d);
            R([cc,rr],[cc,rr]) = G;
            
            Ured = R*Ured;
            givens_array(ii,cnt) = atan(G(1,2)/G(1,1));
            
        end
    end
    
end


%%
figure
gg = 0;
for cc = 1:d
    for rr = cc+1:d
        gg = gg + 1;
        idx = rr*(d-1)+(cc);
        subplot(d,d,idx), hold on
        
        [f,x]=hist(givens_array(:,gg),100);
        bar(x,f/trapz(x,f));
        th = -pi/2:0.01:pi/2;
        pdf = cos(th).^(rr-cc-1);
        pdf = pdf/(0.01*sum(pdf));
        plot(th,pdf,'r');
        
    end
end
