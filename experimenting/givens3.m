d = 6;

g = unifrnd(-pi/2,pi/2);
R = [cos(g) sin(g);
    -sin(g) cos(g)];

ii = 1;
jj = 2;
G = eye(d);
G([ii,jj],[ii,jj]) = R;

i1 = 2;
i2 = 4;
E = eye(d);
E(i1,i1) = -1;
E(i2,i2) = -1;

G
G*E
E*G
E*G'

% %%
% d = 5;
% g = pi/2;
% 
% R = [cos(g) sin(g);
%     -sin(g) cos(g)];
% 
% ii = 2;
% jj = 5;
% G = eye(d);
% G([ii,jj],[ii,jj]) = R;
% E = eye(d);
% E(ii,ii) = -1;
% E(jj,jj) = -1;
% 
% G'-E*G