function [ G ] = givrot( g, ii, jj, d )
%GIVROT Summary of this function goes here
%   Detailed explanation goes here

R = [cos(g) sin(g);
    -sin(g) cos(g)];
G = eye(d);
G([ii,jj],[ii,jj]) = R;

end

