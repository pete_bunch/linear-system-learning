clear all
clc

d = 8;
r = 4;

    
% Random SO(d) matrix

U = RandOrthMat(d);

Ured = U;
Uc = eye(d);
Ur = eye(d);
Un = eye(d);

% Row space
cnt = 0;
for rr = r:-1:1
    for cc = rr-1:-1:1
        
        cnt = cnt + 1;
        
        v = Ured(rr,[rr,cc])';
        [G,~] = planerot(v);
        R = eye(d);
        R([cc,rr],[cc,rr]) = G;
        
        Ured = Ured*R;
        
        Ur = R'*Ur;
        
    end
end

% Cross rotations
cnt = 0;
for cc = 1:r
    for rr = r+1:d
        
        cnt = cnt + 1;
        
        v = Ured([cc,rr],cc);
        [G,~] = planerot(v);
        R = eye(d);
        R([cc,rr],[cc,rr]) = G;
        
        Ured = R*Ured;
        
        Uc = Uc*R';
        
    end
end

% Null space
cnt = 0;
for rr = d:-1:r+1
    for cc = rr-1:-1:r+1
        
        cnt = cnt + 1;
        
        v = Ured(rr,[rr,cc])';
        [G,~] = planerot(v);
        R = eye(d);
        R([cc,rr],[cc,rr]) = G;
        
        Ured = Ured*R;
        
        Un = R'*Un;
        
    end
end

% Sign ambiguities
E = Ured;

% Check it worked
assert(max(max(abs(diag(E)-1)))<1E-6);
assert(max(max(abs(abs(E)-eye(d))))<1E-6);
assert(max(max(abs(Uc*E*Ur*Un-U)))<1E-6);


