clup

% Parameters
d = 7;
r = 3;

% Build an orthogonal matrix from givens angles in the wrong order
U = eye(d);
for rr = 1:d
    for cc = 1:rr-1
        
        theta = unifrnd(-pi/2,pi/2);
        giv = [cos(theta), sin(theta);
              -sin(theta), cos(theta)];
        G = eye(d);
        G([cc,rr],[cc,rr]) = giv;
        
        U = G*U;
        
        U
        
    end
end