syms z

A = 0.9;
t = pi/6;
% poles = [0.85, 0.95, 0.9*(0.5+1i*sqrt(3)/2), 0.9*(0.5-1i*sqrt(3)/2)];
poles = [A*(cos(t)+1i*sin(t)), A*(cos(t)-1i*sin(t))];

poly = sym(1, 'd');
for pp = 1:length(poles)
    poly = poly*(1-z*poles(pp));
end

pretty(expand(poly))