import numpy as np
import matplotlib.pyplot as plt
from linear_model import LinearModel,SparseLinearModel
from kalman import GaussianDensity, Series

plt.close("all")
np.random.seed(0)

sparse = False
K = 100
d = 2

A = np.array([[0.9,0.9],[0,0.9]])
B = np.array([[1,0],[0,1]])
G = np.array([[0.1,0.01],[0.01,0.1]])

#H = np.array([[1,0],[0,1]])
#R = np.array([[0.1,0],[0,0.1]])
H = np.array([[1,1]])
R = np.array([[0.1]]);

m0 = np.array([1,0])
P0 = np.array([[10,0],[0,0.01]])

# Create necessary objects
first_state_prior = GaussianDensity(m0,P0)
if sparse:
    model = SparseLinearModel(first_state_prior, A, B, G, H, R)
else:
    model = LinearModel(first_state_prior, A, G, H, R)

# Generate data
state, observ = model.simulate_data(K)

# Kalman filter
flt,prd,lhood = model.kalman_filter(observ)

# RTS smoother
smt = model.rts_smoother(flt,prd)

# Sample from the smoothing distribution
smp = model.sample_posterior(observ)


# Try plotting some things
figs = []
for dd in range(d):
    f = plt.figure().add_subplot(111)
    figs.append(f)
    
    Series.plot_sequence(state, dd, f, 'k')
#    Series.plot_sequence(observ, dd, f, 'r')
    Series.plot_sequence(smp, dd, f, 'b')

    GaussianDensity.plot_sequence(flt, dd, f, 'g')
    GaussianDensity.plot_sequence(smt, dd, f, 'c')
plt.show()
