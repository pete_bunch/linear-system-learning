import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from linear_changepoint_model import LinearChangepointModel, LinearChangepointModelLearner
from kalman import GaussianDensity, Series

import system_learning as sysl

plt.close("all")
np.random.seed(1)

K = 300
d = 3

A_prior_vr = np.identity(pow(d,2))
cp_prior_rate = 1./150.

F1 = np.array([[0.8,-0.6,0.2],[0.1,0.7,-0.4],[-0.2,0.3,0.6]])
F2 = np.array([[0.9,0,0],[0,0.9,0],[0,0,0.9]])
F3 = np.array([[0.9,0.3,-0.6],[0,0.9,0.5],[0,0,0.9]])

#F1 = sysl.random_transition_matrix(d, A_prior_vr)
#F2 = sysl.random_transition_matrix(d, A_prior_vr)
#F3 = sysl.random_transition_matrix(d, A_prior_vr)

Q = 1*np.identity(d)
#H = np.array([[1,0,0]])
#R = np.array([[1]])
H = np.identity(d)
R = 1*np.identity(d)
m0 = np.zeros(d)
P0 = np.identity(d)

# Make a changepoint linear system
first_state_prior = GaussianDensity(m0, P0)
cp_model = LinearChangepointModel([100,200],K,[F1,F2,F3],Q,H,R,first_state_prior)


# Generate data
state, observ = cp_model.simulate_data()
f = plt.figure()
Series.plot_sequence(state, range(d), f)

# Create an estimate of the linear system
F = []
cp = []
#cp = [10,290]
#cp = [30, 60, 90, 120, 150, 180, 210, 240, 270]
for nn in range(len(cp)+1):
    F.append(sysl.random_transition_matrix(d, A_prior_vr))
init_cp_model_est = LinearChangepointModel(cp, K, F, Q, H, R, first_state_prior)

# Make a system learning object
brain = LinearChangepointModelLearner(init_cp_model_est, observ, A_prior_vr, cp_prior_rate)

# Gibbs sample the transition matrix
for ii in range(1000):
    print(ii)
    brain.iterate_transition_matrix(3)

#    brain.iterate_simple_changepoint("add")
#    for nn in range(1,brain.model.Nm):
#        brain.iterate_simple_changepoint("move", idx=nn)
#        brain.iterate_simple_changepoint("adjust",idx=nn)
#    brain.iterate_simple_changepoint("remove")
    
    brain.iterate_annealed_changepoint("add", num_anneal_steps = 10)
    for nn in range(1,brain.model.Nm):
        brain.iterate_annealed_changepoint("move")
        brain.iterate_annealed_changepoint("adjust", num_anneal_steps = 5)
    brain.iterate_annealed_changepoint("remove")

    brain.save_link()

#brain.draw_matrix_histogram(cp_model, 50)
brain.draw_changepoint_histogram(cp_model, 20)

# Final model estimate
cp_model_est = brain.return_model()

if 0:
        
    # Kalman filter
    flt,prd,lhood = cp_model.kalman_filter(observ)
    
    # RTS smoother
    smt = cp_model.rts_smoother(flt, prd)
    
    # Sample from the smoothing distribution
    smp = cp_model.sample_posterior(observ)
    
    # Try plotting some things
    figs = []
    for dd in range(d):
        f = plt.figure().add_subplot(111)
        figs.append(f)
        f.plot( range(K), [x[dd] for x in state], 'k-' )
        f.plot( range(K), [x[dd] for x in observ], 'r-' )
        # Filter
        f.plot( range(K), [x.mn[dd] for x in flt], 'g-' )
        f.plot( range(K), [x.mn[dd]+2*np.sqrt(x.vr[dd,dd]) for x in flt], ':g' )
        f.plot( range(K), [x.mn[dd]-2*np.sqrt(x.vr[dd,dd]) for x in flt], ':g' )
        # Smoother
        f.plot( range(K), [x.mn[dd] for x in smt], 'c-' )
        f.plot( range(K), [x.mn[dd]+2*np.sqrt(x.vr[dd,dd]) for x in smt], ':c' )
        f.plot( range(K), [x.mn[dd]-2*np.sqrt(x.vr[dd,dd]) for x in smt], ':c' )
        # Posterior Sample
        f.plot( range(K), [x[dd] for x in smp], 'b-')
    plt.show()