import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg as la
import system_learning as sysl
import kalman as kal
from scipy.stats import multivariate_normal as mvn
from kalman import GaussianDensity, Series


#import collections
#SystemMatrices = collections.namedtuple('SystemMatrices', ['F', 'Q', 'H', 'R'])

class SystemMatrices:
     """Container for a set of system matrices
        This need to be mutable, which is why we're not using a named tuple"""
     def __init__(self, F, Q, H, R):
         self.F = F
         self.Q = Q
         self.H = H
         self.R = R



class GenericLinearChangepointModel:
    """Base class for both standard and annealed changepoint models."""
    # For now, this expects a list of transition matrices, and a constant for the other system matrices.
    
    @property
    def Nm(self):
        """Number of models"""
        return len(self.system)
        
    @property
    def Nc(self):
        """Number of changepoints"""
        return len(self.point)
        
    def stage_at(self,k):
        """Return the index of the regime in force at the (k)th time instant"""
        pass
        
    def first_in_stage(self, n):
        """Return the index of the first instant at which the (n)th system is
           in force"""
        if n == 0:
            first_idx = 0
        else:
            first_idx = self.point[n-1]
        return first_idx
        
    def last_in_stage(self, n):
        """Return the indexes of the first instant at which the (n)th regime is
           NOT in force"""
        if n < self.Nc:
            last_idx = self.point[n]
        else:
            last_idx = self.K
        return last_idx
        
    def cp_prior(self, cp_prior_rate):
        """Evaluate prior for the changepoint sequence"""
        # Currently just exponential prior. 
        prior = self.Nm*np.log(cp_prior_rate) - cp_prior_rate*self.K
        return prior

    def propose_move(self, nn):
        """MH Proposal: Move a changepoint within the bounds set by those
           either side"""
        f_idx = self.first_in_stage(nn-1)
        l_idx = self.last_in_stage(nn)
        
        # Is there space?
        if l_idx-f_idx > 1:
            self.point[nn-1] = np.random.random_integers(f_idx+1,l_idx-1)
            valid = True
        else:
            valid = False
        
        # Copies for annealing
        padded_point = self.point
        padded_system = self.system
        
        return valid,padded_point,padded_system
        
    def propose_adjust(self, nn, half_width):
        """MH Proposal: Move a changepoint within a small window"""

        # Get limits of the adjacent stages
        f_idx = self.first_in_stage(nn-1)
        l_idx = self.last_in_stage(nn)
        
        # Is there space?
        if l_idx-f_idx > 1:        
        
            # Work out the window in which we can move it
            lower = max(f_idx+1,self.point[nn-1]-half_width)
            upper = min(l_idx-1,self.point[nn-1]+half_width)
            fwd_win_len = upper-lower+1
            
            # Propose a new value
            new_point = np.random.random_integers(lower,upper)
            self.point[nn-1] = new_point
            
            # Work out the window for the reverse move
            lower = max(f_idx+1,new_point-half_width)
            upper = min(l_idx-1,new_point+half_width)
            bck_win_len = upper-lower+1

            valid = True            
            
        else:
            valid = False
            
        # Copies for annealing
        padded_point = self.point
        padded_system = self.system
        
        return valid,fwd_win_len,bck_win_len,padded_point,padded_system
        
    def propose_add(self, nn, A_prior_vr):
        """MH Proposal: Add a changepoint and propose a new transition matrix
           from the prior for the shorter of the two newly created stages"""

        # Copy the point list for the annealed version
        padded_point = list(self.point)

        # Get the limits of the stage and see if there is space
        f_idx = self.first_in_stage(nn-1)
        l_idx = self.last_in_stage(nn-1)
        win_len = l_idx-f_idx-1
        
        # Is there space?
        if win_len > 0:
            
            # Propose a new point and matrix
            new_cp = np.random.random_integers(f_idx+1,l_idx-1)
            new_A = sysl.sample_transition_matrix_prior(A_prior_vr)
            new_sys = SystemMatrices(new_A,self._Q,self._H,self._R)
            
            # Insert the point
            self.point.insert(nn-1,new_cp)
            
            # Insert the matrix into the shorter of the new stages
            if (l_idx-new_cp) < (new_cp-f_idx):
                self.system.insert(nn,new_sys)
            else:
                self.system.insert(nn-1,new_sys)
                
            # Copy the system list by reference and add a dummy point to the list
            padded_system = self.system         # Copy by reference only
            if (l_idx-new_cp) < (new_cp-f_idx):
                padded_point.insert(nn-1,l_idx)
            else:
                padded_point.insert(nn-1,max(f_idx-1,0))
            
            valid = True
            
        else:
            padded_system = self.system
            valid = False
            
        return valid,win_len,padded_point,padded_system
    
    def propose_remove(self, nn):
        """MH Proposal: Remove a changepoint and the shorter of the two 
           adjoining stages"""
           
        # Copy the point and system lists for the annealed version
        padded_point = list(self.point)
        padded_system = list(self.system) # Objects copied by reference only
           
        # Get limits of the adjacent stages
        f_idx = self.first_in_stage(nn-1)
        l_idx = self.last_in_stage(nn)
        win_len = l_idx-f_idx-1
        
        # Remove the shorter of the two stages
        if (l_idx-self.point[nn-1]) < (self.point[nn-1]-f_idx):
            self.system.pop(nn)
        else:
            self.system.pop(nn-1)
            
        # Remove the point
        self.point.pop(nn-1)
        valid = True

        # Move the removed point to a dummy location
        if (l_idx-padded_point[nn-1]) < (padded_point[nn-1]-f_idx):
            padded_point[nn-1] = l_idx
        else:
            padded_point[nn-1] = f_idx

        return valid,win_len,padded_point,padded_system
        
        
        
        
class LinearChangepointModel(GenericLinearChangepointModel):
    """Linear Model With Abrupt Changes"""

    def __init__(self, cp, K, F, Q, H, R, P1):
        assert len(F)==len(cp)+1
        self.point = list(cp)
        self.K = K
        self.system = [SystemMatrices(F[nn].copy(),Q.copy(),H.copy(),R.copy()) for nn in range(len(F))]
        self.ds = Q.shape[0]
        self.do = R.shape[0]
        self.P1 = P1
        
        # Save these for later
        self._H = H
        self._Q = Q
        self._R = R

    def copy(self):
        return LinearChangepointModel(self.point, self.K,
                                      [self.system[nn].F for nn in range(self.Nm)],
                                      self._Q, self._H, self._R,
                                      self.P1)

    def simulate_data(self):
        """Simulate artificial data from the model"""
        state = self.sample_prior()
        observ = self.sample_observ(state)
        return state, observ

    def sample_prior(self):
        """Sample from the state prior, running forwards in time"""
        state = Series.new_sequence(self.ds, self.K)
        state[0]  = mvn.rvs( mean=self.P1.mn, 
                             cov=self.P1.vr )
        nn = 0
        for kk in range(1,self.K):
            if kk in self.point:
                nn += 1
            state[kk]  = mvn.rvs( mean=np.dot(self.system[nn].F,state[kk-1]),
                                  cov=self.system[nn].Q )
        return state

    def sample_observ(self, state):
        """Sample an observation for each state"""
        #(only really useful for simulate_data - perhaps merge in)
        observ = Series.new_sequence(self.do, self.K)
        nn = 0
        for kk in range(self.K):
            if kk in self.point:
                nn += 1
            observ[kk] = mvn.rvs( mean=np.dot(self.system[nn].H,state[kk]),
                                  cov=self.system[nn].R )
        return observ

    def kalman_filter(self, observ):
        """Run a Kalman filter using the model on a set of observations and 
           return estimates as mean and covariance."""
        flt = GaussianDensity.new_sequence(self.ds, self.K)
        prd = GaussianDensity.new_sequence(self.ds, self.K)
        lhood = 0
        nn = 0
        for kk in range(self.K):
            if kk in self.point:
                nn += 1
            if kk > 0:
                prd[kk] = kal.predict(flt[kk-1], self.system[nn].F, self.system[nn].Q)
            else:
                prd[kk] = self.P1
            flt[kk],innov = kal.correct(prd[kk], observ[kk], self.system[nn].H, self.system[nn].R)
            lhood = lhood + mvn.logpdf(observ[kk], innov.mn, innov.vr)
        return flt, prd, lhood

    def rts_smoother(self, flt, prd):
        """Run a Rauch-Tung-Striebel smoother on the Kalman filter output 
           using the model"""
        smt = GaussianDensity.new_sequence(self.ds, self.K)
        nn = self.Nc
        for kk in reversed(range(self.K)):
            if kk+2 in self.point:
                nn -= 1
            if kk<self.K-1:
                smt[kk] = kal.update(flt[kk],smt[kk+1],prd[kk+1],self.system[nn].F)
            else:
                smt[kk] = flt[kk]
        return smt
        
    def backward_simulation(self, flt):
        """Use backward simulation to sample from the joint posterior"""
        x = Series.new_sequence(self.ds, self.K)
        nn = self.Nc
        for kk in reversed(range(self.K)):
            if kk+2 in self.point:
                nn -= 1
            if kk < self.K-1:
                samp_dens,_ = kal.correct(flt[kk],x[kk+1],self.system[nn].F,self.system[nn].Q)
            else:
                samp_dens = flt[kk]
            x[kk] = mvn.rvs(mean=samp_dens.mn, cov=samp_dens.vr)
        return x

    def sample_posterior(self, observ):
        """Sample a state trajectory from the joint smoothing distribution
           (i.e KF then BS)"""
        flt,_,_ = self.kalman_filter(observ)
        x = self.backward_simulation(flt)
        return x
        


class LinearChangepointModelLearner:
    """Worker Class for Linear-Gaussian Changepoint Model Learning"""

    def __init__(self, init_model_est, observ, A_prior_vr, cp_prior_rate):
        self.model = init_model_est.copy()
        self.chain = []
        self.chain.append(self.model.copy())
        self.observ = observ
        self.A_prior_vr = A_prior_vr
        self.cp_prior_rate = cp_prior_rate
    
    def return_model(self):
        # This ought to do some sort of averaging over the chain, not just return the final value
        return self.model.copy
        
    def draw_matrix_histogram(self, true_model, burn_in):
        for nn in range(self.model.Nmod):
            d = self.model.ds
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.system[nn].F[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.system[nn].F[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
    
    def draw_changepoint_histogram(self, true_model, burn_in):
        if len(self.chain) > burn_in:
            all_cp_list = []
            cp_num = []
            for rr in range(burn_in,len(self.chain)):
                for nn in range(len(self.chain[rr].point)):
                    all_cp_list.append(self.chain[rr].point[nn])
                    cp_num.append(len(self.chain[rr].point))
            f1 = plt.figure().add_subplot(111)
            f1.hist(all_cp_list, bins=50)
            for nn in range(true_model.Nm-1):
                f1.plot([true_model.point[nn]]*2, [0,len(self.chain[burn_in:])], '-r')
                        
            f2 = plt.figure().add_subplot(111)
            f2.hist(cp_num)
    
    def draw_chain_quantiles(self, true_model, burn_in):
        # Make a nice shaded plot like James's
        pass
        
    def save_link(self):
        """Save the current state of the model as a link in the chain"""
        self.chain.append(self.model.copy())
        print(self.model.point)
        print(" ")

    def iterate_transition_matrix(self, Mtimes):
        """Run a Gibbs iteration on each transition matrix"""
        
        for mm in range(Mtimes):
        
            # First sample the state sequence
            x = self.model.sample_posterior(self.observ)
            
            # Loop through the stages
            for nn in range(self.model.Nm):
                
                # Get the indexes of the (nn)th stretch
                f_idx = self.model.first_in_stage(nn)
                l_idx = self.model.last_in_stage(nn)
                if nn > 0:
                    f_idx = f_idx - 1
                    
                    # Sample a new transition matrix
                    F,_ = sysl.sample_transition_matrix_conditional(x[f_idx:l_idx], self.model._Q, self.A_prior_vr)
                    
                    # Replace the (nn)th system with a new one using the new transition matrix
                    #self.model.system[nn] = self.model.system[nn]._replace(F=F)
                    self.model.system[nn].F = F
        
    def iterate_simple_changepoint(self, move_type, idx=0, half_width=10):
        """Run simple Metropolis-Hastings update on the changepoint sequence"""
        
        # Copy the model
        ppsl_model = self.model.copy()
        
        # Propose a change of some kind
        if move_type == "move":
            if idx < 1: idx = np.random.random_integers(self.model.Nc)
            valid,_,_ = ppsl_model.propose_move(idx)
            bck_ppsl = 0
            fwd_ppsl = 0
        elif move_type == "adjust":
            if idx < 1: idx = np.random.random_integers(self.model.Nc)
            valid,fwd_win_len,bck_win_len,_,_ = ppsl_model.propose_adjust(idx, half_width)
            bck_ppsl = -np.log(bck_win_len)
            fwd_ppsl = -np.log(fwd_win_len)
        elif move_type == "add":
            if idx < 1: idx = np.random.random_integers(self.model.Nm)
            valid,win_len,_,_ = ppsl_model.propose_add(idx, self.A_prior_vr)
            bck_ppsl = 0
            fwd_ppsl = -np.log(win_len)
        elif move_type == "remove":
            if self.model.Nc > 0:
                if (idx < 1):
                    idx = np.random.random_integers(self.model.Nc)
                valid,win_len,_,_ = ppsl_model.propose_remove(idx)
                bck_ppsl = -np.log(win_len)
                fwd_ppsl = 0
            else:
                # Nothing to remove
                valid = False
                bck_ppsl = 0
                fwd_ppsl = 0
        else:
            raise ValueError("Invalid move type")
        
        print("Simple MH - {}: {}".format(move_type, ppsl_model.point))        
        
        if valid:
            _,_,old_lhood = self.model.kalman_filter(self.observ)
            old_cp_prior = self.model.cp_prior(self.cp_prior_rate)        
        
            _,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            new_cp_prior = ppsl_model.cp_prior(self.cp_prior_rate)
        
            ap = (new_lhood+new_cp_prior) - (old_lhood+old_cp_prior) + (bck_ppsl-fwd_ppsl)
        else:
            ap = -np.Infinity
            
        u = np.log(np.random.random_sample())
        print("    Acceptance probability: {}".format(ap))
        if u < ap:
            self.model = ppsl_model
            print("    Accepted")
        else:
            print("    Rejected")
            
    def iterate_annealed_changepoint(self, move_type, idx=0, half_width=10, num_anneal_steps=10):
        """Run annealed Metropolis-Hastings update on the changepoint sequence"""
        
        # Copy the model
        ppsl_model = self.model.copy()
        
        # Propose a change of some kind
        if move_type == "move":
            if idx < 1: idx = np.random.random_integers(self.model.Nc)
            valid,padded_point,padded_system = ppsl_model.propose_move(idx)
            bck_ppsl = 0
            fwd_ppsl = 0
        elif move_type == "adjust":
            if idx < 1: idx = np.random.random_integers(self.model.Nc)
            valid,fwd_win_len,bck_win_len,padded_point,padded_system = ppsl_model.propose_adjust(idx, half_width)
            bck_ppsl = -np.log(bck_win_len)
            fwd_ppsl = -np.log(fwd_win_len)
        elif move_type == "add":
            if idx < 1: idx = np.random.random_integers(self.model.Nm)
            valid,win_len,padded_point,padded_system = ppsl_model.propose_add(idx, self.A_prior_vr)
            bck_ppsl = 0
            fwd_ppsl = -np.log(win_len)
        elif move_type == "remove":
            if self.model.Nc > 0:
                if (idx < 1):
                    idx = np.random.random_integers(self.model.Nc)
                valid,win_len,padded_point,padded_system = ppsl_model.propose_remove(idx)
                bck_ppsl = -np.log(win_len)
                fwd_ppsl = 0
            else:
                # Nothing to remove
                valid = False
                bck_ppsl = 0
                fwd_ppsl = 0
        else:
            raise ValueError("Invalid move type")
        
        print("Annealed MH - {}: {}".format(move_type, ppsl_model.point))        
        
        if valid:
            
            #Create the annealed model
            annealed_model = AnnealedLinearChangepointModel(move_type, self.model, ppsl_model, padded_point, padded_system)
            
            # Old likelihood
            _,_,old_lhood = self.model.kalman_filter(self.observ)
            old_cp_prior = self.model.cp_prior(self.cp_prior_rate)
            
            anneal_ratios = []
        
            # Loop over the annealing steps
            for tt in range(num_anneal_steps):
                anfact = float(tt+1)/num_anneal_steps
                #anfact = num_anneal_steps*pow(0.1,float(num_anneal_steps)/float(tt+1))
            
                # Run annealed-model Kalman filter
                flt,_,old_tmp_lhood = annealed_model.annealed_kalman_filter(self.observ, anfact)
            
                # Annealed backward simulation
                x = annealed_model.annealed_backward_simulation(flt, anfact)
            
                # Sample transition matrices
                annealed_model.sample_annealed_transition_matrixes(x, self.A_prior_vr, anfact)
                
                # Run annealed-model Kalman filter
                _,_,new_tmp_lhood = annealed_model.annealed_kalman_filter(self.observ, anfact)
                
                # Calculate tempering term
                anneal_ratios.append(old_tmp_lhood-new_tmp_lhood)
        
            # New likelihood
            _,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            new_cp_prior = ppsl_model.cp_prior(self.cp_prior_rate)
            anneal_term = sum(anneal_ratios)
        
            ap = (new_lhood+new_cp_prior) - (old_lhood+old_cp_prior) + (bck_ppsl-fwd_ppsl) + anneal_term
        else:
            ap = -np.Infinity
            
        u = np.log(np.random.random_sample())
        print("    Acceptance probability: {}".format(ap))
        if u < ap:
            self.model = ppsl_model
            print("    Accepted")
        else:
            print("    Rejected")
            
            
            

        
class AnnealedLinearChangepointModel:
    
    def __init__(self, move_type, old_model, new_model, padded_point, padded_system):
        
        self.system = padded_system
        
        if move_type == "add":
            self.old_point = padded_point
            self.new_point = new_model.point
        elif move_type == "remove":
            self.old_point = old_model.point
            self.new_point = padded_point
        elif (move_type == "move") or (move_type == "adjust"):
            self.old_point = old_model.point
            self.new_point = new_model.point
        else:
            raise ValueError("Invalid move type")
        
        # Stuff which shouldn't get changed
        self.K = old_model.K
        self.ds = old_model.ds
        self.P1 = old_model.P1
        self._Q = old_model._Q
        self._H = old_model._H
        self._R = old_model._R
        
    @property
    def Nm(self):
        """Number of models"""
        return len(self.system)
        
    @property
    def Nc(self):
        """Number of changepoints"""
        return len(self.point)
        
    def model_indexes_by_frame(self, cp_list):
        """Returns the index of the model to be used for each frame"""
        idx_list = []
        nn = 0
        for kk in range(self.K):
            if kk in cp_list:
                nn = nn + cp_list.count(kk)
            idx_list.append(nn)
        return idx_list
        
    def stage_index_limits(self, n, cp_list):
        """Return the indexes of the first observation time at which the (n)th model is used, and the first at which it is NOT used"""
        if n == 0:
            first_idx = 0
        else:
            first_idx = cp_list[n-1]
        if n < self.Nm-1:
            last_idx = cp_list[n]
        else:
            last_idx = self.K
        return first_idx,last_idx
        
    def annealed_kalman_filter(self, observ, anfact):
        """Run a Kalman filter on an annealed composite of two changepoint models"""
        K = self.K
        flt = GaussianDensity.new_sequence(self.ds, K)
        prd = GaussianDensity.new_sequence(self.ds, K)
        
        idx_list1 = self.model_indexes_by_frame(self.old_point)
        idx_list2 = self.model_indexes_by_frame(self.new_point)
        lhood_seq1 = np.zeros(K)
        lhood_seq2 = np.zeros(K)
        
        # Loop through frames
        for kk in range(K):
            if kk > 0:
                if idx_list1[kk]==idx_list2[kk]:
                    prd[kk] = kal.predict(flt[kk-1], self.system[idx_list1[kk]].F, self._Q)
                else:
                    # Annealed Kalman filter prediction
                    F1 = self.system[idx_list1[kk]].F
                    F2 = self.system[idx_list2[kk]].F
                    prd[kk],prdPrec = kal.annealed_predict(flt[kk-1], F1, F2, self._Q, anfact)
                    fltPm = la.solve(flt[kk-1].vr,flt[kk-1].mn)
                    prdPm = la.solve(prd[kk].vr,prd[kk].mn)
                    lhood_seq2[kk] = 0.5*( np.dot(prd[kk].mn.T, prdPm ) - np.dot( fltPm.T, np.dot( flt[kk-1].vr-la.inv(prdPrec) ,fltPm) ) )\
                                    -0.5*np.log(la.det(prdPrec)) + 0.5*np.log(la.det(prd[kk].vr)) - 0.5*np.log(la.det(flt[kk-1].vr))
            else:
                # First frame
                prd[kk] = self.P1
            flt[kk],innov = kal.correct(prd[kk], observ[kk], self._H, self._R)
            lhood_seq1[kk] = mvn.logpdf(observ[kk], innov.mn, innov.vr)
#        print('')
#        print(lhood_seq1)
#        print('')
#        print(lhood_seq2)
#        print('')
        lhood = sum(lhood_seq1) + sum(lhood_seq2)
#        print(sum(lhood_seq1))
#        print(sum(lhood_seq2))
        return flt,prd,lhood
        
    def annealed_backward_simulation(self, flt, anfact):
        """Run backward simulations on an annealed composite of two changepoint models"""
        idx_list1 = self.model_indexes_by_frame(self.old_point)
        idx_list2 = self.model_indexes_by_frame(self.new_point)
        x = Series.new_sequence(self.ds, self.K)
        for kk in reversed(range(self.K)):
            if kk < self.K-1:
                if idx_list1[kk+1]==idx_list2[kk+1]:
                    samp_dens,_ = kal.correct(flt[kk], x[kk+1], self.system[idx_list1[kk+1]].F, self._Q)
                else:
                    F1 = self.system[idx_list1[kk+1]].F
                    F2 = self.system[idx_list2[kk+1]].F
                    samp_dens = kal.annealed_update(flt[kk], x[kk+1], F1, F2, self._Q, anfact)
            else:
                samp_dens = flt[kk]
            x[kk] = mvn.rvs(samp_dens.mn,samp_dens.vr)
        return x
    
    def sample_annealed_transition_matrixes(self, x, A_prior_vr, anfact):
        
        for nn in range(self.Nm):
            f_idx1,l_idx1 = self.stage_index_limits(nn, self.old_point)
            f_idx2,l_idx2 = self.stage_index_limits(nn, self.new_point)
            
            if not((f_idx1,l_idx1)==(f_idx2,l_idx2)):
                Q = self._Q
                if anfact < 1.0:
                    self.system[nn].F = sysl.sample_annealed_transition_matrix_conditional(x, (f_idx1,l_idx1), (f_idx2,l_idx2), Q, A_prior_vr, anfact)
                else:
                    self.system[nn].F,_ = sysl.sample_transition_matrix_conditional(x[f_idx2:l_idx2], Q, A_prior_vr)
                    
