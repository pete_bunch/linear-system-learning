import matplotlib.pyplot as plt
import numpy as np
import system_learning as sysl
import kalman as kal
from scipy import linalg as la
from scipy.stats import multivariate_normal as mvn
from kalman import GaussianDensity, Series


class LinearModel:
    """Linear-Gaussian Model Description Class"""
    
    def __init__(self, P1, F, G, H, R):
        #Some error checking here would be good
        self.F = F.copy()           # Transition matrix
        self.G = G.copy()           # Transition noise matrix
#        self.Q = Q.copy()           # Transition covariance matrix
        self.H = H.copy()           # Observation matrix
        self.R = R.copy()           # Observation variance
        self.P1 = P1.copy()         # First state prior
        self.ds = F.shape[1]        # State dimension
        
    def copy(self):
        return LinearModel(self.P1, self.F, self.G, self.H, self.R)

    @property
    def Q(self):
        """Transition Covariance Matrix"""
        return np.dot(self.G,self.G.T)
    @Q.setter
    def Q(self, value):
        raise AttributeError("Can't set attribute")        
    @Q.deleter
    def Q(self):
        raise AttributeError("Can't delete attribute")

    def simulate_data(self, K):
        """Simulate artificial data from the model"""
        state = self.sample_prior(K)
        observ = self.sample_observ(state)
        return state, observ

    def sample_prior(self, K):
        """Sample from the state prior, running forwards in time"""
        state = Series.new_sequence(self.ds, K)
        state[0]  = mvn.rvs( mean=self.P1.mn, cov=self.P1.vr )
        for kk in range(1,K):
            state[kk]  = mvn.rvs( mean=np.dot(self.F,state[kk-1]), cov=self.Q )
        return state

    def sample_observ(self, state):
        """Sample an observation for each state"""#(only really useful for simulate_data - perhaps merge in)
        K = len(state)
        observ = Series.new_sequence(self.ds, K)
        for kk in range(K):
            observ[kk] = mvn.rvs( mean=np.dot(self.H,state[kk]), cov=self.R )
        return observ

    def kalman_filter(self, observ):
        """Run a Kalman filter using the model on a set of observations and return estimates as mean and covariance."""
        K = len(observ)
        flt = GaussianDensity.new_sequence(self.ds, K)
        prd = GaussianDensity.new_sequence(self.ds, K)
        lhood = 0
        for kk in range(K):
            if kk > 0:
                prd[kk] = kal.predict(flt[kk-1], self.F, self.Q)
            else:
                prd[kk] = self.P1
            flt[kk],innov = kal.update(prd[kk], observ[kk], self.H, self.R)
            lhood = lhood + mvn.logpdf(observ[kk], innov.mn, innov.vr)
        return flt, prd, lhood

    def rts_smoother(self, flt, prd):
        """Run a Rauch-Tung-Striebel smoother on the Kalman filter output using the model"""
        K = len(flt)
        smt = GaussianDensity.new_sequence(self.ds, K)
        for kk in reversed(range(K)):
            if kk<K-1:
                smt[kk] = kal.correct(flt[kk], smt[kk+1], prd[kk+1], self.F)
            else:
                smt[kk] = flt[kk]
        return smt
        
    def backward_simulation(self, flt):
        """Use backward simulation to sample from the state joint posterior"""
        K = len(flt)
        x = Series.new_sequence(self.ds, K)
        for kk in reversed(range(K)):
            if kk < K-1:
                samp_dens,_ = kal.update(flt[kk], x[kk+1], self.F, self.Q)
            else:
                samp_dens = flt[kk]
            x[kk] = mvn.rvs(mean=samp_dens.mn, cov=samp_dens.vr)
        return x

    def sample_posterior(self, observ):
        """Sample a state trajectory from the joint smoothing distribution (i.e KF then BS)"""
        flt,_,_ = self.kalman_filter(observ)
        x = self.backward_simulation(flt)
        return x
        
    def sample_noise_posterior(self, observ):
        """Sample a noise trajectory from the joint smoothing distribution (i.e KF then BS)"""
        K = len(observ)
        flt,_,_ = self.kalman_filter(observ)
        x = self.backward_simulation(flt)
        x0 = x[0]
        u = Series.new_sequence(self.ds,K)
        for kk in range(1,K):
            u[kk] = la.solve( self.G, (x[kk]-np.dot(self.F,x[kk-1])) )
        #x_verify = Series.new_sequence(self.ds,K)
        #x_verify[0] = x[0].copy()
        #for kk in range(1,K):
        #    x_verify[kk] = np.dot(self.F,x_verify[kk-1]) + np.dot(self.G,u[kk])
        #    print(x_verify[kk])
        #    print(x[kk])
        #    print(x_verify[kk]-x[kk])
        #    assert(np.all(np.abs(x_verify[kk]-x[kk])<1E-6))
        return x0,u
        


class SparseLinearModel(LinearModel):
    """Sparse Linear-Gaussian Model Description Class"""
    
    def __init__(self, P1, A, B, G, H, R):
        #Some error checking here would be good
        self.A = A.copy()           # Dense transition matrix
        self.B = B.copy()           # Transition matrix mask
        self.G = G.copy()           # Transition noise matrix
        self.H = H.copy()           # Observation matrix
        self.R = R.copy()           # Observation variance
        self.P1 = P1.copy()         # First state prior
        self.ds = A.shape[1]        # State dimension
        
    def copy(self):
        return SparseLinearModel(self.P1, self.A, self.B, self.G, self.H, self.R)
    
    @property
    def F(self):
        """Sparse Transition Matrix"""
        return np.multiply(self.A,self.B)
    @F.setter
    def F(self, value):
        raise AttributeError("Can't set attribute")        
    @F.deleter
    def F(self):
        raise AttributeError("Can't delete attribute")




class LinearModelLearner:
    """Worker Class for Linear-Gaussian Model Learning"""

    def __init__(self, init_model_est, observ, A_prior_vr, C_prior_vr, H_prior_vr):
        self.model = init_model_est.copy()
        self.chain = []
        self.chain.append(self.model.copy())
        self.observ = observ
        self.A_prior_vr = A_prior_vr
        self.C_prior_vr = C_prior_vr
        self.H_prior_vr = H_prior_vr
        
    def save_link(self):
        """Save the current state of the model as a link in the chain"""
        self.chain.append(self.model.copy())
        
    def draw_chain_histogram(self, true_model, burn_in):
        d = min(3,self.model.ds) # Don't plot more than 3x3. Too small
        
        if len(self.chain)>burn_in:
            # F
            print("F")
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.F[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
                    ax[rr,cc].set_xlim((-2,2))
                    
            # Q
            print("Q")
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.Q[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-1,2))
                
            # |Q|
            print("|Q|")
            _,ax = plt.subplots(1,1)
            ax.hist([la.det(mm.Q) for mm in self.chain[burn_in:]])
            ax.plot([la.det(true_model.Q)]*2, [0,len(self.chain[burn_in:])], '-r')
        
#            # H
#            print("H")
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm.H[rr,cc] for mm in self.chain[burn_in:]])
#                    ax[rr,cc].plot([true_model.H[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-2,2))
#                
#            # HFH^T
#            print("HFH'")
#            HFprod_chain = [np.dot(np.dot(mm.H,mm.F),mm.H.T) for mm in self.chain]
#            HFprod_true = np.dot(np.dot(true_model.H,true_model.F),true_model.H.T)
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm[rr,cc] for mm in HFprod_chain[burn_in:]])
#                    ax[rr,cc].plot([HFprod_true[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-4,4))
#                    
#            # HG
#            print("HGG'H'")
#            HGprod_chain = [np.dot(np.dot(mm.H,mm.G),np.dot(mm.H,mm.G).T) for mm in self.chain]
#            HGprod_true = np.dot(np.dot(true_model.H,true_model.G),np.dot(true_model.H,true_model.G).T)
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm[rr,cc] for mm in HGprod_chain[burn_in:]])
#                    ax[rr,cc].plot([HGprod_true[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-4,4))
        

    def iterate_transition_matrix(self, Mtimes):
        """Run Gibbs iterations on transition matrix"""
        
        for mm in range(Mtimes):
        
            # First sample the state sequence
            x = self.model.sample_posterior(self.observ)
            
            # Sample a new transition matrix
            F = sysl.sample_transition_matrix_conditional(x, self.model.Q, self.A_prior_vr)
            self.model.F = F
            
    def iterate_observation_matrix(self, Mtimes):
        """Run Gibbs iterations on observation matrix"""
        
        for mm in range(Mtimes):
        
            # First sample the state sequence
            x = self.model.sample_posterior(self.observ)
            
            # Sample a new transition matrix
            H = sysl.sample_observation_matrix_conditional(x, self.observ, self.model.R, self.H_prior_vr)
            self.model.H = H
            
    def iterate_transition_noise_matrix(self, Mtimes):
        """Run Gibbs iterations on the transition noise matrix"""
        
        for mm in range(Mtimes):
            
            # First sample the noise sequence
            x0,u = self.model.sample_noise_posterior(self.observ)
            
            # Sample a new transition noise matrix
            G = sysl.sample_transition_noise_matrix_conditional(x0, u, self.model.F, self.model.H, self.model.R, self.observ, self.C_prior_vr)
            self.model.G = G
            
#            # First sample the state sequence
#            x = self.model.sample_posterior(self.observ)
#            
#            # Sample a new covariance matrix
#            Q = sysl.sample_transition_covariance_conditional(x, self.model.F, 0, np.identity(self.model.ds))
#
##            self.model.Q = Q;            
#            
#            # Square root it
#            G = la.cholesky(Q,lower=True)
#            self.model.G = G
            
            
class SparseLinearModelLearner(LinearModelLearner):  
    """Worker Class for Sparse Linear-Gaussian Model Learning"""
    
    def __init__(self, init_model_est, observ, A_prior_vr, C_prior_vr, H_prior_vr, B_prior_prob):
        self.model = init_model_est.copy()
        self.chain = []
        self.chain.append(self.model.copy())
        self.observ = observ
        self.A_prior_vr = A_prior_vr
        self.C_prior_vr = C_prior_vr
        self.H_prior_vr = H_prior_vr
        self.B_prior_prob = B_prior_prob
    
    def draw_chain_histogram(self, true_model, burn_in):
        d = min(3,self.model.ds) # Don't plot more than 3x3. Too small
        
        if len(self.chain)>burn_in:
            # F
            print("F")
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.F[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
                    ax[rr,cc].set_xlim((-2,2))
                    
            # Q
            print("Q")
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.Q[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-1,2))
                
            # |Q|
            print("|Q|")
            _,ax = plt.subplots(1,1)
            ax.hist([la.det(mm.Q) for mm in self.chain[burn_in:]])
            ax.plot([la.det(true_model.Q)]*2, [0,len(self.chain[burn_in:])], '-r')
        
#            # H
#            print("H")
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm.H[rr,cc] for mm in self.chain[burn_in:]])
#                    ax[rr,cc].plot([true_model.H[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-2,2))
#                
#            # HFH^T
#            print("HFH'")
#            HFprod_chain = [np.dot(np.dot(mm.H,mm.F),mm.H.T) for mm in self.chain]
#            HFprod_true = np.dot(np.dot(true_model.H,true_model.F),true_model.H.T)
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm[rr,cc] for mm in HFprod_chain[burn_in:]])
#                    ax[rr,cc].plot([HFprod_true[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-4,4))
#                    
#            # HG
#            print("HGG'H'")
#            HGprod_chain = [np.dot(np.dot(mm.H,mm.G),np.dot(mm.H,mm.G).T) for mm in self.chain]
#            HGprod_true = np.dot(np.dot(true_model.H,true_model.G),np.dot(true_model.H,true_model.G).T)
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm[rr,cc] for mm in HGprod_chain[burn_in:]])
#                    ax[rr,cc].plot([HGprod_true[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-4,4))
        
        # B
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.B[rr,cc] for mm in self.chain[burn_in:]])
                ax[rr,cc].plot([true_model.B[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
                ax[rr,cc].set_xlim((-0.1,1.1))
                
    
    def iterate_transition_matrix(self, Mtimes):
        """Run Gibbs iterations on transition matrix"""
        
        for mm in range(Mtimes):
        
            # First sample the state sequence
            x = self.model.sample_posterior(self.observ)
            
            # Sample continuous dense part
            self.model.A,_ = sysl.sample_transition_matrix_conditional(x, self.model.Q, self.A_prior_vr, self.model.B)
            
#            # Sample the state sequence again
#            x = self.model.sample_posterior(self.observ)
            
#            # Sample binary mask
#            self.model.B = sysl.sample_transition_matrix_mask_conditional(x, self.model.A, self.model.B, self.model.Q, self.B_prior_prob)
            
            # Sample A,B pairs jointly
            self.model.A,self.model.B = sysl.sample_transition_matrix_and_mask_conditional(x, self.model.A, self.model.B, self.model.Q, self.A_prior_vr, self.B_prior_prob)
#            for nn in range(9):
#                x = self.model.sample_posterior(self.observ)
#                A,B = sysl.sample_transition_matrix_and_mask_conditional(x, self.model.A.copy(), self.model.B.copy(), self.model.Q, self.A_prior_vr, self.B_prior_prob)
#                self.model.A = A
#                self.model.B = B
