import numpy as np
import matplotlib.pyplot as plt
from linear_model import LinearModel, LinearModelLearner, SparseLinearModel, SparseLinearModelLearner
from kalman import GaussianDensity, Series
import system_learning as sysl

plt.close("all")
np.random.seed(1)

sparse = True
K = 100
d=3

A_prior_vr = 10*np.identity(pow(d,2))
H_prior_vr = np.identity(pow(d,2))
C_prior_vr = np.identity(pow(d,2))
B_prior_prob = 0.5

#A = sysl.random_transition_matrix(d, A_prior_vr)
#A = np.array([[0.9,1],[0,0.9]])
#A = np.array([[0.9,0.5],[0,0.7]])
#A = np.array([[1,1],[0,1]])
#B = np.array([[1,1],[0,1]])
#A = np.array([[0.5,0.1,0.1],[0,0.5,0.1],[0,0,0.5]])
A = np.array([[0.9,0.8,0.7],[0,0.9,0.5],[0,0,0.8]])
#A = np.array([[1,1,1],[0,1,1],[0,0,1]])
#A = np.array([[1.1,0.5,0.2],[0,1,0.5],[0,0,1]])
B = np.array([[1,1,1],[0,1,1],[0,0,1]])
#B = sysl.sample_transition_matrix_mask_prior(d, B_prior_prob)

G = 0.5*np.identity(d) + 0.5*np.ones((d,d))
#G = np.ones((d,d))

#G = np.identity(d)
#Q = np.identity(d)
H = np.identity(d)
R = 0.01*np.identity(d)
#H = np.array([[1,1],[0,1]])
#R = 0.1*np.identity(d)
#H = np.array([[1,1],[1,0],[0,1]])
#R = 0.1*np.identity(3)
#H = np.array([[1,0]])
#R = 0.01*np.identity(1)

m0 = np.zeros(d)
P0 = 1*np.identity(d)

# Create necessary objects
first_state_prior = GaussianDensity(m0,P0)
if sparse:
    model = SparseLinearModel(first_state_prior, A, B, G, H, R)
else:
    model = LinearModel(first_state_prior, A, G, H, R)
#if sparse:
#    model = SparseLinearModel(first_state_prior, A, B, Q, H, R)
#else:
#    model = LinearModel(first_state_prior, A, Q, H, R)

# Generate data
state, observ = model.simulate_data(K)

# Create an estimate of the linear system and create learning object
#A_est = sysl.sample_transition_matrix_prior(A_prior_vr)
A_est = np.zeros((d,d))
B_est = np.zeros((d,d))
G_est = np.identity(d)
H_est = np.identity(d)

if sparse:
    init_model_est = SparseLinearModel(first_state_prior, A_est, B_est, G, H_est, R)
    brain = SparseLinearModelLearner(init_model_est, observ, A_prior_vr, C_prior_vr, H_prior_vr, B_prior_prob)
else:
    init_model_est = LinearModel(first_state_prior, A_est, G, H_est, R)
    brain = LinearModelLearner(init_model_est, observ, A_prior_vr, C_prior_vr, H_prior_vr)

f = plt.figure().add_subplot(1,1,1)
Series.plot_sequence(state, 1, f, 'k')

# Gibbs sample the transition matrix
for ii in range(100):
    brain.iterate_transition_matrix(1)
#    brain.iterate_transition_noise_matrix(1)
    #brain.iterate_observation_matrix(1)
    print("")
    print("Iteration number %u" % (ii))
    print("Transition matrix:")
    print(brain.model.F)
    #print("Observation matrix:")
    #print(brain.model.H)
    print("Transition variance:")
    print(brain.model.Q)
    print("Transition variance eigenvalues:")
    print(np.linalg.eigh(brain.model.Q)[0])
    brain.save_link()

print("")
brain.draw_chain_histogram(model, 50)
