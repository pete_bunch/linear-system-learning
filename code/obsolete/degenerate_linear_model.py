import matplotlib.pyplot as plt
import numpy as np
import system_learning as sysl
import kalman as kal
from scipy import linalg as la
from scipy import stats
from scipy.stats import multivariate_normal as mvn
from kalman import GaussianDensity, Series


class DegenerateLinearModel:
    """Linear-Gaussian Model Description Class"""
    
    ### BASIC CREATION AND COPYING ###
    
    def __init__(self, P1, F, D, givens, H, R):
        #Some error checking here would be good
        self.F = F.copy()           # Transition matrix
        self.H = H.copy()           # Observation matrix
        self.R = R.copy()           # Observation variance
        self.P1 = P1.copy()         # First state prior
        self.ds = F.shape[0]        # State dimension
        self.rank = D.shape[0]      # Rank of noise
        
        self._D = D.copy()              # Postive definite part of transition noise matrix
        self._givens = tuple(givens)    # Givens rotations for the orthogonal part of transition noise matrix
        self.U_update()
        
    def copy(self):
        return DegenerateLinearModel(self.P1, self.F, self.D, self.givens, self.H, self.R)

    ### SYSTEM MATRIXES ###

    @property
    def G(self):
        """Transition Noise Matrix"""
        return self._G
    @G.setter
    def G(self, value):
        raise AttributeError("Can't set attribute")        
    @G.deleter
    def G(self):
        raise AttributeError("Can't delete attribute")

    @property
    def Q(self):
        """Transition Covariance Matrix"""
        return np.dot(self.G,self.G.T)
    @Q.setter
    def Q(self, value):
        raise AttributeError("Can't set attribute")        
    @Q.deleter
    def Q(self):
        raise AttributeError("Can't delete attribute")
    
    @property
    def givens(self):
        """Givens rotations parameterising orthogonal part of G"""
        return self._givens
    @givens.setter
    def givens(self, value):
        self._givens = value
        self.U_update()
    @givens.deleter
    def givens(self):
        raise AttributeError("Can't delete attribute")
    def set_givens(self, gg, R):
        """Update a single givens rotation"""
        Ng = len(self._givens)
        self._givens = self._givens[:gg] + (R,) + self._givens[gg+1:]
        if len(self._givens)!=Ng:
            raise ValueError("Number of givens rotations must remain constant")
        self.U_update()
        
    @property
    def U(self):
        """Orthogonal part of transition noise matrix"""
        return self._U        
    @U.setter
    def U(self, value):
        raise AttributeError("Can't set attribute")        
    @U.deleter
    def U(self):
        raise AttributeError("Can't delete attribute")
    
    @property
    def D(self):
        """Positive definite part of G"""
        return self._D
    @D.setter
    def D(self, value):
        self._D = value
        self.G_update()
    @D.deleter
    def D(self):
        raise AttributeError("Can't delete attribute")

    def G_update(self):
        """Recalculate G from its parameters"""
        self._G = np.dot( self._U, np.vstack( (self._D, np.zeros((self.ds-self.rank,self.rank))) ) ) 
    
    def U_update(self):
        """Recalculate orthogonal matrix when givens rotations are changed"""
        M = np.identity(self.ds)
        gg = 0
        for ii in range(self.rank):
            for jj in range(self.rank,self.ds):
                rot = np.identity(self.ds)
                rot[ii,ii] = np.cos(self.givens[gg])
                rot[jj,jj] = np.cos(self.givens[gg])
                rot[ii,jj] = np.sin(self.givens[gg])
                rot[jj,ii] =-np.sin(self.givens[gg])
                M = np.dot( M, rot)
                gg += 1
        self._U = M
        self.G_update()

    ### SAMPLING ###
    
    def simulate_data(self, K):
        """Simulate artificial data from the model"""
        state = self.sample_prior(K)
        observ = self.sample_observ(state)
        return state, observ

    def sample_prior(self, K):
        """Sample from the state prior, running forwards in time"""
        state = Series.new_sequence(self.ds, K)
        state[0]  = mvn.rvs( mean=self.P1.mn, cov=self.P1.vr )
        for kk in range(1,K):
            state[kk]  = mvn.rvs( mean=np.dot(self.F,state[kk-1]), cov=self.Q )
        return state

    def sample_observ(self, state):
        """Sample an observation for each state"""
        K = len(state)
        observ = Series.new_sequence(self.ds, K)
        for kk in range(K):
            observ[kk] = mvn.rvs( mean=np.dot(self.H,state[kk]), cov=self.R )
        return observ

    def kalman_filter(self, observ):
        """Run a Kalman filter on a set of observations"""
        K = len(observ)
        flt = GaussianDensity.new_sequence(self.ds, K)
        prd = GaussianDensity.new_sequence(self.ds, K)
        lhood = 0
        for kk in range(K):
            if kk > 0:
                prd[kk] = kal.predict(flt[kk-1], self.F, self.Q)
            else:
                prd[kk] = self.P1
            flt[kk],innov = kal.correct(prd[kk], observ[kk], self.H, self.R)
            lhood = lhood + mvn.logpdf(observ[kk], innov.mn, innov.vr)
        return flt, prd, lhood

    def rts_smoother(self, flt, prd):
        """Run a Rauch-Tung-Striebel smoother on the Kalman filter output"""
        K = len(flt)
        smt = GaussianDensity.new_sequence(self.ds, K)
        for kk in reversed(range(K)):
            if kk<K-1:
                smt[kk] = kal.update(flt[kk], smt[kk+1], prd[kk+1], self.F)
            else:
                smt[kk] = flt[kk]
        return smt
        
    def backward_simulation(self, flt):
        """Use backward simulation to sample from the state joint posterior"""
        K = len(flt)
        x = Series.new_sequence(self.ds, K)
        for kk in reversed(range(K)):
            if kk < K-1:
                samp_dens,_ = kal.correct(flt[kk], x[kk+1], self.F, self.Q)
            else:
                samp_dens = flt[kk]
            x[kk] = mvn.rvs(mean=samp_dens.mn, cov=samp_dens.vr)
        return x

    def sample_posterior(self, observ):
        """Sample a state trajectory from the joint smoothing distribution"""
        flt,_,_ = self.kalman_filter(observ)
        x = self.backward_simulation(flt)
        return x
        



class DegenerateLinearModelLearner:
    """Worker Class for Linear-Gaussian Model Learning"""

    def __init__(self, init_model_est, observ, A_prior_vr, C_prior_vr, H_prior_vr, giv_std, pad_std):
        self.model = init_model_est.copy()
        self.chain = []
        self.chain.append(self.model.copy())
        self.observ = observ
        self.A_prior_vr = A_prior_vr
        self.C_prior_vr = C_prior_vr
        self.H_prior_vr = H_prior_vr
        self.giv_std = [giv_std]*len(self.model.givens)
        self.pad_std = pad_std
        
        self.Facc = []
        self.pad_std_chain = []
        
        self.Gacc = []
        self.givens_acc = [[] for gg in range(len(self.model.givens))]
        self.giv_std_chain = [[] for gg in range(len(self.model.givens))]
        for gg in range(len(self.model.givens)):
            self.giv_std_chain[gg].append(self.giv_std[gg])
        
    def save_link(self):
        """Save the current state of the model as a link in the chain"""
        self.chain.append(self.model.copy())
        self.pad_std_chain.append(self.pad_std)
        for gg in range(len(self.model.givens)):
            self.giv_std_chain[gg].append(self.giv_std[gg])
        
    def draw_chain_histogram(self, true_model, burn_in):
        d = min(3,self.model.ds) # Don't plot more than 3x3. Too small
        
        if len(self.chain)>burn_in:
            # F
            print("F")
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.F[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
                    ax[rr,cc].set_xlim((-2,2))

            # D
            print("D")
            d = min(3,self.model.rank)
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.D[rr,cc] for mm in self.chain[burn_in:]])
                    ax[rr,cc].plot([true_model.D[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-1,2))
                    
            # givens
            print("givens")
            _,ax = plt.subplots(1,len(self.model.givens),squeeze=False)
            for cc in range(len(self.model.givens)):
                ax[0,cc].hist([mm.givens[cc] for mm in self.chain[burn_in:]])
                ax[0,cc].plot([true_model.givens[cc]]*2, [0,len(self.chain[burn_in:])], '-r')
                ax[0,cc].set_xlim((-np.pi,np.pi))
                    
#            # Q
#            print("Q")
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm.Q[rr,cc] for mm in self.chain[burn_in:]])
#                    ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
##                    ax[rr,cc].set_xlim((-1,2))
                
#            # |Q|
#            print("|Q|")
#            _,ax = plt.subplots(1,1)
#            ax.hist([la.det(mm.Q) for mm in self.chain[burn_in:]])
#            ax.plot([la.det(true_model.Q)]*2, [0,len(self.chain[burn_in:])], '-r')
        
#            # H
#            print("H")
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm.H[rr,cc] for mm in self.chain[burn_in:]])
#                    ax[rr,cc].plot([true_model.H[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-2,2))
#                
#            # HFH^T
#            print("HFH'")
#            HFprod_chain = [np.dot(np.dot(mm.H,mm.F),mm.H.T) for mm in self.chain]
#            HFprod_true = np.dot(np.dot(true_model.H,true_model.F),true_model.H.T)
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm[rr,cc] for mm in HFprod_chain[burn_in:]])
#                    ax[rr,cc].plot([HFprod_true[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-4,4))
#                    
#            # HG
#            print("HGG'H'")
#            HGprod_chain = [np.dot(np.dot(mm.H,mm.G),np.dot(mm.H,mm.G).T) for mm in self.chain]
#            HGprod_true = np.dot(np.dot(true_model.H,true_model.G),np.dot(true_model.H,true_model.G).T)
#            _,ax = plt.subplots(d,d,squeeze=False)
#            for rr in range(d):
#                for cc in range(d):
#                    ax[rr,cc].hist([mm[rr,cc] for mm in HGprod_chain[burn_in:]])
#                    ax[rr,cc].plot([HGprod_true[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                    ax[rr,cc].set_xlim((-4,4))
        
    def iterate_transition_matrix(self, Mtimes):
        """Run Gibbs iterations on transition matrix"""
        
        ds = self.model.ds
        
        for mm in range(Mtimes):
            
            # KF to get the likelihood
            flt,_,old_lhood = self.model.kalman_filter(self.observ)
            
            # Sample a state trajectory
            x = self.model.backward_simulation(flt)
            
            # Pad the degenerate covariance matrix
            padding = (self.pad_std**2)*np.identity(self.model.ds)
            Qpad = self.model.Q + padding
            
            # Sample a new transition matrix
            Fppsl,F_fwd_ppsl_prob = sysl.sample_transition_matrix_conditional(x, Qpad, self.A_prior_vr)
            Fold = self.model.F.copy()
            self.model.F = Fppsl
            
            # New likelihood
            ppsl_flt,_,new_lhood = self.model.kalman_filter(self.observ)            
            
            # Sample a new state sequence
            xppsl = self.model.backward_simulation(ppsl_flt)
#            xppsl = self.model.sample_posterior(self.observ)
            
            # Reverse proposal
            _,F_bwd_ppsl_prob = sysl.sample_transition_matrix_conditional(xppsl, Qpad, self.A_prior_vr, A=Fold)
            
            # Priors
            new_prior = mvn.logpdf( Fppsl.flatten(), np.zeros((ds**2)), self.A_prior_vr )
            old_prior = mvn.logpdf( Fold.flatten(),  np.zeros((ds**2)), self.A_prior_vr )
            
            # Accept?
            ap = (new_lhood+new_prior) - (old_lhood+old_prior) + (F_bwd_ppsl_prob-F_fwd_ppsl_prob)
            if np.log(np.random.random()) < ap:
                self.Facc.append(1)
                
                pass
            else:
                self.model.F = Fold
                self.Facc.append(0)
            
            if sum(self.Facc)>1:
                # Sample within subspace
                x = self.model.sample_posterior(self.observ)
                F = sysl.sample_degenerate_transition_matrix_conditional(x, self.model.F, np.dot(self.model.D,self.model.D.T), self.model.U, self.A_prior_vr)
                self.model.F = F
            
            
            
            
            
            
            
            
            
    def iterate_observation_matrix(self, Mtimes):
        """Run Gibbs iterations on observation matrix"""
        
        for mm in range(Mtimes):
        
            # First sample the state sequence
            x = self.model.sample_posterior(self.observ)
            
            # Sample a new transition matrix
            H = sysl.sample_observation_matrix_conditional(x, self.observ, self.model.R, self.H_prior_vr)
            self.model.H = H
    
    def iterate_transition_noise_matrix(self, Mtimes):
        """Run Gibbs iterations on the transition noise matrix and MH on the givens rotations"""
        
        for mm in range(Mtimes):
            
            # First sample the state sequence
            flt,_,old_lhood = self.model.kalman_filter(self.observ)
            x = self.model.backward_simulation(flt)
#            x = self.model.sample_posterior(self.observ)
            
            # Sample a new covariance matrix
            self.model.D = sysl.sample_degenerate_transition_noise_matrix_conditional(x, self.model.F, self.model.rank, self.model.U, 0, np.identity(self.model.rank))
#            print(D)
            
            if self.model.rank < self.model.ds:
            
                ### Sample givens rotations ###
                
                for gg in range(len(self.model.givens)):
                
                    # Store existing set
                    old_ga = self.model.givens[gg]
                    
                    # Proposal standard deviation
                    gstd = self.giv_std[gg]
                    
                    # Propose a change
#                    self.model.givens[gg] = (self.model.givens[gg]+np.random.normal(0,gstd) + np.pi/2) % np.pi - np.pi/2
                    new_ga = (self.model.givens[gg]+np.random.normal(0,gstd) + np.pi/2) % np.pi - np.pi/2
                    self.model.set_givens(gg, new_ga)
                    
                    # Likelihood
                    _,_,new_lhood = self.model.kalman_filter(self.observ)
                
                    ap = (new_lhood-old_lhood)
                    
                    # Accept/Reject
                    if np.log(np.random.random()) < ap:    
                        self.givens_acc[gg].append(1)
                        old_lhood = new_lhood
                        pass
                    else:
#                        self.model.givens = old_givens
                        self.model.set_givens(gg, old_ga)
                        self.givens_acc[gg].append(0)
                
                


    def adapt_std(self, B):
        
        ar = np.mean(np.array(self.Facc[-B:]))
        bn = len(self.Facc)/B
        delta = min(0.1,1/np.sqrt(bn))
        if (ar<0.44):
            self.pad_std = self.pad_std*np.exp(-delta)
        else:
            self.pad_std = self.pad_std*np.exp(delta)        
        
        for gg in range(len(self.model.givens)):
            ar = np.mean(np.array(self.givens_acc[gg][-B:]))
            bn = len(self.givens_acc[gg])/B
            delta = min(0.1,1/np.sqrt(bn))
            if (ar<0.44):
                self.giv_std[gg] = self.giv_std[gg]*np.exp(-delta)
            else:
                self.giv_std[gg] = self.giv_std[gg]*np.exp(delta)
        
            

def draw_chain_histogram(self, true_model, burn_in):
    d = min(4,self.model.ds) # Don't plot more than 3x3. Too small
    
    if len(self.chain)>burn_in:
        # F
        print("F")
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.F[rr,cc] for mm in self.chain[burn_in:]])
                ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
                ax[rr,cc].set_xlim((-2,2))
                
        # Q
        print("Q")
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.Q[rr,cc] for mm in self.chain[burn_in:]])
                ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                ax[rr,cc].set_xlim((-2,2))

        # D
        print("D")
        d = min(3,self.model.rank)
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.D[rr,cc] for mm in self.chain[burn_in:]])
                ax[rr,cc].plot([true_model.D[rr,cc]]*2, [0,len(self.chain[burn_in:])], '-r')
#                ax[rr,cc].set_xlim((-1,2))
                    
        # givens
        print("givens")
        _,ax = plt.subplots(1,len(self.model.givens),squeeze=False)
        for cc in range(len(self.model.givens)):
            ax[0,cc].hist([mm.givens[cc] for mm in self.chain[burn_in:]])
            ax[0,cc].plot([true_model.givens[cc]]*2, [0,len(self.chain[burn_in:])], '-r')
            ax[0,cc].set_xlim((-np.pi,np.pi))
            
            
