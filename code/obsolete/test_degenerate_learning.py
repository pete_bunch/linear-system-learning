import numpy as np
import matplotlib.pyplot as plt
from degenerate_linear_model import DegenerateLinearModelLearner
from linear_models import DegenerateLinearModel
from kalman import GaussianDensity, Series
import system_learning as sysl

#formatter = get_ipython().display_formatter.formatters['text/plain']
#formatter.for_type(np.array, lambda n, p, cycle: p.text("%f" % n))

plt.close("all")
np.random.seed(1)

sparse = False
K = 100
d=4

A_prior_vr = np.identity(pow(d,2))
H_prior_vr = np.identity(pow(d,2))
C_prior_vr = np.identity(pow(d,2))
giv_std = 0.3
pad_std = 0.01#*np.identity(d)

#A = sysl.random_transition_matrix(d, A_prior_vr)
#A = np.array([[0.9,1],[0,0.9]])
#A = np.array([[0.9,0.5],[0,0.7]])
#A = np.array([[1,1],[0,1]])
#B = np.array([[1,1],[0,1]])
#A = np.array([[0.5,0.1,0.1],[0,0.5,0.1],[0,0,0.5]])
#A = np.array([[0.9,0.8,0.7],[0,0.9,0.5],[0,0,0.8]])
#A = np.array([[1,1,1],[0,1,1],[0,0,1]])
#A = np.array([[1.1,0.5,0.2],[0,1,0.5],[0,0,1]])
A = np.array([[0.9,0.7,0.7,0],[0,0.9,-0.5,0],[0,0,0.9,0.9],[0,0,0,0.9]])


#G = 0.5*np.identity(d) + 0.5*np.ones((d,d))
#G = np.ones((d,d))
#G = np.identity(d)
#G = np.array([[1,0.5],[0.5,1]])
#givens = [np.pi/6, np.pi/3]
#G = np.array([[1]])
#givens = [np.pi/6, np.pi/3]
G = np.array([[1,0.5],[0.5,1]])
givens = [0.1, np.pi/6, 0.05, np.pi/4]

H = np.identity(d)
R = 0.01*np.identity(d)
#H = np.array([[1,1],[0,1]])
#R = 0.1*np.identity(d)
#H = np.array([[1,1],[1,0],[0,1]])
#R = 0.1*np.identity(3)
#H = np.array([[1,0]])
#R = 0.01*np.identity(1)

m0 = np.zeros(d)
P0 = 1*np.identity(d)

# Create necessary objects
first_state_prior = GaussianDensity(m0,P0)
model = DegenerateLinearModel(first_state_prior, A, G, givens, H, R)

# Generate data
state, observ = model.simulate_data(K)

# Plot state components
f = plt.figure()
Series.plot_sequence(state, [0,1,2,3], f, 'k')

# Create an estimate of the linear system and create learning object
#A_est = brain.model.F
A_est = 0.9*np.identity(d)
#A_est = A + np.array([[0.1,-0.1,0.05],[-0.05,0.05,0.1],[0,0.2,-0.1]])
#A_est = A + np.random.normal(loc=0,scale=0.05,size=(d,d))
G_est = 10*np.identity(G.shape[0])
givens_est = [0]*len(givens)
#G_est = np.array([[2,0],[0,2]])
#givens_est = [0,0,0,0]
init_model_est = DegenerateLinearModel(first_state_prior, A_est, G_est, givens_est, H, R)
brain = DegenerateLinearModelLearner(init_model_est, observ, A_prior_vr, C_prior_vr, H_prior_vr, giv_std, pad_std)

adapt_batch_size = 10

# Gibbs sample the transition matrix
for ii in range(10):

    print("")
    print("Iteration number %u" % (ii))
    
    if (((ii%adapt_batch_size)==0) and (ii>0)):
        brain.adapt_std(adapt_batch_size)
    print("")
    print("Givens rotation proposal standard deviations:")
    print(brain.giv_std)
    print("Transition matrix padding standard deviation:")
    print(brain.pad_std)
    
    brain.iterate_transition_matrix(1)
    brain.iterate_transition_noise_matrix(1)
        
#    brain.iterate_observation_matrix(1)
        
    print("Transition matrix:")
    print(brain.model.F)
    print("Transition matrix eigenvalue magnitudes:")
    print(abs(np.linalg.eig(brain.model.F)[0]))
    #print("Observation matrix:")
    #print(brain.model.H)
    print("Transition variance:")
    print(brain.model.Q)
    print("Transition variance eigenvalues:")
    print(np.linalg.eigh(brain.model.Q)[0])
    print("Givens rotations")
    print(brain.model.givens)
    brain.save_link()

print("")

from degenerate_linear_model import draw_chain_histogram
draw_chain_histogram(brain, model, 5)
plt.figure().add_subplot(1,1,1).plot(np.cumsum(np.array(brain.Facc)))
#plt.figure().add_subplot(1,1,1).plot(np.cumsum(np.array(brain.Gacc)))
ax = plt.figure().add_subplot(1,1,1)
for dd in range(len(givens)):
    ax.plot(np.cumsum(np.array(brain.givens_acc[dd])))
    
ax = plt.figure().add_subplot(1,1,1)
for dd in range(len(givens)):
    ax.plot(np.array(brain.giv_std_chain[dd]))
    
plt.figure().add_subplot(1,1,1).plot(np.array(brain.pad_std_chain))

f_mcgiv = plt.figure()
for dd in range(len(givens)):
    ax = f_mcgiv.add_subplot(len(givens),1,dd)
    ax.plot([mm.givens[dd] for mm in brain.chain])
    ax.plot([0,len(brain.chain)], [givens[dd]]*2, 'r')
    


