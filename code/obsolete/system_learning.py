# This module contains basic subroutines for learning linear-Gaussian sequential models.
# sample_transition_matrix          - Sample a transition matrix with a matrix Gaussian prior and known state sequence

# The focus is speed and simplicity. No error checking. No special cases. No batch processing.

import numpy as np
from scipy import linalg as la
from scipy import stats as sps
from scipy import misc as spm
from scipy.stats import multivariate_normal as mvn
from numpy import random as rnd

def sample_transition_matrix_prior(P):
    ds = np.sqrt(P.shape[0])
    Avec = mvn.rvs(mean=np.zeros(P.shape[0]), cov=P)
    A = np.reshape(Avec,(ds,ds))
    return A
    
def sample_transition_matrix_mask_prior(ds, pB):
    ds2 = pow(ds,2)
    Bvec = np.zeros(ds2)
    for ii in range(ds2):
        Bvec[ii] = rnd.random()<pB
    B = np.reshape(Bvec,(ds,ds))
    return B

def sample_transition_matrix_conditional(x, Q, P, B=None, A=None):
    K = len(x)
    ds = Q.shape[1]
    ds2 = ds**2
    XQX = np.zeros((ds2,ds2))
    XQx = np.zeros(ds2)
    for kk in range(1,K):
        if type(B)==np.ndarray:
            tmp = [ np.multiply((B[ii,:].T), x[kk-1]) for ii in range(ds) ]
        else:
            tmp = [x[kk-1]]*ds
        Xmat = la.block_diag(*tmp)
        XQX = XQX + np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,Xmat )
        XQx = XQx + np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,x[kk])
    Avr = la.inv(la.inv(P, check_finite=False) + XQX, check_finite=False)
    Avr = (Avr+Avr.T)/2
    Amn = np.dot(Avr,XQx)
    if not(type(A)==np.ndarray):
        Avec = mvn.rvs(mean=Amn, cov=Avr)
        A = np.reshape(Avec,(ds,ds))
    else:
        Avec = A.flatten()
    pdf = mvn.logpdf(Avec,mean=Amn,cov=Avr)
    return A,pdf



def sample_degenerate_transition_matrix_conditional(x, A, Q, Om, P):
    K = len(x)
    ds = A.shape[0]
    r = Q.shape[0]
    
    z = x.copy()
    z[0] = np.zeros(z[0].shape)
    for kk in range(1,K):
        z[kk] = np.dot(Om.T, x[kk]-np.dot(A,x[kk-1]))
        if not all(abs(z[kk][r:])<(1E-5*K)):
            print(z[kk])
            raise ValueError('State difference in incorrect subspace.')
    
    XQX = np.zeros((ds*r,ds*r))
    XQx = np.zeros(ds*r)
    for kk in range(1,K):
        tmp = [x[kk-1]]*r
        Xmat = la.block_diag(*tmp)
        XQX = XQX + np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,Xmat )
        XQx = XQx + np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,z[kk][:r])
#        print(np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,z[kk][:r]))
#    Avr = la.inv(la.inv(P, check_finite=False) + XQX, check_finite=False)
    Avr = la.inv(XQX, check_finite=False)
    Avr = (Avr+Avr.T)/2
    Amn = np.dot(Avr,XQx)
    Avec = mvn.rvs(mean=Amn, cov=Avr)
    A = A + np.dot(Om, np.vstack((np.reshape(Avec,(r,ds)),np.zeros((ds-r,ds)))) )
    return A
    
    
#def sample_degenerate_transition_matrix_conditional(x, A, Q, Om, P, B=None):
#    K = len(x)
#    ds = Q.shape[0]
#    
#    QpOm = Q + Om
#    z = x.copy()
#    eps = np.zeros(ds)
#    for kk in range(K):
#        eps = np.dot(A,eps) + mvn.rvs( mean=np.zeros(ds), cov=Om )
#        z[kk] = x[kk] + eps
##    print('')
##    print(eps)
#
#    ds2 = pow(ds,2)
#    XQX = np.zeros((ds2,ds2))
#    XQx = np.zeros(ds2)
#    for kk in range(1,K):
#        if type(B)==np.ndarray:
#            tmp = [ np.multiply((B[ii,:].T), z[kk-1]) for ii in range(ds) ]
#        else:
#            tmp = [z[kk-1]]*ds
#        Xmat = la.block_diag(*tmp)
#        XQX = XQX + np.dot( la.solve(QpOm,Xmat,sym_pos=True,check_finite=False).T ,Xmat )
#        XQx = XQx + np.dot( la.solve(QpOm,Xmat,sym_pos=True,check_finite=False).T ,x[kk])
#    Avr = la.inv(la.inv(P, check_finite=False) + XQX, check_finite=False)
#    Avr = (Avr+Avr.T)/2
#    Amn = np.dot(Avr,XQx)
#    Avec = mvn.rvs(mean=Amn, cov=Avr)
#    A = np.reshape(Avec,(ds,ds))
#    return A
    
    
    
def sample_transition_matrix_mask_conditional(x, A, B, Q, pB):
    K = len(x)
    ds = A.shape[1]
    ds2 = pow(ds,2)
    XQX = np.zeros((ds2,ds2))
    XQx = np.zeros(ds2)
    for kk in range(1,K):
        tmp = [ np.multiply((A[ii,:].T), x[kk-1]) for ii in range(ds) ]
        Xmat = la.block_diag(*tmp)
        XQX = XQX + np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,Xmat )
        XQx = XQx + np.dot( la.solve(Q,Xmat,sym_pos=True,check_finite=False).T ,x[kk])
    
    Bvec = B.flatten()
    order = np.random.permutation(ds2)
    pon = np.zeros(2)
    for ii in order:
        Bmod = list(Bvec)
        Bmod[ii] = 0.5
        pon[0] = np.log(pB) - 0.5*(  2*np.dot(XQX[ii,:],Bmod)-2*XQx[ii]  )
        pon[1] = np.log(1-pB)
        pon = pon - spm.logsumexp(pon)
        Bvec[ii] = np.log(rnd.random())<pon[0]
    
    B = np.reshape(Bvec,(ds,ds))
    return B
    




def sample_transition_matrix_and_mask_conditional(x, A, B, Q, P, pB):
    # Currently just ignores the prior structure for A and uses an independent prior
    pa = P[0,0]
    
    K = len(x)
    ds = A.shape[1]
    
    # Don't change originals
    Anew = A.copy()
    Bnew = B.copy()
    
    order1 = np.random.permutation(ds)
    order2 = np.random.permutation(ds)
#    order1 = [np.random.random_integers(0,ds-1)]
#    order2 = [np.random.random_integers(0,ds-1)]
    
    for ii in order1:
        for jj in order2:
            XQx = 0
            xQx = 0
            F = np.multiply(Anew,Bnew)
            F[ii,jj] = 0
            for kk in range(1,K):
                xij = np.zeros(ds)
                xij[ii] = x[kk-1][jj]
                D = x[kk]-np.dot(F,x[kk-1])
                XQx = XQx + np.dot(D.T, la.solve(Q,xij) )
                xQx = xQx + np.dot(xij.T, la.solve(Q,xij) )
            
            pon = np.zeros(2)
            vr = 1/pa + xQx
            pon[0] = np.log(pB) + 0.5*( (XQx**2)/vr ) - 0.5*np.log(vr)
            pon[1] = np.log(1-pB) - 0.5*np.log(1/pa)
            pon = pon - spm.logsumexp(pon)
            Bnew[ii,jj] = np.log(rnd.random())<pon[0]
            
            if Bnew[ii,jj]:
                a_vr = 1/(1/pa + xQx)
                a_mn = a_vr*XQx
            else:
                a_vr = pa
                a_mn = 0
            Anew[ii,jj] = mvn.rvs(mean=a_mn, cov=a_vr)
            
    return Anew,Bnew




    
def sample_observation_matrix_conditional(x, y, R, P, D=None):
    K = len(x)
    ds = x[0].shape[0]
    do = R.shape[0]
    ds2 = pow(ds,2)
    XRX = np.zeros((ds2,ds2))
    XRy = np.zeros(ds2)
    for kk in range(K):
        if type(D)==np.ndarray:
            tmp = [ np.multiply((D[ii,:].T), x[kk]) for ii in range(ds) ]
        else:
            tmp = [x[kk]]*ds
        Xmat = la.block_diag(*tmp)
        XRX = XRX + np.dot( la.solve(R,Xmat,sym_pos=True,check_finite=False).T ,Xmat )
        XRy = XRy + np.dot( la.solve(R,Xmat,sym_pos=True,check_finite=False).T ,y[kk])
    Cvr = la.inv(la.inv(P, check_finite=False) + XRX, check_finite=False)
    Cvr = (Cvr+Cvr.T)/2
    Cmn = np.dot(Cvr,XRy)
    Cvec = mvn.rvs(mean=Cmn, cov=Cvr)
    C = np.reshape(Cvec,(ds,do))
    return C
    
    
    
def sample_transition_noise_matrix_conditional(x0, u, A, H, R, y, P):
    K = len(u)
    ds = A.shape[1]
    ds2 = pow(ds,2)
    HRH = np.dot(H.T, la.solve(R,H,sym_pos=True,check_finite=False) )
    HR = la.solve(R,H,sym_pos=True,check_finite=False).T
    PHRHP = np.zeros((ds2,ds2))
    PHRy = np.zeros(ds2)
    Akx0 = x0
    Phi = np.zeros((ds,ds2))
    for kk in range(1,K):
        tmp = [u[kk]]*ds
        Umat = la.block_diag(*tmp)
        Phi = np.dot(A,Phi) + Umat
        PHRHP = PHRHP + np.dot(Phi.T,np.dot(HRH,Phi))
        Akx0 = np.dot(A,Akx0)
        PHRy = PHRy + np.dot(Phi.T,np.dot(HR,y[kk]-np.dot(H,Akx0)))
    PHRHP = (PHRHP+PHRHP.T)/2
    Gvr = la.inv(la.inv(P) + PHRHP)
    Gvr = (Gvr+Gvr.T)/2
    Gmn = np.dot(Gvr,PHRy)
    Gvec = mvn.rvs(mean=Gmn, cov=Gvr)
    G = np.reshape(Gvec,(ds,ds))
    
#    _,G = la.polar(G)
#    print(G)
    
    return G
    
    
def sample_degenerate_transition_noise_matrix_conditional(x, A, r, orth, nu0, P0):
    K = len(x)
    delta = [np.dot(orth.T,x[nn]-np.dot(A,x[nn-1]))[:r] for nn in range(1,K)]
#    print(delta)
    invP = la.inv(P0)
    for kk in range(K-1):
        invP = invP + np.outer(delta[kk],delta[kk])
    nu = nu0 + K-1
    P = la.inv(invP)
    invQ = sample_wishart(nu,P)
    Q = la.inv(invQ)
    G = la.sqrtm(Q)    
    return G
    
    
    
    
def sample_transition_covariance_conditional(x, A, nu0, P0):
    K = len(x)
    invP = la.inv(P0)
    for kk in range(1,K):
        delta = x[kk]-np.dot(A,x[kk-1])
        invP = invP + np.outer(delta,delta)
    nu = nu0 + K-1
    P = la.inv(invP)
    invQ = sample_wishart(nu,P)
    Q = la.inv(invQ)
    return Q
    
def sample_wishart(nu, P):
    dim = P.shape[0]
    cholP = la.cholesky(P,lower=True)
    R = np.zeros((dim,dim))
    for ii in range(dim):
        R[ii,ii] = np.sqrt(sps.chi2.rvs(nu-(ii+1)+1))
        for jj in range(ii+1,dim):
            R[jj,ii] = sps.norm.rvs()
    cholX = np.dot(cholP,R)
    X = np.dot(cholX,cholX.T)
    return X
            
    




# FOR ANNEALED SYSTEM LEARNING
def sample_annealed_transition_matrix_conditional(x, idx1, idx2, Q, P, gamma):
    K = len(x)
    ds = Q.shape[1]
    ds2 = pow(ds,2)
    XQX = np.zeros((ds2,ds2))
    XQx = np.zeros(ds2)
    for kk in range(1,K):
        if   ((kk>=idx1[0])and(kk<idx1[1])) and    ((kk>=idx2[0])and(kk<idx2[1])):
            Qtemp = Q
        elif ((kk>=idx1[0])and(kk<idx1[1])) and not((kk>=idx2[0])and(kk<idx2[1])):
            Qtemp = Q/(1-gamma)
        elif not((kk>=idx1[0])and(kk<idx1[1])) and ((kk>=idx2[0])and(kk<idx2[1])):
            Qtemp = Q/gamma
        else:
            continue
        tmp = [x[kk-1]]*ds
        Xmat = la.block_diag(*tmp)
        XQX = XQX + np.dot( la.solve(Qtemp,Xmat,sym_pos=True,check_finite=False).T ,Xmat )
        XQx = XQx + np.dot( la.solve(Qtemp,Xmat,sym_pos=True,check_finite=False).T ,x[kk])
    Avr = la.inv(la.inv(P, check_finite=False) + XQX, check_finite=False)
    Avr = (Avr+Avr.T)/2
    Amn = np.dot(Avr,XQx)
    Avec = mvn.rvs(mean=Amn, cov=Avr)
    A = np.reshape(Avec,(ds,ds))
    return A
    
    
# This is something of a fudge - needs to be stable    
def random_transition_matrix(d, P):
    A = sample_transition_matrix_prior(P)
    A = (A+A.T)/2
    val,vec = la.eig(A)
    val = 1.8*np.random.random(d)-0.9
    A = np.dot(vec, la.solve(vec.T,np.diag(val)).T )
    return A
    
