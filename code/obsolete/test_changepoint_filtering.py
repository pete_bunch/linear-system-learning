import numpy as np
import matplotlib.pyplot as plt
from linear_changepoint_model import LinearChangepointModel
from kalman import GaussianDensity, Series

plt.close("all")
np.random.seed(0)

K = 100
d = 2

F1 = np.array([[1,0],[0,1]])
F2 = np.array([[0.95,0.95],[0,1]])
F3 = np.array([[0.5,0.5],[0,1]])

Q = np.array([[0.1,0],[0,0.1]])
H = np.array([[1,0],[0,1]])
R = np.array([[0.1,0],[0,0.1]])
m0 = np.array([1,0])
P0 = np.array([[10,0],[0,0.01]])

# Make a changepoint linear system
first_state_prior = GaussianDensity(m0, P0)
cp_model = LinearChangepointModel([30,60],K,[F1,F2,F3],Q,H,R,first_state_prior)

# Generate data
state, observ = cp_model.simulate_data()

# Kalman filter
flt,prd,lhood = cp_model.kalman_filter( observ)

# RTS smoother
smt = cp_model.rts_smoother(flt, prd)

# Sample from the smoothing distribution
smp = cp_model.sample_posterior(observ)

# Try plotting some things
f = plt.figure()
Series.plot_sequence(state, range(d), f, 'k')
Series.plot_sequence(observ, range(d), f, 'r')
Series.plot_sequence(smp, range(d), f, 'b')

GaussianDensity.plot_sequence(flt, range(d), f, 'g')
GaussianDensity.plot_sequence(smt, range(d), f, 'c')
