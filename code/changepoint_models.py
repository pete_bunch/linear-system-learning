import numpy as np
import kalman as kal
from scipy.stats import multivariate_normal as mvn
from scipy import linalg as la
from scipy import special as sps
from scipy import stats
from kalman import GaussianDensity, Series


class LinearChangepointModel:
    """Linear-Gaussian Changepoint Model Class"""
    
    def __init__(self, K, point, P1, F, Q, H, R):
        self.F = [ff.copy() for ff in F]    # Transition matrix
        self.Q = [qq.copy() for qq in Q]    # Transition covariance
        self.H = H.copy()           # Observation matrix
        self.R = R.copy()           # Observation variance
        self.P1 = P1.copy()         # First state prior
        self.ds = F[0].shape[0]     # State dimension
        self.do = H.shape[0]        # State dimension
        self.K = K                  # Data length (i.e. upper bound for changepoints)
        
        self.point = list(point)    # Changepoint list
        self.order = list(range(len(point)+1))
        
    def copy(self):
        copy = LinearChangepointModel(self.K, self.point, self.P1, self.F, 
                                                        self.Q, self.H, self.R)
        copy.order = list(self.order)
        return copy
    
    @property
    def model_in_effect(self):
        """List of length K indicating which model"""
        mie = []
        mm = 0
        for kk in range(self.K):
            if kk in self.point:
                mm += 1
            mie.append(self.order[mm])
        return mie
    @model_in_effect.setter
    def model_in_effect(self, value):
        raise AttributeError("Can't set attribute")        
    @model_in_effect.deleter
    def model_in_effect(self):
        raise AttributeError("Can't delete attribute")
        
    def add_changepoint(self, cp, Fnew, Qnew, side):
        """Add a new changepoint along with associated system model"""
        # Side is either 0 or 1, deciding which side of the new changepoint the
        # new system is inserted.
        point = self.point
        if cp in point:
            ValueError("Cannot add changepoint already in list")
        point.append(cp)
        point.sort()
        idx = point.index(cp)
        mi = len(self.F)
        self.order.insert(idx+side,mi)
        self.F.append(Fnew)
        self.Q.append(Qnew)
        
        return idx,mi
        
    def remove_changepoint(self, idx, side):
        """Remove a changepoint along with the associated system model"""
        # Side is either 0 or 1, deciding which side of the changepoint the
        # a system is removed from.
        mi = self.order.pop(idx+side)
        cp = self.point.pop(idx)
#        Frem = self.F.pop(mi)
#        Qrem = self.Q.pop(mi)
        
        return mi,cp#,Frem,Qrem
        
    def changepoint_prior(self, cp_prior_rate):
        """Evaluate prior for the changepoint sequence"""
        # Currently just exponential prior. 
        #prior = (len(self.point)+1)*np.log(cp_prior_rate) - cp_prior_rate*self.K
        
        # Gamma prior on interval durations
        shape = 2
        scale = 1/(cp_prior_rate*shape)
        prior = 0
        last_cp = 0
        for kk in range(len(self.point)):
            interval = self.point[kk] - last_cp
            prior += stats.gamma.logpdf(interval,shape,scale=scale)
            last_cp = self.point[kk]
            
        last_interval = self.K-last_cp
        prior += np.log( 1-stats.gamma.cdf(last_interval,shape,scale=scale) )
        
        return prior

    def simulate_data(self):
        """Simulate artificial data from the model"""
        state = self.sample_prior()
        observ = self.sample_observ(state)
        return state, observ

    def sample_prior(self):
        """Sample from the state prior, running forwards in time"""
        state = Series.new_sequence(self.ds, self.K)
        mie = self.model_in_effect
        state[0]  = mvn.rvs( mean=self.P1.mn, cov=self.P1.vr )
        for kk in range(1,self.K):
            F = self.F[mie[kk]]
            Q = self.Q[mie[kk]]
            state[kk]  = mvn.rvs( mean=np.dot(F,state[kk-1]), cov=Q )
        return state

    def sample_observ(self, state):
        """Sample an observation for each state"""
        K = len(state)
        observ = Series.new_sequence(self.do, K)
        for kk in range(K):
            observ[kk] = mvn.rvs( mean=np.dot(self.H,state[kk]), cov=self.R )
        return observ

    def kalman_filter(self, observ):
        """Run a Kalman filter on a set of observations"""
        K = len(observ)
        flt = GaussianDensity.new_sequence(self.ds, K)
        prd = GaussianDensity.new_sequence(self.ds, K)
        mie = self.model_in_effect
        lhood = 0
        for kk in range(K):
            if kk > 0:
                F = self.F[mie[kk]]
                Q = self.Q[mie[kk]]
                prd[kk] = kal.predict(flt[kk-1], F, Q)
            else:
                prd[kk] = self.P1
            flt[kk],innov = kal.correct(prd[kk], observ[kk], self.H, self.R)
            lhood = lhood + mvn.logpdf(observ[kk], innov.mn, innov.vr)
        return flt, prd, lhood

    def rts_smoother(self, flt, prd):
        """Run a Rauch-Tung-Striebel smoother on the Kalman filter output"""
        K = len(flt)
        smt = GaussianDensity.new_sequence(self.ds, K)
        mie = self.model_in_effect
        for kk in reversed(range(K)):
            if kk<K-1:
                F = self.F[mie[kk]]
                smt[kk] = kal.update(flt[kk], smt[kk+1], prd[kk+1], F)
            else:
                smt[kk] = flt[kk]
        return smt
        
    def backward_simulation(self, flt):
        """Use backward simulation to sample from the state joint posterior"""
        K = len(flt)
        x = Series.new_sequence(self.ds, K)
        mie = self.model_in_effect
        for kk in reversed(range(K)):
            if kk < K-1:
                F = self.F[mie[kk]]
                Q = self.Q[mie[kk]]
                samp_dens,_ = kal.correct(flt[kk], x[kk+1], F, Q)
            else:
                samp_dens = flt[kk]
            x[kk] = mvn.rvs(mean=samp_dens.mn, cov=samp_dens.vr)
        return x

    def sample_posterior(self, observ):
        """Sample a state trajectory from the joint smoothing distribution"""
        flt,_,_ = self.kalman_filter(observ)
        x = self.backward_simulation(flt)
        return x
    
    ### CHANGEPOINT PROPOSALS ###
    
    def propose_shift(self, idx):
        """Propose a shift to the value of one changepoint"""
        
        # idx indicates which changepoint to move
        cp = self.point[idx]
        
        # Bounds
        if idx > 0:
            lb = self.point[idx-1]+1
        else:
            lb = 0
        if idx < len(self.point)-1:
            ub = self.point[idx+1]-1
        else:
            ub = self.K-1
        
        if ub-lb > 1:
            # Sample a new value uniformly
            new_cp = cp
            while new_cp==cp:
                new_cp = np.random.random_integers(lb,ub)
            
            # Add it to the list
            self.point[idx] = new_cp
            valid = True
        else:
            valid = False
        
        # Probabilities - reversible, so doesn't matter
        bwd_ppsl = 0
        fwd_ppsl = 0
        
        # Models affected by this change
        aff = [ self.order[idx], self.order[idx+1] ]
        
        return aff,valid,fwd_ppsl,bwd_ppsl
        
    def propose_adjust(self, idx, half_width):
        """Propose an adjustment to the value of one changepoint"""
        
        # idx indicates which changepoint to move
        cp = self.point[idx]
        
        # Bounds
        if idx > 0:
            lb = self.point[idx-1]+1
        else:
            lb = 0
        if idx < len(self.point)-1:
            ub = self.point[idx+1]-1
        else:
            ub = self.K-1
        
        fwd_lb = max(lb,cp-half_width)
        fwd_ub = min(ub,cp+half_width)
        
        if ub-lb > 1:
            # Sample a new value uniformly
            new_cp = cp
            while new_cp==cp:
                new_cp = np.random.random_integers(fwd_lb,fwd_ub)
            
            # Add it to the list
            self.point[idx] = new_cp
            valid = True
            
            # Reverse bounds
            bwd_lb = max(lb,new_cp-half_width)
            bwd_ub = min(ub,new_cp+half_width)
            
            # Probabilities
            fwd_ppsl = -np.log(fwd_ub-fwd_lb+1)
            bwd_ppsl = -np.log(bwd_ub-bwd_lb+1)
            
        else:
            valid = False
            bwd_ppsl = 0
            fwd_ppsl = 0
            
        # Models affected by this change
        aff = [ self.order[idx], self.order[idx+1] ]
        
        return aff,valid,fwd_ppsl,bwd_ppsl
        
    def propose_add(self, A, Q):
        """Propose adding a new changepoint to the sequence"""
        
        # Sample a new changepoint
        cp = -1
        while ( (cp<0) or (cp in self.point) ):
            cp = np.random.random_integers(self.K)-1
        side = np.random.random_integers(0,1)
        
        # Add it to the changepoint model
        idx,mi = self.add_changepoint(cp, A, Q, side)
        valid = True
        
        # Probabilities
        fwd_ppsl = -np.log(self.K-(len(self.point)-1))
        bwd_ppsl = -np.log(len(self.point))
        
        # Models affected by this change
        aff = [ self.order[idx], self.order[idx+1] ]
        
        return aff,valid,fwd_ppsl,bwd_ppsl
        
    def propose_remove(self, idx=None):
        """Propose removing a changepoint from the sequence"""
        
        if len(self.point) > 0:
            
            # Sample a changepoint
            if idx is None:
                idx = np.random.random_integers(len(self.point))-1
            side = np.random.random_integers(0,1)
            
            # Models affected by this change
            aff = [ self.order[idx], self.order[idx+1] ]
            
            # Remove it from the changepoint model
            side,cp = self.remove_changepoint(idx, side)
            valid = True
            
            # Probabilities
            fwd_ppsl = -np.log(len(self.point)+1)
            bwd_ppsl = -np.log(self.K-len(self.point))
            
        else:
            valid = False
            bwd_ppsl = 0
            fwd_ppsl = 0
            aff = []
        
        return aff,valid,fwd_ppsl,bwd_ppsl
