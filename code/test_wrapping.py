# Standard modules
import numpy as np
import matplotlib.pyplot as plt

# My modules
from linear_models import *
from linear_models_mcmc import *
from kalman import GaussianDensity, Series
from scipy.stats import multivariate_normal as mvn

# Close all windows
plt.close("all")

# Set random seed
np.random.seed(0)

### SPECIFY A LINEAR MODEL ###
model_type = 2      # 0=basic, 1=sparse, 2=degenerate

# Basic parameters
K = 100         # Number of time instants in data
d=5             # Number of state dimensions

# Dense transition matrix
A = np.array([[0.9,0.7,0.7,0,0],[0,0.9,-0.5,0.1,0],[0,0,0.9,0.9,0.5],[0,0,0,0.9,0.8],[0,0,0,0,0.9]])

# Transition matrix mask
B = np.ceil(abs(A))

# Observation matrix
H = np.identity(d)

# Observation covariance matrix
R = 0.01*np.identity(d)

# Initial state prior 
m0 = np.zeros(d)
P0 = 1*np.identity(d)
first_state_prior = GaussianDensity(m0,P0)

###############################################################################

# Transition noise matrix
rank = 3
u1 = np.array([1,0,0])
u2 = np.array([0.2,0.3,0.4])
D = np.outer(u1,u1)+1.5*np.outer(u2,u2)
#D = np.array([[1,0.5],[0.5,1]])
givens = [0.1,0.2,0.3,0.4,0.3,0.2]

model = DegenerateLinearModel(first_state_prior, A, D, givens, H, R)
test = model.copy()

#val,vec = test.reduce_rank()

        
# Eigendecomposition of D and sort into descending order
Dval,Dvec = la.eigh(test.D)
idx = Dval.argsort()
idx = idx[::-1]
Dval = Dval[idx]
Dvec = Dvec[:,idx]

# Find minimal orthogonal matrix
U = test.U
Uinc = np.identity(d)
Uinc[:rank,:rank] = Dvec
U = np.dot(U,Uinc)
U = np.delete(U, list(range(rank,d)), 1)

#eigs = U
#eigs = la.eigh(test.Q)[1][:,:rank]
#new_vec = mvn.rvs(mean=np.zeros(d),cov=np.identity(d))
#eigs = np.append(eigs,np.expand_dims(new_vec,1),1)
#orth,_ = la.qr(eigs,mode='economic')
#new_vec = orth[:,rank]

test.increase_rank(0, None)

print(model.Q)
print('')
print(test.Q)
print('')
print(test.Q-model.Q)



## Create MCMC container object
#brain = LinearModelMCMC(model_type, init_model_est, observ,
#                        A_prior_vr=A_prior_vr, B_prior_prob=B_prior_prob,
#                        Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof,
#                        giv_std=giv_std, pad_std=pad_std)
#
#    
#    # MCMC moves
#    brain.iterate_transition_matrix(1)
#    brain.iterate_transition_noise_matrix(1)
#    brain.iterate_transition_noise_matrix_rank(1)
##    brain.iterate_observation_matrix(1)
#    
