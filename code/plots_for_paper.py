# Plots for paper.
# 1. Plain old linear model
# 2. Sparse liner model
# 3. Degenerate linear model without rank moves
# 4. Degenerate linear model with rank moves

# Standard modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as la

# My modules
from linear_models import *
from linear_models_mcmc import *
from kalman import GaussianDensity, Series

# Basic parameters
K = 100         # Number of time instants in data
d=4             # Number of state dimensions

# Observation model
H = np.identity(d)
R = 1*np.identity(d)

# Initial state prior 
m0 = np.zeros(d)
P0 = 1*np.identity(d)
first_state_prior = GaussianDensity(m0,P0)

### Model priors ###
A_prior_vr = 1
B_prior_prob = 0.5
Q_prior_scale = 1
Q_prior_dof = d-1

### Algorithm parameters ###
giv_std = 0.05
pad_std = 0.1
rot_std = 0.002
adapt_batch_size = 10

# Close all windows
plt.close("all")

for tt in range(2,3):
    
    # Model type: 0=basic, 1=sparse, 2=degenerate
    if tt == 0:
        model_type = 0
        num_iter = 6000
        num_burn_in = 1000
    elif tt == 1:
        model_type = 1
        num_iter = 6000
        num_burn_in = 1000
    elif tt == 2:
        model_type = 2
        num_iter = 10000
        num_burn_in = 5000
    elif tt == 3:
        model_type = 2
        num_iter = 10000
        num_burn_in = 5000
    
    # Set random seed
    np.random.seed(0)

    # Dense transition matrix
#    A = np.array([[0.9,0.7,0.7,0],[0,0.9,-0.5,0.1],[0,0,0.9,0.9],[0,0,0,0.9]])
    A = np.array([[0.9,0.7,0.7,0],[0,0.9,-0.5,0.1],[0,0,1.6,-0.8],[0,0,1,0]])
#    A = np.array([[0.9,0.9,0,0],[0,0.9,0,0],[-0.6,0,0.6,0.2],[-0.5,0.1,0,0.5]])
#    X = np.random.normal(size=(d,d))
#    _,U = np.linalg.eigh(np.dot(X,X.T))
#    Aar = np.array([[27./10, -259./80, 8739./4000, -26163./40000],[1,0,0,0],[0,1,0,0],[0,0,1,0]])
#    A = np.dot(U,np.dot(Aar,U.T))
    
    # Transition matrix mask
    if model_type==1:
        B = (A!=0).astype(int)

    # Transition noise matrix
    if (model_type==0) or (model_type==1):
        Q = 0.5*np.identity(4)+0.5*np.ones((4,4))
        G = la.cholesky(Q,lower=True)
#        G = 0.5*np.identity(d) + 0.5*np.ones((d,d))
    elif (model_type==2):
        rank = 2
        D = np.array([[1,0.5],[0.5,1]])
        givens = [0.8,-0.5,0.3,0]
    
    ### Initial Model Estimate ###
    A_est = 0.9*np.identity(d)
    if model_type==1:
        B_est = np.ones((d,d))
    if (tt==0) or (tt==1):
        G_est = 10*np.identity(d)
    elif (tt==2):
        D_est = 10*np.identity(D.shape[0])
        givens_est = [0]*len(givens)
    elif (tt==3):
        D_est = 10*np.identity(d)
        givens_est = []
    
    ###########################################################################
    
    # Create model objects
    if model_type == 0:
        model = LinearModel(first_state_prior, A, G, H, R)
        init_model_est = LinearModel(first_state_prior, A_est, G_est, H, R)
    elif model_type == 1:
        model = SparseLinearModel(first_state_prior, A, B, G, H, R)
        init_model_est = SparseLinearModel(
                                  first_state_prior, A_est, B_est, G_est, H, R)
    elif model_type == 2:
        model = DegenerateLinearModel(first_state_prior, A, D, givens, H, R)
        init_model_est = DegenerateLinearModel(
                             first_state_prior, A_est, D_est, givens_est, H, R)
    else:
        raise ValueError("Invalid model type")

    # Generate data
    state, observ = model.simulate_data(K)
    
    # Create MCMC container object
    brain = LinearModelMCMC(model_type, init_model_est, observ,
                          A_prior_vr=A_prior_vr, B_prior_prob=B_prior_prob,
                          Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof,
                          giv_std=giv_std, pad_std=pad_std, rot_std=rot_std)
    
    # Gibbs sample the transition matrix
    for ii in range(num_iter):
        
        print("")
        print("Iteration number %u" % (ii))
        
        if model_type == 2:
            # MCMC proposal adaptation
            if (((ii%adapt_batch_size)==0) and (ii>0)):
                brain.adapt_std(adapt_batch_size)
        
        # MCMC moves
        brain.iterate_transition_matrix(1)
        brain.iterate_transition_noise_matrix(1)
        if tt == 3:
            brain.iterate_transition_noise_matrix_rank(1)
        
        # Save current state of the chain
        brain.save_link()
        
        # Print current state
        print("")
        print("Transition matrix:")
        print(brain.model.F)
        print("Transition matrix eigenvalue magnitudes:")
        print(abs(np.linalg.eig(brain.model.F)[0]))
        print("Transition variance:")
        print(brain.model.Q)
        print("Transition variance eigenvalues:")
        print(np.linalg.eigh(brain.model.Q)[0])
        if model_type == 2:
            print("Rank")
            print(brain.model.rank)
            print("Givens rotations")
            print(brain.model.givens)
            print("")
            print("Rotation proposal standard deviation:")
            print(brain.rot_std)
            print("Givens rotation proposal standard deviations:")
            print(brain.giv_std)
            print("Transition matrix padding standard deviation:")
            print(brain.pad_std)

    # Plot state components
    f = plt.figure()
    Series.plot_sequence(state, range(d), f, 'k')
    for dd in range(d):
        ax = f.add_subplot(d,1,dd+1)
        ax.plot( range(K), [x[dd] for x in observ], 'r.' )
    
    # Plot some MCMC output graphs
    figs = draw_chain_histogram(brain.chain, model, num_burn_in)
    
    if tt==0:
        f.savefig('data_fullrank.pdf', bbox_inches='tight')
        figs[0].savefig('basic_F.pdf', bbox_inches='tight')
        figs[1].savefig('basic_Q.pdf', bbox_inches='tight')
        figs[2].savefig('basic_detQ.pdf', bbox_inches='tight')
    if tt==1:
        figs[0].savefig('sparse_F.pdf', bbox_inches='tight')
        figs[1].savefig('sparse_Q.pdf', bbox_inches='tight')
        figs[3].savefig('sparse_B.pdf', bbox_inches='tight')
    if tt==2:
        f.savefig('data_degenerate.pdf', bbox_inches='tight')
        figs[0].savefig('degenerate_F.pdf', bbox_inches='tight')
        figs[1].savefig('degenerate_Q.pdf', bbox_inches='tight')
        figs[4].savefig('degenerate_D.pdf', bbox_inches='tight')
        figs[5].savefig('degenerate_givens.pdf', bbox_inches='tight')
    if tt==3:
        figs[0].savefig('degenerate_F_with_rank.pdf', bbox_inches='tight')
        figs[1].savefig('degenerate_Q_with_rank.pdf', bbox_inches='tight')
        figs[3].savefig('degenerate_rank_with_rank.pdf', bbox_inches='tight')
        figs[4].savefig('degenerate_D_with_rank.pdf', bbox_inches='tight')
        figs[5].savefig('degenerate_givens_with_rank.pdf', bbox_inches='tight')
    
    f = draw_chain(brain.chain, model, num_burn_in, ['Q'])
    if tt==0:
        f.savefig('basic_Q_chain.pdf', bbox_inches='tight')
    if tt==2:
        f.savefig('sparse_Q_chain.pdf', bbox_inches='tight')
    if tt==2:
        f.savefig('degenerate_Q_chain.pdf', bbox_inches='tight')
    if tt==3:
        f.savefig('degenerate_Q_chain_with_rank.pdf', bbox_inches='tight')
    
    f = draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['Q'], nlags=50)
    if tt==0:
        f.savefig('basic_Q_acf.pdf', bbox_inches='tight')
    if tt==1:
        f.savefig('sparse_Q_acf.pdf', bbox_inches='tight')
    if tt==2:
        f.savefig('degenerate_Q_acf.pdf', bbox_inches='tight')
        
    if tt==3:
        f.savefig('degenerate_Q_acf_with_rank.pdf', bbox_inches='tight')
    
#    for ax in figs[0].axes:
#        ax.set_xlim([-0.5,1.5])
#        ax.set_ylim([0,30])
#        ax.locator_params(nbins=4)
#    figs[0].show()
#    for ax in figs[1].axes:
#        ax.set_xlim([0,2.5])
#        ax.set_ylim([0,30])
#        ax.locator_params(nbins=4)
#    figs[1].show()
        
#draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['F'], nlags=10)
#draw_chain_histogram(brain.chain, model, num_burn_in)
#if model_type==2:
#    draw_chain(brain.chain, model, num_burn_in, ['givens'])
#    draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['givens'], nlags=100)
#    draw_chain_acceptance([brain.F_acc], num_burn_in)
#    draw_chain_acceptance(brain.givens_acc, num_burn_in)
#    draw_chain_adaptation([brain.pad_std_chain], num_burn_in)
#    draw_chain_adaptation(brain.giv_std_chain, num_burn_in)
    

