# Compare basic and sparse linear models on one data set with lots of random seeds

# Standard modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as la

# My modules
from linear_models import *
from linear_models_mcmc import *
from kalman import GaussianDensity, Series

# Basic parameters
K = 100         # Number of time instants in data
d=4             # Number of state dimensions

# Observation model
H = np.identity(d)
R = 1*np.identity(d)

# Initial state prior 
m0 = np.zeros(d)
P0 = 1*np.identity(d)
first_state_prior = GaussianDensity(m0,P0)

### Model priors ###
A_prior_vr = 1
B_prior_prob = 0.5
Q_prior_scale = 1
Q_prior_dof = d-1

# Close all windows
plt.close("all")

# Set random seed
np.random.seed(0)

 # System
A = np.array([[0.9,0.7,0.7,0],[0,0.9,-0.5,0.1],[0,0,1.6,-0.8],[0,0,1,0]])
B = (A!=0).astype(int)
Q = 0.5*np.identity(4)+0.5*np.ones((4,4))
G = la.cholesky(Q,lower=True)

# Estimates
A_est = 0.9*np.identity(d)
B_est = np.ones((d,d))
G_est = 10*np.identity(d)

# Model
model = SparseLinearModel(first_state_prior, A, B, G, H, R)

# Generate data
state, observ = model.simulate_data(K)

# Create two estimates
init_model_est1 = LinearModel(first_state_prior, A_est, G_est, H, R)
init_model_est2 = SparseLinearModel(first_state_prior, A_est, B_est, G_est, H, R)

# Results array
Fresults1 = []
Qresults1 = []
Fresults2 = []
Qresults2 = []

num_rpts = 20
num_iter = 1000
num_burn_in = 500

for kk in range(num_rpts):
    
    print("")
    print("Repeat number %u" % (kk))
    
    # Create two samplers
    brain1 = LinearModelMCMC(0, init_model_est1, observ,
                             A_prior_vr=A_prior_vr,
                             Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof)
    brain2 = LinearModelMCMC(1, init_model_est2, observ,
                             A_prior_vr=A_prior_vr, B_prior_prob=B_prior_prob,
                             Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof)
    
    Ferr1 = []
    Ferr2 = []
    Fsum1 = np.zeros((d,d))
    Fsum2 = np.zeros((d,d))
    Qerr1 = []
    Qerr2 = []
    Qsum1 = np.zeros((d,d))
    Qsum2 = np.zeros((d,d))
    
    # Gibbs sample the transition matrix
    for ii in range(num_iter):
        
#        print("")
#        print("Iteration number %u" % (ii))
        
        # MCMC moves
        brain1.iterate_transition_matrix(1)
        brain1.iterate_transition_noise_matrix(1)
        brain2.iterate_transition_matrix(1)
        brain2.iterate_transition_noise_matrix(1)
        
        # Save current state of the chain
        brain1.save_link()
        brain2.save_link()
        
        # Errors
        if ii >= num_burn_in:
            Fsum1 += brain1.model.F
            Qsum1 += brain1.model.Q
            Ferr1.append( la.norm( (Fsum1/(ii+1-num_burn_in))-model.F ) )
            Qerr1.append( la.norm( (Qsum1/(ii+1-num_burn_in))-model.Q ) )
            
            Fsum2 += brain2.model.F
            Qsum2 += brain2.model.Q
            Ferr2.append( la.norm( (Fsum2/(ii+1-num_burn_in))-model.F ) )
            Qerr2.append( la.norm( (Qsum2/(ii+1-num_burn_in))-model.Q ) )
        
        # Print current state
#        print("")
#        print("Transition matrix:")
#        print(Fsum1)
#        print("")
#        print(Fsum2)
#        print("")
#        print("Transition covariance:")
#        print(Qsum1)
#        print("")
#        print(Qsum2)
        
#        print(brain.model.F)
#        print("Transition matrix eigenvalue magnitudes:")
#        print(abs(np.linalg.eig(brain.model.F)[0]))
#        print("Transition variance:")
#        print(brain.model.Q)
#        print("Transition variance eigenvalues:")
#        print(np.linalg.eigh(brain.model.Q)[0])
#        if model_type == 2:
#            print("Rank")
#            print(brain.model.rank)
#            print("Givens rotations")
#            print(brain.model.givens)
#            print("")
#            print("Givens rotation proposal standard deviations:")
#            print(brain.giv_std)
#            print("Transition matrix padding standard deviation:")
#            print(brain.pad_std)

    Fresults1.append(Ferr1)
    Qresults1.append(Qerr1)
    Fresults2.append(Ferr2)
    Qresults2.append(Qerr2)

# Average over repeats
meanFerr1 = np.mean( np.array(Fresults1), axis=0 )
meanQerr1 = np.mean( np.array(Qresults1), axis=0 )
meanFerr2 = np.mean( np.array(Fresults2), axis=0 )
meanQerr2 = np.mean( np.array(Qresults2), axis=0 )

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(meanFerr1, 'k-')
ax.plot(meanFerr2, 'b:')
fig.savefig('Ferrs.pdf', bbox_inches='tight')

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(meanQerr1, 'k-')
ax.plot(meanQerr2, 'b:')
fig.savefig('Qerrs.pdf', bbox_inches='tight')

