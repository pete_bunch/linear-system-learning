import statsmodels.tsa.stattools as st

def draw_autocorrelation(chain, burn_in):
    
    seq = np.array([mm.F[1,2] for mm in chain[burn_in:]])
    ac = st.acf(seq,unbiased=False,nlags=30,)

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(seq)
    
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(ac)
    
    return ac



def save_transition_histograms(brain, true_model, burn_in):
    
    d = min(3,brain.model.ds) # Don't plot more than 3x3. Too small
    
    if len(brain.chain)>burn_in:
        # F
        print("F")
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.F[rr,cc] for mm in brain.chain[burn_in:]],bins=5)
                ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(brain.chain[burn_in:])], '-r')
                ax[rr,cc].set_xlim((-0.5,1.5))
                ax[rr,cc].yaxis.set_visible(False)
        plt.savefig('F.pdf', bbox_inches='tight')
                
        # Q
        print("Q")
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.Q[rr,cc] for mm in brain.chain[burn_in:]],bins=5)
                ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(brain.chain[burn_in:])], '-r')
                ax[rr,cc].set_xlim((0,2))
                ax[rr,cc].yaxis.set_visible(False)
        plt.savefig('Q.pdf', bbox_inches='tight')
            
        # |Q|
        print("|Q|")
        _,ax = plt.subplots(1,1)
        ax.hist([la.det(mm.Q) for mm in brain.chain[burn_in:]])
        ax.plot([la.det(true_model.Q)]*2, [0,len(brain.chain[burn_in:])], '-r')
        ax.set_xlim((-0.1,1))
        ax.yaxis.set_visible(False)
        plt.savefig('detQ.pdf', bbox_inches='tight')
        
def save_sparsity_matrix(brain, true_model, burn_in):
    
    d = min(3,brain.model.ds) # Don't plot more than 3x3. Too small
    
    if len(brain.chain)>burn_in:
        # B
        print("B")
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.B[rr,cc] for mm in brain.chain[burn_in:]],bins=3)
                ax[rr,cc].plot([true_model.B[rr,cc]]*2, [0,len(brain.chain[burn_in:])], '-r')
                ax[rr,cc].set_xlim((-0.2,1.2))
                ax[rr,cc].yaxis.set_visible(False)
                ax[rr,cc].locator_params(nbins=3)
        plt.savefig('sparseB.pdf', bbox_inches='tight')