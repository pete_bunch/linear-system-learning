# Standard modules
import numpy as np
import matplotlib.pyplot as plt

# My modules
from linear_models import *
from linear_models_mcmc import *
from kalman import GaussianDensity, Series

# Close all windows
plt.close("all")

# Set random seed
np.random.seed(1)

### SPECIFY A LINEAR MODEL ###
model_type = 1      # 0=basic, 1=sparse, 2=degenerate

# Basic parameters
K = 100         # Number of time instants in data
d=4             # Number of state dimensions

# Dense transition matrix
#A = np.array([[0.9,1],[0,0.9]])
#A = np.array([[0.9,0.5],[0,0.7]])
#A = np.array([[1,1],[0,1]])
#A = np.array([[0.5,0.1,0.1],[0,0.5,0.1],[0,0,0.5]])
#A = np.array([[0.9,0.8,0.7],[0,0.9,0.5],[0,0,0.8]])
#A = np.array([[1,1,1],[0,1,1],[0,0,1]])
#A = np.array([[1.1,0.5,0.2],[0,1,0.5],[0,0,1]])
#A = np.array([[0.9,0.7,0.7,0],[0,0.9,-0.5,0.1],[0,0,0.9,0.9],[0,0,0,0.9]])
#A = np.array([[0.97,-0.2,0],[0.2,0.97,0],[0,0,1]])
#A = np.array([[0.97,-0.2,0.3,0],[0.2,0.97,0,0],[0,0,0.8,-0.5],[0,0,0.5,0.8]])
A = np.array([[0.95,0.95,0,0],[0,0.95,0,0],[0,0,0.95,0.95],[0,0,0,0.95]])

# Transition matrix mask
#B = np.array([[1,1],[0,1]])
#B = np.ones((d,d))
B = np.ceil(abs(A))

Q = np.array([[1./3,1./2,0,0],[1./2,1,0,0],[0,0,1./3,1./2],[0,0,1./2,1]])
G = la.cholesky(Q).T

# Transition noise matrix
#G = np.diag([0.001,0.001,0.1])
#G = 0.5*np.identity(d) + 0.5*np.ones((d,d))
#G = 0.01*np.identity(d)
#givens = [np.pi/6, np.pi/3]
#G = np.array([[1]])
#givens = [np.pi/6, np.pi/3]
rank = 2
D = np.array([[1,0.5],[0.5,1]])
givens = [0.8,-0.5,0.3,0]
orient = np.ones(d)
#D = np.identity(2)
#givens = [0,0,0,0]

# Observation matrix
#H = np.array([[1,0]])
#H = np.array([[1,1],[0,1]])
#H = np.array([[1,1],[1,0],[0,1]])
H = np.identity(d)
#H = np.array([[1,0,1],[0,1,0],[0,0,1]])
#H = np.array([[1,0,1],[0,1,0]])
#H = np.array([[1,0,0,0],[0,0,1,0]])

# Observation covariance matrix
R = 1*np.identity(d)
#R = 0.01*np.identity(2)

#H = np.delete(H,[2,3],0)
#R = 0.01*np.identity(2)

# Initial state prior 
m0 = np.zeros(d)
P0 = 1*np.identity(d)

### Model priors ###
A_prior_vr = 1
B_prior_prob = 0.5
Q_prior_scale = 1
Q_prior_dof = d-1

### Algorithm parameters ###
giv_std = 0.05
pad_std = 0.01
rot_std = 0.002
adapt_batch_size = 10
num_iter = 1000
num_burn_in = int(num_iter/2)

### Initial Model Estimate ###
A_est = 0.9*np.identity(d)
B_est = np.ones((d,d))
G_est = 10*np.identity(d)
D_est = 10*np.identity(D.shape[0])
givens_est = [0]*len(givens)

#B_prior_prob = 0.5 * np.array([[1,1,1,1],[1,1,1,1],[0,0,1,1],[0,0,1,1]])
#G_est = G

###############################################################################

# Create model objects
first_state_prior = GaussianDensity(m0,P0)
if model_type == 0:
    model = LinearModel(first_state_prior, A, G, H, R)
    init_model_est = LinearModel(first_state_prior, A_est, G_est, H, R)
elif model_type == 1:
    model = SparseLinearModel(first_state_prior, A, B, G, H, R)
    init_model_est = SparseLinearModel(
                                  first_state_prior, A_est, B_est, G_est, H, R)
elif model_type == 2:
    model = DegenerateLinearModel(first_state_prior, A, D, givens, H, R)
    init_model_est = DegenerateLinearModel(
                             first_state_prior, A_est, D_est, givens_est, H, R)
else:
    raise ValueError("Invalid model type")

# Generate data
state, observ = model.simulate_data(K)

#dummy_model = model
#import linear_models_sampling as lmsmpl
#import scipy.linalg as la
#Q = lmsmpl.sample_wishart(d+2, np.identity(d))
#Qval,Qvec = la.eigh(Q)
#Qval = Qval[0:2]
#Qvec = Qvec[:,0:2]
#G = np.dot(Qvec,np.diag(np.sqrt(Qval)))
##G = np.array([[1,1,1,1],[2,2,2,2],[1,0,1,0],[-1,0,-1,0]])
#model = LinearModel(first_state_prior, A, G, H, R)
#state, observ = model.simulate_data(K)

# Create MCMC container object
brain = LinearModelMCMC(model_type, init_model_est, observ,
                        A_prior_vr=A_prior_vr, B_prior_prob=B_prior_prob,
                        Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof,
                        giv_std=giv_std, pad_std=pad_std, rot_std=rot_std)

# Gibbs sample the transition matrix
for ii in range(num_iter):

    print("")
    print("Iteration number %u" % (ii))
    
    if model_type == 2:
        # MCMC proposal adaptation
        if (((ii%adapt_batch_size)==0) and (ii>0)):
            brain.adapt_std(adapt_batch_size)
    
    # MCMC moves
    brain.iterate_transition_matrix(1)
    brain.iterate_transition_noise_matrix(1)
    if model_type == 2:
        brain.iterate_transition_noise_matrix_rank(1)
#    brain.iterate_observation_matrix(1)
    
    # Save current state of the chain
    brain.save_link()
    
    # Print current state
    print("")
    print("Transition matrix:")
    print(brain.model.F)
    print("Transition matrix eigenvalue magnitudes:")
    print(abs(np.linalg.eig(brain.model.F)[0]))
    print("Transition variance:")
    print(brain.model.Q)
    print("Transition variance eigenvalues:")
    print(np.linalg.eigh(brain.model.Q)[0])
    #print("Observation matrix:")
    #print(brain.model.H)
    if model_type == 2:
        print("Rank")
        print(brain.model.rank)
        print("Givens rotations")
        print(brain.model.givens)
        print("")
        print("Rotation proposal standard deviation:")
        print(brain.rot_std)
        print("Givens rotation proposal standard deviations:")
        print(brain.giv_std)
        print("Transition matrix padding standard deviation:")
        print(brain.pad_std)


# Plot state components
f = plt.figure()
Series.plot_sequence(state, range(d), f, 'k')

# Plot some MCMC output graphs
draw_chain(brain.chain, model, num_burn_in, ['F'])
draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['F'], nlags=10)
draw_chain_histogram(brain.chain, model, num_burn_in)
if model_type==2:
#    draw_chain(brain.chain, model, num_burn_in, ['givens'])
#    draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['givens'], nlags=100)
    draw_chain_acceptance([brain.F_acc], num_burn_in)
#    draw_chain_acceptance(brain.givens_acc, num_burn_in)
#    draw_chain_adaptation([brain.pad_std_chain], num_burn_in)
#    draw_chain_adaptation(brain.giv_std_chain, num_burn_in)
    

