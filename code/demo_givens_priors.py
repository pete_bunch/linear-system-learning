import numpy as np
from matplotlib import pyplot as plt

from linear_models import givensise

plt.close('all')

np.random.seed(13)

d = 8
r = 5
N = 100000
K = 100

Ur_list = []
Uc_list = []
E_list = []
givens_list = []

for ii in range(N):
    
#    X = np.random.normal(size=(d,r))
#    U,_,_ = np.linalg.svd(X)
    X = np.random.normal(size=(d,d))
    _,U = np.linalg.eigh(np.dot(X,X.T))
    U = np.delete(U,range(r,d),1)

    Ur,Uc,givens,order,E = givensise(U)
    
    Ur_list.append(Ur)
    Uc_list.append(Uc)
    E_list.append(E)
    givens_list.append(givens)
    
r_range = np.linspace(-np.pi/2,np.pi/2,K)
fig, ax = plt.subplots(d-r,r)
gg = 0
for (ii,jj) in order:
    ax[jj-r,ii].hist([giv[gg] for giv in givens_list], normed=True, bins=30,color='0.5')
#    x,bins,p = np.histogram([uu[jj,ii] for uu in Uc_list],density=True)
    
    pdf = np.cos(r_range)**(d-1-jj+ii)
    pdf = pdf/(np.sum(pdf)*(r_range[1]-r_range[0]))
    ax[jj-r,ii].plot(r_range, pdf, color='r', linewidth=2)
    
    ax[jj-r,ii].set_ylim([0,1.2])
    ax[jj-r,ii].set_xlim([-np.pi/2,np.pi/2])
    ax[jj-r,ii].xaxis.set_ticks([])
    ax[jj-r,ii].yaxis.set_ticks([])
    ax[jj-r,ii].set_xlabel((ii+1,jj+1))
    
    gg += 1
    
plt.savefig('../figures/givens_priors.pdf', bbox_inches='tight')


fig, ax = plt.subplots(1,r)
gg = 0
for ii in range(r):
    eii = [ee[gg,gg] for ee in E_list]
    ax[ii].hist(eii, normed=True, bins=2)
    
    count = sum(np.array(eii)>0)
    sd = (count-N/2)/np.sqrt(0.25*N)
    print(sd)
    
    gg += 1