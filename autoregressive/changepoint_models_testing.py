# Standard modules
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

# My modules
from changepoint_models import *
from changepoint_models_mcmc import *
from kalman import GaussianDensity, Series

# Close all windows
plt.close("all")

# Set random seed
np.random.seed(2)

# Basic parameters
K = 300         # Number of time instants in data

### Changepoints ###
cp = [100,200]
#cp = [50,100,200,250]
#cp = [40,80,120,220,260]

### Transition models ###
d = 6
poles = [np.array([0.9*np.exp(np.pi*0.0j),0.99*np.exp(np.pi*0.333j),0.99*np.exp(-np.pi*0.333j),0.9*np.exp(np.pi*0.167j),0.9*np.exp(-np.pi*0.167j)]), 
         np.array([0.9*np.exp(np.pi*0.0j),0.0*np.exp(np.pi*0.0j),0.0*np.exp(np.pi*0.0j),0.95*np.exp(np.pi*0.2j),0.95*np.exp(-np.pi*0.2j)]), 
         np.array([0.9*np.exp(np.pi*0.0j),0.9*np.exp(np.pi*0.4j),0.9*np.exp(-np.pi*0.4j),0.8*np.exp(np.pi*0.05j),0.8*np.exp(-np.pi*0.05j)]) ]
tf = [ sp.signal.zpk2tf([],pp,1)[1] for pp in poles ]
arc = [-tt[1:] for tt in tf]
ari = [3.0,1.5,1.0]

#d=4             # Number of state dimensions
#arc = [np.array([0.9,-0.5,0.2]), np.array([0.95,0.0,0.0]), np.array([0.7,-0.3,0.1])]
#ari = [1.0,1.5,1.0]

F = [ np.vstack(( np.hstack((aa[np.newaxis,:],[[0]])),np.hstack(( np.identity(d-1),np.zeros((d-1,1)) )) )) for aa in arc]
Q = [ np.zeros((d,d)) for ii in range(len(ari))]
for ii in range(len(ari)):
    Q[ii][0,0] = ari[ii]

### Other system parameters ###
H = np.zeros((1,d))
H[0,0] = 1
R = 30*np.identity(1)
m0 = np.zeros(d)
P0 = np.identity(d)
first_state_prior = GaussianDensity(m0, P0)

### Model priors ###
A_prior_vr = 3**2
Q_prior_scale = 3  # scale (beta)
Q_prior_dof = 4    # shape (alpha)
cp_prior_rate = 1./100.

### Algorithm parameters ###
num_iter = 250
num_burn_in = int(num_iter/5)
warm_up = 10

### Initial Model Estimate ###
#cp_est = [25,75,150,225,275]
#F_est = [0.9*np.identity(d)]*6
#Q_est = [10*np.identity(d)]*6
#cp_est = [75,150,225]
#F_est = [0.9*np.identity(d)]*4
#Q_est = [10*np.identity(d)]*4
#cp_est = [50,250]
#F_est = [0.9*np.identity(d)]*3
#Q_est = [10*np.identity(d)]*3
#cp_est = [150]
#F_est = [0.9*np.identity(d)]*2
#Q_est = [10*np.identity(d)]*2

arc_est = np.zeros(d-1)
arc_est[0] = 0.9
ari_est = 10
Fe = np.vstack(( np.hstack((arc_est[np.newaxis,:],[[0]])),np.hstack(( np.identity(d-1),np.zeros((d-1,1)) )) ))
Qe = np.zeros((d,d))
Qe[0,0] = ari_est

cp_est = []
F_est = [Fe]
Q_est = [Qe]

###############################################################################

# Create model objects
model = LinearChangepointModel(K, cp, first_state_prior, F, Q, H, R)
init_model_est = LinearChangepointModel(K, cp_est, first_state_prior, F_est, Q_est, H, R)

# Generate data
state,observ = model.simulate_data()

# Plot it
f_data = plt.figure(figsize=(8,3))
ax = f_data.add_subplot(1,1,1)
ax.plot( range(K), [x[0] for x in state], 'k-' )
ax.plot( range(K), [x[0] for x in observ], 'r.' )


## Filter and sample
#flt,_,_ = model.kalman_filter(observ)
#smp = model.sample_posterior(observ)
#GaussianDensity.plot_sequence(flt, range(d), f, colour="g")
#Series.plot_sequence(smp, range(d), f, colour="c")
#plt.show()

# Create MCMC container object
brain = LinearChangepointModelMCMC(init_model_est, observ, 
                         A_prior_vr, Q_prior_scale, Q_prior_dof, cp_prior_rate)

# Train the models for the initial guess
for ii in range(warm_up):
    brain.iterate_transition_matrix(1)
    brain.iterate_transition_noise_matrix(1)

# MCMC loop
for ii in range(num_iter):

    print("")
    print("Iteration number %u" % (ii))

    # MCMC moves
    brain.iterate_transition_matrix(1)
    brain.iterate_transition_noise_matrix(1)
    
#    brain.iterate_changepoints("add")
#    for nn in range(len(brain.model.point)):
#        brain.iterate_changepoints("shift", idx=nn)
#        brain.iterate_changepoints("adjust",idx=nn, half_width=3)
#    brain.iterate_changepoints("remove")
    
#    brain.iterate_changepoints_with_annealing("add", num_anneal_steps=0)
#    for nn in range(len(brain.model.point)):
#        brain.iterate_changepoints_with_annealing("shift", idx=nn, num_anneal_steps=0)
#        brain.iterate_changepoints_with_annealing("adjust",idx=nn, half_width=3, num_anneal_steps=0)
#    for nn in reversed(range(len(brain.model.point))):
#        brain.iterate_changepoints_with_annealing("remove", num_anneal_steps=0)
    
    brain.iterate_changepoints_with_annealing("add", num_anneal_steps=100)
    for nn in range(len(brain.model.point)):
        brain.iterate_changepoints_with_annealing("shift", idx=nn, num_anneal_steps=20)
        brain.iterate_changepoints_with_annealing("adjust",idx=nn, half_width=3, num_anneal_steps=0)
    for nn in reversed(range(len(brain.model.point))):
        brain.iterate_changepoints_with_annealing("remove", idx=nn, num_anneal_steps=100)
    
#    brain.iterate_changepoints_and_parameters("add")
#    for nn in range(len(brain.model.point)):
#        brain.iterate_changepoints_and_parameters("shift", idx=nn)
#        brain.iterate_changepoints_and_parameters("adjust",idx=nn, half_width=3)
#    brain.iterate_changepoints_and_parameters("remove")
    
    # Save current state of the chain
    brain.save_link()
    
    # Print current progress
    print("Changepoint set:")
    print(brain.model.point)
    
#    print(brain.model.order)
#    print(len(brain.model.F))
#
    
figs = draw_changepoint_occupation(brain.chain, model, num_burn_in, ylim=300)
f_chain = draw_chain_numcps(brain.chain, model, num_burn_in)
f_poles = draw_pole_locations(brain.chain, model, num_burn_in)

#f_data.savefig('data_ar.pdf', bbox_inches='tight')
#figs[0].savefig('cp_occupation_ar.pdf', bbox_inches='tight')
#figs[1].savefig('cp_number_ar.pdf', bbox_inches='tight')
#f_chain.savefig('cp_number_chain_ar.pdf', bbox_inches='tight')
f_poles.savefig('poles_ar.pdf', bbox_inches='tight')



#
## Plot state components
#f = plt.figure()
#Series.plot_sequence(state, range(d), f, 'k')
#
## Plot some MCMC output graphs
#draw_chain(brain.chain, model, num_burn_in, ['F'])
#draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['F'], nlags=10)
#draw_chain_histogram(brain.chain, model, num_burn_in)
#
