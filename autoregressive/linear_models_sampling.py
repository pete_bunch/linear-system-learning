import numpy as np
from scipy import linalg as la
from scipy import stats as sps
from scipy import misc as spm
from scipy import stats as stats
from scipy import special as spec
from scipy.stats import multivariate_normal as mvn
from numpy import random as rnd

### PRIORS ###

def sample_transition_matrix_prior(ds, pA, A=None):
    """Sample an AR transition matrix from a Gaussian prior"""
    P = pA*np.identity(ds-1)
    if not(type(A)==np.ndarray):
        Avec = mvn.rvs(mean=np.zeros(ds-1), cov=P)
        A = np.vstack(( np.hstack((Avec[np.newaxis,:],[[0]])),np.hstack(( np.identity(ds-1),np.zeros((ds-1,1)) )) ))
    else:
        Avec = A[0,:-1]
    pdf = mvn.logpdf(Avec,mean=np.zeros(ds-1),cov=P)
    return A,pdf

def sample_transition_covariance_prior(ds, nu0, pQ, Q=None):
    """Sample an AR transition covariance matrix from an inverse gamma prior"""
    if not(type(Q)==np.ndarray):
        vr = stats.invgamma.rvs(nu0,scale=pQ)
        Q = np.zeros((ds,ds))
        Q[0,0] = vr
    else:
        vr = Q[0,0]
    pdf = stats.invgamma.pdf(vr,nu0,scale=pQ)
    return Q,pdf

### BASIC LINEAR MODEL CONDTIONALS ###

def sample_transition_matrix_conditional(x, Q, pA, B=None, A=None):
    """Sample an AR transition matrix from its posterior conditional on the
       sampled state sequence. Also return the pdf of the sampled matrix."""
    K = len(x)
    ds = Q.shape[0]
    P0 = pA*np.identity(ds-1)
    
    ss1 = np.zeros(ds-1)
    ss2 = np.zeros((ds-1,ds-1))
    for kk in range(1,K):
        ss1 += x[kk][0]*x[kk-1][:-1]
        ss2 += np.outer(x[kk-1][:-1],x[kk-1][:-1])
    
    P = la.inv(la.inv(P0)+ss2)
    m = np.dot(P,ss1)
    
    if not(type(A)==np.ndarray):
        Avec = mvn.rvs(mean=m, cov=P)
        A = np.vstack(( np.hstack((Avec[np.newaxis,:],[[0]])),np.hstack(( np.identity(ds-1),np.zeros((ds-1,1)) )) ))
    else:
        Avec = A[0,:-1]
    pdf = mvn.logpdf(Avec,mean=m,cov=P)
    return A,pdf
    
def sample_transition_covariance_conditional(x, F, nu0, pQ, Q=None):
    """Sample an AR transition covariance matrix from its posterior conditional
       on the sampled state sequence"""
    K = len(x)
    ds = F.shape[0]

    ss0 = 0
    ss1 = 0
    for kk in range(1,K):
        ss0 += 1
        ss1 += ( x[kk][0]-np.dot(F,x[kk-1])[0] )**2
    
    alpha = nu0 + 0.5*ss0
    beta = pQ + 0.5*ss1
    
    if not(type(Q)==np.ndarray):
        vr = stats.invgamma.rvs(alpha,scale=beta)
        Q = np.zeros((ds,ds))
        Q[0,0] = vr
    else:
        vr = Q[0,0]
    pdf = stats.invgamma.pdf(vr,alpha,scale=beta)
    return Q,pdf
    
def sample_observation_covariance_scale_conditional(x, y, H, nu0, pR):
    
    shape = nu0
    rate = pR
    for kk in range(len(y)):
        if len(y[kk])>0:
            shape += len(y[kk])
            rate += la.norm( y[kk]-np.dot(H[kk],x[kk]) )**2
    Rinvscale = stats.gamma.rvs(shape,scale=1./rate)
    Rscale = 1./Rinvscale
    return Rscale
    
### ANNEALED CHANGEPOINT SYSTEM LEARNING ###
def sample_annealed_transition_matrix_conditional(x, idx1, idx2, Q, pA, gamma):
    K = len(x)
    ds = Q.shape[0]
    P0 = pA*np.identity(ds-1)
    
    ss1 = np.zeros(ds-1)
    ss2 = np.zeros((ds-1,ds-1))
    for kk in range(1,K):
        if (kk in idx1) and (kk in idx2):
            sf = 1
        elif (kk in idx1):
            sf = (1-gamma)
        elif (kk in idx2):
            sf = gamma
        else:
            continue
        
        ss1 += sf*x[kk][0]*x[kk-1][:-1]
        ss2 += sf*np.outer(x[kk-1][:-1],x[kk-1][:-1])
    
    P = la.inv(la.inv(P0)+ss2)
    m = np.dot(P,ss1)
    
    Avec = mvn.rvs(mean=m, cov=P)
    A = np.vstack(( np.hstack((Avec[np.newaxis,:],[[0]])),np.hstack(( np.identity(ds-1),np.zeros((ds-1,1)) )) ))
    return A

def sample_annealed_transition_covariance_conditional(x, idx1, idx2, F, nu0, pQ, gamma):
    K = len(x)
    ds = F.shape[0]

    ss0 = 0
    ss1 = 0
    for kk in range(1,K):
        if (kk in idx1) and (kk in idx2):
            sf = 1
        elif (kk in idx1):
            sf = (1-gamma)
        elif (kk in idx2):
            sf = gamma
        else:
            continue
        
        ss0 += sf
        ss1 += sf*( x[kk][0]-np.dot(F,x[kk-1])[0] )**2
    
    alpha = nu0 + 0.5*ss0
    beta = pQ + 0.5*ss1
    
    vr = stats.invgamma.rvs(alpha,scale=beta)
    Q = np.zeros((ds,ds))
    Q[0,0] = vr
    return Q
    
    

### BASIC SAMPLING OPERATIONS ###

def sample_wishart(nu, P):
    dim = P.shape[0]
    cholP = la.cholesky(P,lower=True)
    R = np.zeros((dim,dim))
    for ii in range(dim):
        R[ii,ii] = np.sqrt(sps.chi2.rvs(nu-(ii+1)+1))
        for jj in range(ii+1,dim):
            R[jj,ii] = sps.norm.rvs()
    cholX = np.dot(cholP,R)
    X = np.dot(cholX,cholX.T)
    return X
    
def wishart_density(X,nu,P):
    d = X.shape[0]
    pdf = 0.5*(nu-d-1)*np.log(la.det(X)) - 0.5*np.trace(la.solve(P,X)) \
         -0.5*d*nu*np.log(2) - 0.5*nu*np.log(la.det(P)) - spec.multigammaln(nu/2,d)
    return pdf

def sample_cayley(d, s):
    """Sample a random Cayley-distributed orthogonal matrix"""
    
    # Random skew-symmetric matrix
    S = np.zeros((d,d))
    for dd in range(d-1):
        y = mvn.rvs(mean=np.zeros(d-dd-1), cov=s**2*np.identity(d-dd-1))
        S[dd,dd+1:] = y
        S[dd+1:,dd] = -y
    
    # Cayley transformation
    I = np.identity(d)
    M = la.solve(I-S,I+S)
    
    return M
    
#    # Scalar case
#    if d == 1:
#        M = 1
#        return M
#    
#    # Initialise in 2 dimensions
#    y = stats.t.rvs(k)
#    S
#    
#    # Loop up throu
#    for dd in range(d):
#        
#    
#    return M
    
    
def sample_truncated_gamma(a,b,c):
    """Sample from a truncated gamma distribution"""
    lb = stats.gamma.cdf(c,a,scale=b)
    u = stats.uniform.rvs(loc=lb,scale=1-lb)
    x = stats.gamma.ppf(u,a,scale=b)
    return x