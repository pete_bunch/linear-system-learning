import matplotlib.pyplot as plt
import numpy as np
import kalman as kal
import linear_models_sampling as sysl
from scipy import linalg as la
from scipy import stats
from scipy import signal as spsig
from scipy.stats import multivariate_normal as mvn
import statsmodels.tsa.stattools as st
from changepoint_models import LinearChangepointModel
from kalman import GaussianDensity, Series

class LinearChangepointModelMCMC:
    """Container Class for Linear Changepoint Model Learning with MCMC"""

    def __init__(self, init_model_est, observ, A_prior_vr=None, Q_prior_scale=None, Q_prior_dof=None, cp_prior_rate=None):
        self.model = init_model_est.copy()
        self.observ = observ

        self.chain = []
        self.chain.append(self.model.copy())        
        
        self.A_prior_vr = A_prior_vr
        self.Q_prior_scale = Q_prior_scale # This is the scalar p such that the Q^{-1} is Wishart with mean nu*p*I (i.e. phi in the paper)
        self.Q_prior_dof = Q_prior_dof
        self.cp_prior_rate = cp_prior_rate
        
        self.state = self.model.sample_posterior(self.observ)
            
        
    def save_link(self):
        """Save the current state of the model as a link in the chain"""
        self.chain.append(self.model.copy())
    
    ### MCMC moves ###
    
    def iterate_transition_matrix(self, Mtimes):
        """Run Gibbs iterations on transition matrix"""
        
        if not self.A_prior_vr:
            raise ValueError("A transition matrix prior must be supplied in order to sample")
        
        mie = self.model.model_in_effect
        x = self.state
        
        for mm in range(Mtimes):
            
            for nn in range(len(self.model.F)):
                
                # Find relevant time instants
                idx = select_model_instants(nn, mie)
                
                # Sample a new transition matrix
                self.model.F[nn],_ = sysl.sample_transition_matrix_conditional(x[idx], self.model.Q[nn], self.A_prior_vr)
                
            # Sample the state sequence
            x = self.model.sample_posterior(self.observ)
                
        self.state = x
                
    
    def iterate_transition_noise_matrix(self, Mtimes):
        """Run Gibbs iterations on the transition noise matrix"""
        
        if ((self.Q_prior_dof==None) or (self.Q_prior_scale==None)):
            raise ValueError("A transition covariance matrix prior must be supplied in order to sample")
        
        mie = self.model.model_in_effect
        x = self.state
        
        for mm in range(Mtimes):
            
            for nn in range(len(self.model.Q)):
                
                # Which instants use this model?
                idx = select_model_instants(nn, mie)
            
                # Sample a new covariance matrix
                self.model.Q[nn],_ = sysl.sample_transition_covariance_conditional(x[idx], self.model.F[nn], self.Q_prior_dof, self.Q_prior_scale)
                
            # Sample the state sequence
            x = self.model.sample_posterior(self.observ)
                
        self.state = x
    
    def iterate_changepoints(self, move_type, idx=None, half_width=np.inf):
        """Run MH interations on the changepoint sequence"""
        
        # Copy the model
        ppsl_model = self.model.copy()
        
        # Propose a change of some kind
        if move_type == "shift":
            
            _,valid,fwd_ppsl,bwd_ppsl = ppsl_model.propose_shift(idx)
            
        elif move_type == "adjust":
            
            _,valid,fwd_ppsl,bwd_ppsl = ppsl_model.propose_adjust(idx, half_width)
            
        elif move_type == "add":
            
            # idx is ignored (just propose uniformly across time)
            
            # Sample a new system to go with it - this is not part of the proposal in our extended system paradigm, it was always there
            A,_ = sysl.sample_transition_matrix_prior(ppsl_model.ds, self.A_prior_vr)
            Q,_ = sysl.sample_transition_covariance_prior(ppsl_model.ds, self.Q_prior_dof, self.Q_prior_scale)
            
            _,valid,fwd_ppsl,bwd_ppsl = ppsl_model.propose_add(A, Q)

            
        elif move_type == "remove":
            
            # idx is ignored (we have to ensure balance with the add move)
            
            _,valid,fwd_ppsl,bwd_ppsl = ppsl_model.propose_remove()
        
        else:
            raise ValueError("Invalid move type")
        
        print("MH Move - {}: {}".format(move_type, ppsl_model.point))        
        
        if valid:
            _,_,old_lhood = self.model.kalman_filter(self.observ)
            old_cp_prior = self.model.changepoint_prior(self.cp_prior_rate)        
        
            flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            new_cp_prior = ppsl_model.changepoint_prior(self.cp_prior_rate)
        
            ap = (new_lhood+new_cp_prior) - (old_lhood+old_cp_prior) + (bwd_ppsl-fwd_ppsl)
        else:
            ap = -np.Infinity
            
        u = np.log(np.random.random_sample())
        print("    Acceptance probability: {}".format(ap))
        if u < ap:
            self.model = ppsl_model
            self.state = self.model.backward_simulation(flt)
            print("    Accepted")
        else:
            print("    Rejected")
            
            
            
    def iterate_changepoints_and_parameters(self, move_type, idx=None, half_width=np.inf):
        """Run MH interations on the changepoint sequence and affected 
        parameters"""
        
        # Copy the model - orig_model is self.model but padded where necessary with the extra parameters
        ppsl_model = self.model.copy()
        orig_model = self.model.copy()
        
       # Propose a change of some kind
        if move_type == "shift":
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_shift(idx)
            
        elif move_type == "adjust":
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_adjust(idx, half_width)
            
        elif move_type == "add":
            
            # idx is ignored
            
            # Sample a new system to go with it - this is not part of the proposal in our extended system paradigm, it was always there
            A,_ = sysl.sample_transition_matrix_prior(ppsl_model.ds, self.A_prior_vr)
            Q,_ = sysl.sample_transition_covariance_prior(ppsl_model.ds, self.Q_prior_dof, self.Q_prior_scale)
            orig_model.F.append(A)
            orig_model.Q.append(Q)
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_add(A, Q)

            
        elif move_type == "remove":
            
            # idx is ignored (we have to ensure balance with the add move)
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_remove()
        
        else:
            raise ValueError("Invalid move type")
        
        print("MH Move - {}: {}".format(move_type, ppsl_model.point))        
        
        if valid:
            
            # Which models apply where?
            old_mie = orig_model.model_in_effect
            new_mie = ppsl_model.model_in_effect
            
            old_idx = []
            new_idx = []
            
            for jj,nn in enumerate(aff):
                
                old_idx.append(select_model_instants(nn, old_mie))
                new_idx.append(select_model_instants(nn, new_mie))
            
            # Create probability arrays
            fwd_F_ppsl = np.zeros(2)
            bwd_F_ppsl = np.zeros(2)
            fwd_Q_ppsl = np.zeros(2)
            bwd_Q_ppsl = np.zeros(2)
            
            # Loop through affected models proposing new parameters
            for jj,nn in enumerate(aff):
                
                # Sample new system values
                x = self.state
                ppsl_model.F[nn],fwd_F_ppsl[jj] = sysl.sample_transition_matrix_conditional(x[new_idx[jj]], orig_model.Q[nn], self.A_prior_vr)
                ppsl_model.Q[nn],fwd_Q_ppsl[jj] = sysl.sample_transition_covariance_conditional(x[new_idx[jj]], orig_model.F[nn], self.Q_prior_dof, self.Q_prior_scale)
                
            # Sample a new state
            flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            x_new = ppsl_model.backward_simulation(flt)
            
            # Loop through affected models calculating reverse probabilities
            for jj,nn in enumerate(aff):
                
                
                # Calculate reverse proposal probabilities
                _,bwd_F_ppsl[jj] = sysl.sample_transition_matrix_conditional(x_new[old_idx[jj]], ppsl_model.Q[nn], self.A_prior_vr, A=orig_model.F[nn])
                _,bwd_Q_ppsl[jj] = sysl.sample_transition_covariance_conditional(x_new[old_idx[jj]], ppsl_model.F[nn], self.Q_prior_dof, self.Q_prior_scale, Q=orig_model.Q[nn])
                
            # Easy probabilities
            _,_,old_lhood = orig_model.kalman_filter(self.observ)
            old_cp_prior = orig_model.changepoint_prior(self.cp_prior_rate)    
            new_cp_prior = ppsl_model.changepoint_prior(self.cp_prior_rate)
            
            # Calculate priors
            new_F_prior = np.zeros(2)
            old_F_prior = np.zeros(2)
            new_Q_prior = np.zeros(2)
            old_Q_prior = np.zeros(2)
            for jj,nn in enumerate(aff):
                _,new_F_prior[jj] = sysl.sample_transition_matrix_prior(ppsl_model.ds, self.A_prior_vr, A=ppsl_model.F[nn])
                _,old_F_prior[jj] = sysl.sample_transition_matrix_prior(ppsl_model.ds, self.A_prior_vr, A=orig_model.F[nn])
                _,new_Q_prior[jj] = sysl.sample_transition_covariance_prior(ppsl_model.ds, self.Q_prior_dof, self.Q_prior_scale, Q=ppsl_model.Q[nn])
                _,old_Q_prior[jj] = sysl.sample_transition_covariance_prior(ppsl_model.ds, self.Q_prior_dof, self.Q_prior_scale, Q=orig_model.Q[nn])
            
            # Acceptance probability
            ap = + (new_lhood+new_cp_prior+sum(new_F_prior)+sum(new_Q_prior)) \
                 - (old_lhood+old_cp_prior+sum(old_F_prior)+sum(old_Q_prior)) \
                 + (bwd_cp_ppsl+sum(bwd_F_ppsl)+sum(bwd_Q_ppsl)) \
                 - (fwd_cp_ppsl+sum(fwd_F_ppsl)+sum(fwd_Q_ppsl))
        else:
            ap = -np.inf
            
        u = np.log(np.random.random_sample())
        print("    Acceptance probability: {}".format(ap))
        if u < ap:
            self.model = ppsl_model
            self.state = x_new
            print("    Accepted")
        else:
            print("    Rejected")
            
            
            
    def iterate_changepoints_with_annealing(self, move_type, idx=None, half_width=np.inf, num_anneal_steps=10):
        """Run MH interations on the changepoint sequence"""
        
        # Copy the model
        ppsl_model = self.model.copy()
        orig_model = self.model.copy()
        
        # Propose a change of some kind
        if move_type == "shift":
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_shift(idx)
            
        elif move_type == "adjust":
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_adjust(idx, half_width)
            
        elif move_type == "add":
            
            # idx is ignored (just propose uniformly across time)
            
            # Sample a new system to go with it - this is not part of the proposal in our extended system paradigm, it was always there
            A,_ = sysl.sample_transition_matrix_prior(ppsl_model.ds, self.A_prior_vr)
            Q,_ = sysl.sample_transition_covariance_prior(ppsl_model.ds, self.Q_prior_dof, self.Q_prior_scale)
            orig_model.F.append(A)
            orig_model.Q.append(Q)
            
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_add(A, Q)
            
            # Correction if we're trying to remove each changepoint
            fwd_cp_ppsl -= np.log(len(ppsl_model.point))

            
        elif move_type == "remove":
            
            # idx is ignored (we have to ensure balance with the add move)
            #aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_remove()

            # If we're trying to remove each changepoint
            aff,valid,fwd_cp_ppsl,bwd_cp_ppsl = ppsl_model.propose_remove(idx)
            bwd_cp_ppsl -= np.log(len(self.model.point))
        
        else:
            raise ValueError("Invalid move type")
        
        print("MH Move with annealing - {}: {}".format(move_type, ppsl_model.point))        
        
        if valid:
            
            # Which models apply where?
            old_mie = orig_model.model_in_effect
            new_mie = ppsl_model.model_in_effect
            
            old_idx = []
            new_idx = []
            
            for jj,nn in enumerate(aff):
                
                old_idx.append(select_model_instants(nn, old_mie))
                new_idx.append(select_model_instants(nn, new_mie))
            
            ######TESTING######
            
            ### ANNEALING ###
            
            anneal_ratios = []
        
            # Loop over the annealing steps
            for tt in range(num_anneal_steps):
#                anfact = float(tt+1)/num_anneal_steps
                base = 0.5
                anfact = (base**(float(num_anneal_steps)/float(tt+1)))/base
                
#                print(anfact)
            
                # Run annealed-model Kalman filter
                flt,_,old_tmp_lhood = annealed_kalman_filter(ppsl_model.P1, ppsl_model.H, ppsl_model.R,
                                    self.observ, anfact, old_mie, new_mie, ppsl_model.F, ppsl_model.Q)
            
                # Annealed backward simulation
                x = annealed_backward_simulation(flt, self.observ, anfact, 
                                  old_mie, new_mie, ppsl_model.F, ppsl_model.Q)
                
                # Sample transition matrices
                for jj,nn in enumerate(aff):
#
#                    print(ppsl_model.F[nn])
#                    
                    if anfact==1.0:
                        ppsl_model.F[nn],_ = sysl.sample_transition_matrix_conditional(x[new_idx[jj]], ppsl_model.Q[nn], self.A_prior_vr)
                        ppsl_model.Q[nn],_ = sysl.sample_transition_covariance_conditional(x[new_idx[jj]], ppsl_model.F[nn], self.Q_prior_dof, self.Q_prior_scale)
                    else:
                        ppsl_model.F[nn] = sysl.sample_annealed_transition_matrix_conditional(x, old_idx[jj], new_idx[jj], ppsl_model.Q[nn], self.A_prior_vr, anfact)
                        ppsl_model.Q[nn] = sysl.sample_annealed_transition_covariance_conditional(x, old_idx[jj], new_idx[jj], ppsl_model.F[nn], self.Q_prior_dof, self.Q_prior_scale, anfact)
#                        
#                    print(ppsl_model.F[nn])
#                    
                # Run annealed-model Kalman filter
                _,_,new_tmp_lhood = annealed_kalman_filter(ppsl_model.P1, ppsl_model.H, ppsl_model.R,
                                  self.observ, anfact, old_mie, new_mie, ppsl_model.F, ppsl_model.Q)
                
                # Calculate tempering term
                anneal_ratios.append(old_tmp_lhood-new_tmp_lhood)
            
            #################
            _,_,old_lhood = self.model.kalman_filter(self.observ)
            old_cp_prior = self.model.changepoint_prior(self.cp_prior_rate)        
        
            flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            new_cp_prior = ppsl_model.changepoint_prior(self.cp_prior_rate)
        
            ap = (new_lhood+new_cp_prior) - (old_lhood+old_cp_prior) + (bwd_cp_ppsl-fwd_cp_ppsl) + sum(anneal_ratios)
        else:
            ap = -np.Infinity
            
        u = np.log(np.random.random_sample())
        print("    Acceptance probability: {}".format(ap))
#        print("    Anneal ratios: {}".format(anneal_ratios))
        if u < ap:
            self.model = ppsl_model
            self.state = self.model.backward_simulation(flt)
            print("    Accepted")
        else:
            print("    Rejected")
        
#        print(self.model.point)
#        print(self.model.order)
#        print(len(self.model.F))
            
            
### ANNEALING ROUTINES ###
def annealed_kalman_filter(P1, H, R, observ, anfact, mie1, mie2, F_list, Q_list):
    """Run a Kalman filter on an annealed composite of two changepoint models"""
    K = len(observ)
    ds = F_list[0].shape[0]
    flt = GaussianDensity.new_sequence(ds, K)
    prd = GaussianDensity.new_sequence(ds, K)
    
    prd_lhood_seq = np.zeros(K)
    flt_lhood_seq = np.zeros(K)
    
    # Loop through frames
    for kk in range(K):
        if kk > 0:
            if mie1[kk]==mie2[kk]:
                prd[kk] = kal.predict(flt[kk-1], F_list[mie1[kk]], Q_list[mie1[kk]])
            else:
                # Annealed Kalman filter prediction
                F1 = F_list[mie1[kk]]
                F2 = F_list[mie2[kk]]
                Q1 = Q_list[mie1[kk]]
                Q2 = Q_list[mie2[kk]]
                
                prd[kk],prdPrec = kal.annealed_predict(flt[kk-1], F1, F2, Q1, Q2, anfact)
                
                if la.det(prd[kk].vr)<0:
                    print(kk)
                    print(prd[kk].vr)
                    print(la.eig(prd[kk].vr)[0])
                    print(Q1)
                    print(la.eig(Q1)[0])
                    print(Q2)
                    print(la.eig(Q2)[0])
                    print(anfact)
                
                fltPm = la.solve(flt[kk-1].vr,flt[kk-1].mn)
                prdPm = la.solve(prd[kk].vr,prd[kk].mn)
                prd_lhood_seq[kk] = 0.5*( np.dot(prd[kk].mn.T, prdPm ) - np.dot( fltPm.T, np.dot( flt[kk-1].vr-la.inv(prdPrec) ,fltPm) ) )\
                                -0.5*np.log(la.det(prdPrec)) + 0.5*np.log(la.det(prd[kk].vr)) - 0.5*np.log(la.det(flt[kk-1].vr)) \
                                -0.5*(1-anfact)*np.log(la.det(Q1)) -0.5*anfact*np.log(la.det(Q2))
                                
        else:
            # First frame
            prd[kk] = P1
        flt[kk],innov = kal.correct(prd[kk], observ[kk], H, R)
        d = innov.vr.shape[0]
        try:
            flt_lhood_seq[kk] = mvn.logpdf(observ[kk], innov.mn, 1E-8*np.identity(d) + innov.vr)
        except:
            print(kk)
            print(innov.vr)
            print(innov.vr-innov.vr.T)
            print(la.eig(innov.vr)[0])
        
#    print(prd_lhood_seq)
#    print(flt_lhood_seq)
        
    lhood = sum(prd_lhood_seq) + sum(flt_lhood_seq)
    return flt,prd,lhood

def annealed_backward_simulation(flt, observ, anfact, mie1, mie2, F_list, Q_list):
    """Run backward simulations on an annealed composite of two changepoint models"""
    K = len(observ)
    ds = F_list[0].shape[0]
    x = Series.new_sequence(ds, K)
    for kk in reversed(range(K)):
        if kk < K-1:
            if mie1[kk+1]==mie2[kk+1]:
                samp_dens,_ = kal.correct(flt[kk], x[kk+1], F_list[mie1[kk+1]], Q_list[mie1[kk+1]])
            else:
                F1 = F_list[mie1[kk+1]]
                F2 = F_list[mie2[kk+1]]
                Q1 = Q_list[mie1[kk+1]]
                Q2 = Q_list[mie2[kk+1]]
                samp_dens = kal.annealed_update(flt[kk], x[kk+1], F1, F2, Q1, Q2, anfact)
        else:
            samp_dens = flt[kk]
        x[kk] = mvn.rvs(samp_dens.mn,samp_dens.vr)
    return x    

#################

def select_model_instants(nn, mie):
    if ((mie[0]==nn) or not (nn in mie)): # We need to take the state one instant BEFORE the model comes into effect, assuming it exists.
        idx = []
    else:
        idx = [mie.index(nn)-1]
    idx.extend([ii for ii,mod in enumerate(mie) if mod==nn])
    return idx



def draw_pole_locations(chain, true_model, burn_in):
    
    true_num_mods = len(true_model.order)
    
    poles = [[] for pp in range(true_num_mods)]
    for mm in chain:
        if (len(mm.order)==true_num_mods) and (mm>=burn_in):
            for ii in range(true_num_mods):
                arc = np.concatenate((np.asarray([1]), -mm.F[ii][0,:-1]))
                _,pp,_ = spsig.tf2zpk([1],arc)
                poles[ii].append(pp)
    
    fig = plt.figure(figsize=(8,4))
    for ii in range(true_num_mods):
        ax = fig.add_subplot(1,3,ii+1)
        
        ax.set_xlim((0,1.1))
        ax.set_ylim((-1.1,1.1))
        
        unit_circle = plt.Circle((0,0), 1, facecolor='1.0', edgecolor='0.5')
        ax.add_patch(unit_circle)        
        
        for jj in range(len(poles[ii])):
            ax.plot( [pls.real for pls in poles[ii][jj]], [pls.imag for pls in poles[ii][jj]], '+k' )
        
        arc = np.concatenate((np.asarray([1]), -true_model.F[ii][0,:-1]))
        _,pp,_ = spsig.tf2zpk([1],arc)
        ax.plot( [pls.real for pls in pp], [pls.imag for pls in pp], 'xr', markersize=10 )
    
    return fig



def draw_changepoint_occupation(chain, true_model, burn_in, ylim=None):
    
    if ylim is None:
        ylim = len(chain[burn_in:])
    
    figs = []
    
    all_cps = []
    for ii in range(burn_in,len(chain)):
        all_cps.extend(chain[ii].point)
    
    f = plt.figure()
    figs.append(f)
    ax = f.add_subplot(1,1,1)
    ax.hist(all_cps,bins=true_model.K,range=(0-0.000001,true_model.K+0.000001),color='0.5')
    for pp in range(len(true_model.point)):
        ax.plot([true_model.point[pp]]*2, [0,ylim], '-r')
        
    ax.set_xlim([0,true_model.K])
    ax.set_ylim([0,ylim])
    
    num_cps = [len(mm.point) for mm in chain]
    bins = np.array(range(0,np.max(num_cps)+2))-0.5
    cph = np.histogram(num_cps[burn_in:], bins=bins)
    
    wid = 0.8;
    f = plt.figure()
    figs.append(f)
    ax = f.add_subplot(1,1,1)
    ax.bar(cph[1][:-1]+(0.5-wid/2),cph[0],width=wid,color='0.5')
    ax.plot([len(true_model.point)]*2, [0,len(chain[burn_in:])], '-r')
    ax.set_xlim([-0.5,np.max(num_cps)+1.5])
    ax.set_ylim([0,len(chain[burn_in:])])
    ax.locator_params(axis='x',nbins=len(bins)+1)
    
    return figs

def draw_chain_numcps(chain, true_model, burn_in):
    chain_num_cps = [len(mm.point) for mm in chain]
    f = plt.figure()
    ax = f.add_subplot(1,1,1)
    ax.plot(chain_num_cps)
    ax.set_ylim([0,np.max(chain_num_cps)+1])
    ax.plot([burn_in]*2, [0,np.max(chain_num_cps)+1], '-r')
    
    return f





            
### DISPLAY ###
def draw_chain_histogram(chain, true_model, burn_in):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        # F
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.F[rr,cc] for mm in chain[burn_in:]])
                ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
#                ax[rr,cc].set_xlim((-2,2))
                
        # Q
        _,ax = plt.subplots(d,d,squeeze=False)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].hist([mm.Q[rr,cc] for mm in chain[burn_in:]])
                ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
#                ax[rr,cc].set_xlim((-2,2))
        
        # |Q|
        _,ax = plt.subplots(1,1)
        ax.hist([la.det(mm.Q) for mm in chain[burn_in:]])
        ax.plot([la.det(true_model.Q)]*2, [0,len(chain[burn_in:])], '-r')
        
        if type(true_model) == SparseLinearModel:
            
            # B
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.B[rr,cc] for mm in chain[burn_in:]])
                    ax[rr,cc].plot([true_model.B[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
        
        if type(true_model) == DegenerateLinearModel:
        
            # D
            d = min(3,true_model.rank)
            _,ax = plt.subplots(d,d,squeeze=False)
            for rr in range(d):
                for cc in range(d):
                    ax[rr,cc].hist([mm.D[rr,cc] for mm in chain[burn_in:]])
                    ax[rr,cc].plot([true_model.D[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
            
            # givens
            _,ax = plt.subplots(1,len(true_model.givens),squeeze=False)
            for cc in range(len(true_model.givens)):
                ax[0,cc].hist([mm.givens[cc] for mm in chain[burn_in:]])
                ax[0,cc].plot([true_model.givens[cc]]*2, [0,len(chain[burn_in:])], '-r')
#                ax[0,cc].set_xlim((-np.pi,np.pi))


def draw_chain(chain, true_model, burn_in, fields):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        
        for ff in fields:
            if ff in ['F','Q','G','D']:
                d = getattr(true_model, ff).shape[0]
                d = min(4,d)
                _,ax = plt.subplots(d,d,squeeze=False)
                for rr in range(d):
                    for cc in range( getattr(chain[-1],ff).shape[1] ):
                        ax[rr,cc].plot([getattr(mm,ff)[rr,cc] for mm in chain])
                        ax[rr,cc].plot([0,len(chain)], [getattr(true_model,ff)[rr,cc]]*2, '-r')
            elif ff in ['givens']:
                d = len(getattr(true_model,ff))
                fig = plt.figure()
                for dd in range(d):
                    ax = fig.add_subplot(d,1,dd+1)
                    ax.plot([getattr(mm,ff)[dd] for mm in chain])
                    ax.plot([0,len(chain)], [getattr(true_model,ff)[dd]]*2, 'r')
    

def draw_chain_autocorrelation(chain, true_model, burn_in, fields, nlags=30):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        
        for ff in fields:
            if ff in ['F','Q','G','D']:
                d = getattr(true_model, ff).shape[0]
                d = min(4,d)
                _,ax = plt.subplots(d,d,squeeze=False)
                for rr in range(d):
                    for cc in range( getattr(chain[-1],ff).shape[1] ):
                        seq = [getattr(mm,ff)[rr,cc] for mm in chain[burn_in:]]
                        ac = st.acf(seq,unbiased=False,nlags=nlags,)
                        ax[rr,cc].plot(range(len(ac)),ac,'k')
                        ax[rr,cc].plot(range(len(ac)),[0]*len(ac),':k')
                        ax[rr,cc].set_ylim([-0.1,1])
            elif ff in ['givens']:
                d = len(getattr(true_model,ff))
                fig = plt.figure()
                for dd in range(d):
                    ax = fig.add_subplot(d,1,dd+1)
                    seq = [getattr(mm,ff)[dd] for mm in chain[burn_in:]]
                    ac = st.acf(seq,unbiased=False,nlags=nlags,)
                    ax.plot(range(len(ac)),ac,'k')
                    ax.plot(range(len(ac)),[0]*len(ac),':k')
                    ax.set_ylim([-0.1,1])


def draw_chain_acceptance(chain_acc, burn_in):
    d = len(chain_acc)
    ax = plt.figure().add_subplot(1,1,1)
    maxval = 0
    for dd in range(d):
        acc_array = np.array(chain_acc[dd])
        ax.plot(np.cumsum(acc_array))
        maxval = max(maxval,acc_array.sum())
    ax.plot([burn_in]*2,[0,maxval])


def draw_chain_adaptation(chain_adpt, burn_in):
    d = len(chain_adpt)
    ax = plt.figure().add_subplot(1,1,1)
    maxval = 0
    for dd in range(d):
        adpt_array = np.array(chain_adpt[dd])
        ax.plot(adpt_array)
        maxval = max(maxval,adpt_array.max())
    ax.plot([burn_in]*2,[0,maxval])
