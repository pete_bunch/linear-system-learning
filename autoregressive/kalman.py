# This module contains basic subroutines for inference in linear-Gaussian sequential models.
# predict       - Kalman filter prediction
# update        - Kalman filter update
# correct       - Rauch-Tung-Striebel smoothing correction

# These all operate on a instances of a namedtuple subclass called GaussianDensity

# The focus is speed and simplicity. No error checking. No special cases. No batch processing.

import numpy as np
from scipy import linalg as la
import collections

class GaussianDensity(collections.namedtuple('GaussianSequence', ['mn', 'vr'])):
    @property
    def dim(self):
        return len(self.mn)
        
    @classmethod
    def new_sequence(self, dim, K):
        blank = GaussianDensity(mn=np.zeros(dim), vr=np.zeros((dim,dim)))
        seq = [blank.copy() for kk in range(K)]
        return seq
        
    def copy(self):
        return GaussianDensity(self.mn, self.vr)
        
    @classmethod
    def plot_sequence(self, seq, dim, f, colour=""):
        K = len(seq)
        D = len(dim)
        for dd in range(D):
            ax = f.add_subplot(D,1,dd+1)
            ax.locator_params(axis='y', nbins=4)
            ax.plot( range(K), [x.mn[dim[dd]] for x in seq], colour+'-' )
            ax.plot( range(K), [x.mn[dim[dd]]+2*np.sqrt(x.vr[dim[dd],dim[dd]]) for x in seq], colour+':' )
            ax.plot( range(K), [x.mn[dim[dd]]-2*np.sqrt(x.vr[dim[dd],dim[dd]]) for x in seq], colour+':' )
            ax.set_xlim([0,K])



class Series(np.ndarray):
    @classmethod
    def new_sequence(self, dim, K):
        seq = np.zeros((K,dim))
#        blank = Series(dim)
#        seq = [blank.copy() for kk in range(K)]
        return seq
        
    @classmethod
    def plot_sequence(self, seq, dim, f, colour=""):
        
        K = len(seq)
        D = len(dim)
        for dd in range(D):
            ax = f.add_subplot(D,1,dd+1)
            ax.locator_params(axis='y', nbins=4)
            ax.plot( range(K), seq[:,dim[dd]], colour )
            ax.set_xlim([0,K])


def predict(dens, A, Q):
    """Kalman filter prediction"""
    prd_mn = np.dot(A,dens.mn)
    prd_vr = np.dot(A, np.dot(dens.vr,A.T) ) + Q
    prd_vr = (prd_vr+prd_vr.T)/2
    return GaussianDensity(prd_mn,prd_vr)

def correct(dens, y, H, R):
    """Kalman filter correction"""
    mu = np.dot(H,dens.mn)
    S = R + np.dot(H, np.dot(dens.vr,H.T) )
    S = (S+S.T)/2
    G = np.dot(dens.vr, la.solve(S,H,check_finite=False).T )
    I = np.identity(dens.dim)
    upd_vr = np.dot( I-np.dot(G,H), dens.vr)
    upd_vr = (upd_vr+upd_vr.T)/2
    upd_mn = dens.mn + np.dot(G, y-mu )
    return GaussianDensity(upd_mn,upd_vr), GaussianDensity(mu,S)
    
def update(flt, nxt_smt, nxt_prd, A):
    """Rauch-Tung-Striebel smoother update"""
    G = np.dot(flt.vr, la.solve(nxt_prd.vr,A,sym_pos=True,check_finite=False).T )
    smt_vr = flt.vr + np.dot(G, np.dot( nxt_smt.vr-nxt_prd.vr ,G.T) )
    smt_mn = flt.mn + np.dot(G, nxt_smt.mn-nxt_prd.mn )
    return GaussianDensity(smt_mn,smt_vr)
    
## Sensitivity equations
#
#def predict_paramderiv(dens, A, Q, deriv_dens, dA_dp, dQ_dp):
#    """Kalman filter prediction with parameter sensitivity"""
#    prd_mn = np.dot(A,dens.mn)
#    prd_vr = np.dot(A, np.dot(dens.vr,A.T) ) + Q
#    prd_vr = (prd_vr+prd_vr.T)/2
#    
#    Np = len(dA_dp)
#    assert(len(dQ_dp)==Np)
#    
#    dmn_dp = [np.dot(dA_dp[ii],dens.mn)+np.dot(A,deriv_dens[ii].mn) for ii in range(Np)]
#    dvr_dp = [ np.dot(dA_dp[ii],np.dot(dens.vr,A.T)) \
#              +np.dot(A,np.dot(dens.vr,dA_dp[ii].T)) \
#              +np.dot(A,np.dot(deriv_dens[ii].vr,A.T)) \
#              +dQ_dp[ii] for ii in range(Np)]
#
#    prd_deriv_dens = [GaussianDensity(dmn_dp[ii],dvr_dp[ii]) for ii in range(Np)]
#    
#    return GaussianDensity(prd_mn,prd_vr), prd_deriv_dens
#
#def update_paramderiv(dens, y, H, R, deriv_dens, dH_dp, dR_dp):
#    """Kalman filter update with parameter sensitivity"""
#    mu = np.dot(H,dens.mn)
#    S = R + np.dot(H, np.dot(dens.vr,H.T) )
#    S = (S+S.T)/2
#    G = np.dot(dens.vr, la.solve(S,H,check_finite=False).T )
#    I = np.identity(dens.dim)
#    upd_vr = np.dot( I-np.dot(G,H), dens.vr)
#    upd_vr = (upd_vr+upd_vr.T)/2
#    upd_mn = dens.mn + np.dot(G, y-mu )
#    
#    Np = len(dH_dp)
#    assert(len(dR_dp)==Np)
#    
#    domn_dp = [np.dot(dH_dp[ii],dens.mn)+np.dot(H,deriv_dens[ii].mn) for ii in range(Np)]
#    dovr_dp = [ np.dot(dH_dp[ii],np.dot(dens.vr,H.T)) \
#               +np.dot(H,np.dot(dens.vr,dH_dp[ii].T)) \
#               +np.dot(H,np.dot(deriv_dens[ii].vr,H.T)) \
#               +dR_dp[ii] for ii in range(Np)]
#    dG_dp = [ np.dot(deriv_dens[ii].vr, la.solve(S,H).T) \
#             +np.dot(dens.vr, la.solve(S,dH_dp[ii]).T) \
#             -np.dot( np.dot(dens.vr, la.solve(S,H).T) , la.solve(S,dovr_dp[ii]).T ) for ii in range(Np)]
#    dmn_dp = [deriv_dens[ii].mn + np.dot(dG_dp[ii],y-mu) - np.dot(G, domn_dp[ii]) for ii in range(Np)]
#    dvr_dp = [deriv_dens[ii].vr - np.dot(dG_dp[ii],np.dot(S,G.T)) \
#                                - np.dot(G,np.dot(S,dG_dp[ii].T)) \
#                                - np.dot(G,np.dot(dovr_dp[ii],G.T)) for ii in range(Np)]
#                            
#    upd_deriv_dens = [GaussianDensity(dmn_dp[ii],dvr_dp[ii]) for ii in range(Np)]
#    
#    Symu = la.solve(S,y-mu)
#    prtl_param_deriv = [- 0.5*np.trace(la.solve(S,dovr_dp[ii])) - np.dot((y-mu).T,la.solve(S,-domn_dp[ii])) + 0.5*np.dot(Symu.T,np.dot(dovr_dp[ii],Symu)) for ii in range(Np)]
#    
#    return GaussianDensity(upd_mn,upd_vr), GaussianDensity(mu,S), upd_deriv_dens,prtl_param_deriv


# FUNCTIONS FOR ANNEALED KALMAN FILTERING, ETC. FOR MODEL LEARNING
def annealed_predict(dens, A1, A2, Q1, Q2, anfact):
    d = Q1.shape[0]
    Q1 += 1E-8 * np.identity(d)
    Q2 += 1E-8 * np.identity(d)
    
    invQmix = (1-anfact)*la.inv(Q1) + anfact*la.inv(Q2)
    #C = (1-anfact)*la.solve(Q1,A1) + anfact*la.solve(Q2,A2)
    C = ( (1-anfact)*la.solve(Q1,A1) + anfact*la.solve(Q2,A2) ).T
    
    AQA1 = np.dot(A1.T, la.solve(Q1,A1,sym_pos=True,check_finite=False) )
    AQA2 = np.dot(A2.T, la.solve(Q2,A2,sym_pos=True,check_finite=False) )
    
    prec = la.inv(dens.vr, check_finite=False) + (1-anfact)*AQA1 + anfact*AQA2
    
    try:
        prd_vr = la.inv( invQmix - np.dot(C.T, la.solve(prec,C,sym_pos=True,check_finite=False) ), check_finite=False )
        prd_mn = np.dot(prd_vr, np.dot( C.T, la.solve(prec, la.solve(dens.vr,dens.mn,sym_pos=True,check_finite=False),sym_pos=True,check_finite=False) ))
    except:
        print(dens.vr)
        print(dens.vr-dens.vr.T)
        print(la.eig(dens.vr)[0])
        raise
        
    return GaussianDensity(prd_mn,prd_vr), prec
    
def annealed_update(dens, x, A1, A2, Q1, Q2, anfact):
    d = Q1.shape[0]
    Q1 += 1E-8 * np.identity(d)
    Q2 += 1E-8 * np.identity(d)
    
    #C = (1-anfact)*la.solve(Q1,A1) + anfact*la.solve(Q2,A2)
    C = ( (1-anfact)*la.solve(Q1,A1) + anfact*la.solve(Q2,A2) ).T
    
    AQA1 = np.dot(A1.T, la.solve(Q1,A1,sym_pos=True,check_finite=False) )
    AQA2 = np.dot(A2.T, la.solve(Q2,A2,sym_pos=True,check_finite=False) )
    
    prec = la.inv(dens.vr, check_finite=False) + (1-anfact)*AQA1 + anfact*AQA2
    
    upd_vr = la.inv(prec, check_finite=False)
    upd_mn = np.dot(upd_vr, (la.solve(dens.vr,dens.mn,sym_pos=True,check_finite=False)+np.dot(C,x)) )

    return GaussianDensity(upd_mn,upd_vr)
    
