# Standard modules
import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
import sys

# Add to path
sys.path.append( '../code/' )

# My modules
from mocap_linear_model import *
from mocap_model_mcmc import *
from kalman import GaussianDensity, Series

def calculate_rmse(test_section, gs, truth):
    mse = 0
    num_insants = 0
    for jj in range(4):
        for kk in test_section[jj]:
            mse += la.norm( truth[kk][3*jj:3*jj+3]-gs[kk].mn[3*jj:3*jj+3] )**2
            num_insants += 1
    rmse = np.sqrt(mse/num_insants)
    return rmse
    
def calculate_rmse2(test_section, est, truth):
    mse = 0
    num_insants = 0
    for jj in range(4):
        for kk in test_section[jj]:
            mse += la.norm( truth[kk][3*jj:3*jj+3]-est[kk,3*jj:3*jj+3] )**2
            num_insants += 1
    rmse = np.sqrt(mse/num_insants)
    return rmse
    
def mocap_msvd(init_markers, marker_present, prop_energy, num_it):
    markers = init_markers.copy()
    last_S = np.nan*np.ones(init_markers.shape[1])    
    
    for it in range(num_it):
        
        U,S,V = la.svd(markers)
        if np.allclose(S,last_S,1E-7):
            break

        # Choose number of singular values
        normS = S/np.sum(S)
        cumsumS = np.cumsum(normS)
        num_sing = np.where( cumsumS>prop_energy )[0][0]
        
        # Reduce components
        U = U[:,:num_sing]
        V = V[:num_sing,:]
        Smat = np.diag(S[:num_sing])
        
        # Reconstruct
        reconstructed = np.dot(U,np.dot(Smat,V))
        
        for kk in range(K):
            for jj in range(num_markers):
                if not marker_present[jj,kk]:
                    markers[kk,3*jj:3*jj+3] = reconstructed[kk,3*jj:3*jj+3]
                
        last_S = S
        
    return markers
    


# Close all windows
plt.close("all")

# Set random seed
np.random.seed(1)

# Import marker data
raw_marker_data = np.genfromtxt('downsampled_head_markers.csv', delimiter=',')
truth = np.genfromtxt('downsampled_head_markers_truth.csv', delimiter=',')
K,d = raw_marker_data.shape
num_markers = int(d/3)
markers = Series.new_sequence(d,K)
for kk in range(K):
    markers[kk] = raw_marker_data[kk]

# Knock out a section to test on
test_section = [np.arange(40,60), np.arange(90,110), np.arange(140,160), np.arange(190,210)]
    
# Missing observations
marker_present = np.ones((num_markers,K))
observ = []
H_series = []
do_series = []
H_basic = np.hstack(( np.identity(d), np.zeros((d,d)) ))
for kk in range(K):
    H_tmp = H_basic.copy()
    do_tmp = d
    y_tmp = markers[kk]
    for jj in reversed(range(num_markers)):
        kill = False
        if np.any(np.isnan( y_tmp[3*jj:3*jj+3] )):
            kill = True
        if kk in test_section[jj]:
            kill = True
        if kill:
            marker_present[jj,kk] = 0
            H_tmp = np.delete( H_tmp, range(3*jj,3*jj+3), axis=0 )
            y_tmp = np.delete( y_tmp, range(3*jj,3*jj+3) )
            do_tmp -= 3
    H_series.append(H_tmp)
    do_series.append(do_tmp)
    observ.append(y_tmp)


# Draw it
f = plt.figure()
Series.plot_sequence(markers, [0,1,2], f, 'k')
Series.plot_sequence(markers, [3,4,5], f, 'b')
Series.plot_sequence(markers, [6,7,8], f, 'r')
Series.plot_sequence(markers, [9,10,11], f, 'g')
f.savefig('mocap_data.pdf', bbox_inches='tight')



# Set up model
ds = 2*d
Z = np.zeros((d,d))
I = np.identity(d)
Rscale = 0.001
F = np.array(np.bmat([[I,I],[Z,I]]))

D = np.identity(d)
givens = []
#D = np.identity(1)
#givens = [0,0,0,0,0,0,0,0,0,0,0]

m1 = np.zeros(ds)
P1 = 1000*np.identity(ds)
first_state_prior = GaussianDensity(m1,P1)

init_model = MocapLinearModel(first_state_prior, F, D, givens, H_series, Rscale, do_series)

init_flt,init_prd,_ = init_model.kalman_filter(observ)
init_smt = init_model.rts_smoother(init_flt,init_prd)
init_rmse = calculate_rmse(test_section, init_smt, truth)

# Fill in gaps with linear interpolation
init_markers = markers.copy()
for kk in range(K):
    for jj in range(num_markers):
        if not marker_present[jj,kk]:
            init_markers[kk,3*jj:3*jj+3] = init_smt[kk].mn[3*jj:3*jj+3]
f = plt.figure()
Series.plot_sequence(init_markers, [0,1,2], f, 'k')
Series.plot_sequence(init_markers, [3,4,5], f, 'b')
Series.plot_sequence(init_markers, [6,7,8], f, 'r')
Series.plot_sequence(init_markers, [9,10,11], f, 'g')

#num_sing = 5
prop_energy = 0.95
num_it = 1000
msvd_markers = mocap_msvd(init_markers, marker_present, prop_energy, num_it)
msvd_rmse = calculate_rmse2(test_section, msvd_markers, truth)


f_kf1 = plt.figure()
for jj in range(num_markers):
    if jj==0:
        colour = 'c'
    elif jj==1:
        colour = 'b'
    elif jj==2:
        colour = 'r'
    elif jj==3:
        colour = 'g'
    Series.plot_sequence(truth, range(3*jj,3*jj+3), f_kf1, colour+'--')
    GaussianDensity.plot_sequence(init_smt, range(3*jj,3*jj+3), f_kf1, colour)
#f_kf1 = plt.figure()
#GaussianDensity.plot_sequence(init_flt, [0,1,2], f_kf1, 'k')
#GaussianDensity.plot_sequence(init_flt, [3,4,5], f_kf1, 'b')
#GaussianDensity.plot_sequence(init_flt, [6,7,8], f_kf1, 'r')
#GaussianDensity.plot_sequence(init_flt, [9,10,11], f_kf1, 'g')



### Model priors ###
A_prior_vr = 1
Q_prior_scale = 100
Q_prior_dof = d-1

### Algorithm parameters ###
giv_std = 0.05
pad_std = 0.01
rot_std = 0.002
adapt_batch_size = 10
num_iter = 1000
num_burn_in = int(num_iter/2)



# Create learner object
brain = MocapModelMCMC(init_model, observ,
                        A_prior_vr=A_prior_vr,
                        Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof,
                        giv_std=giv_std, pad_std=pad_std, rot_std=rot_std)

# MCMC it
for ii in range(num_iter):

    print("")
    print("Iteration number %u" % (ii))
    
    # MCMC proposal adaptation
    if (((ii%adapt_batch_size)==0) and (ii>0)):
        brain.adapt_std(adapt_batch_size)
    
    # MCMC moves
    brain.iterate_transition_noise_matrix(1)
    brain.iterate_observation_variance(1)
    brain.iterate_transition_noise_matrix_rank(1)
    brain.iterate_observation_variance(1)
    
    # Save current state of the chain
    brain.save_link()
    
    # Print current state
    print("")
#    print("Transition variance:")
#    print(brain.model.Q)
    print("Transition variance eigenvalues:")
    print( np.linalg.eigh(brain.model.Q)[0] )
    print("Rank")
    print(brain.model.rank)
#    print("Givens rotations")
#    print(brain.model.givens)
    print("Observation covariance scale")
    print(brain.model.Rscale)
    print("")
    print("Rotation proposal standard deviation:")
    print(brain.rot_std)
    print("Givens rotation proposal standard deviations:")
    print(brain.giv_std)
    print("Transition matrix padding standard deviation:")
    print(brain.pad_std)


figs = draw_chain_histogram(brain.chain, num_burn_in)
figs[0].savefig('mocap_Qhist.pdf', bbox_inches='tight')


    
Q_chain = [mm.Q for mm in brain.chain[num_burn_in:]]
#Q_mean = np.mean( np.array(Q_chain), axis=0 )
Q_mean = Q_chain[-1]
R_chain = [mm.Rscale for mm in brain.chain[num_burn_in:]]
Rscale_mean = np.mean( np.array(R_chain), axis=0 )
est_model = TestMocapLinearModel(first_state_prior, F, Q_mean, H_series, Rscale_mean, do_series)

final_flt,final_prd,_ = est_model.kalman_filter(observ)
final_smt = est_model.rts_smoother(final_flt,final_prd)

f_kf2 = plt.figure()
for jj in range(num_markers):
    if jj==0:
        colour = 'c'
    elif jj==1:
        colour = 'b'
    elif jj==2:
        colour = 'r'
    elif jj==3:
        colour = 'g'
    Series.plot_sequence(truth, range(3*jj,3*jj+3), f_kf2, colour+'--')
    GaussianDensity.plot_sequence(final_smt, range(3*jj,3*jj+3), f_kf2, colour)

#f_kf2 = plt.figure()
#GaussianDensity.plot_sequence(final_smt, [0,1,2], f_kf2, 'k')
#GaussianDensity.plot_sequence(final_smt, [3,4,5], f_kf2, 'b')
#GaussianDensity.plot_sequence(final_smt, [6,7,8], f_kf2, 'r')
#GaussianDensity.plot_sequence(final_smt, [9,10,11], f_kf2, 'g')

final_rmse = calculate_rmse(test_section, final_smt, truth)

print('Simple interpolation: {}'.format(init_rmse))
print('MSVD interpolation: {}'.format(msvd_rmse))
print('Trained interpolation: {}'.format(final_rmse))

f_miss = plt.figure()
for jj in [1]:#range(num_markers):
    colour='k'
#    if jj==0:
#        colour = 'c'
#    elif jj==1:
#        colour = 'b'
#    elif jj==2:
#        colour = 'r'
#    elif jj==3:
#        colour = 'g'
#    Series.plot_sequence(truth, range(3*jj,3*jj+3), f_miss, colour+'--')
#    GaussianDensity.plot_sequence(final_smt, range(3*jj,3*jj+3), f_miss, colour)
#    for dd in range(3):
#        ax = f_miss.axes[dd]
#        ax.plot(msvd_markers[:,3*jj+dd],colour+'-.')
#        ax.set_xlim([85,115])
    dd = 0
    Series.plot_sequence(truth, [3*jj+dd], f_miss, colour)
    GaussianDensity.plot_sequence(final_smt, [3*jj+dd], f_miss, colour)
    init_est = [gg.mn[3*jj+dd] for gg in init_smt]
    ax = f_miss.axes[dd]
    ax.plot(init_est,colour+'-.')
    ax.plot(msvd_markers[:,3*jj+dd],colour+'-.')
    ax.set_xlim([85,115])
    ax.set_ylim([-0.3,0.7])
    ax.set_xlabel('Time instant')
    ax.set_ylabel('x position')
f_miss.savefig('missing_marker.pdf', bbox_inches='tight')

