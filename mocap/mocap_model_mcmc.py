import matplotlib.pyplot as plt
import numpy as np
import linear_models_sampling as sysl
from scipy import linalg as la
from scipy import stats
from scipy.stats import multivariate_normal as mvn
import statsmodels.tsa.stattools as st
from mocap_linear_model import MocapLinearModel
from kalman import Series

class MocapModelMCMC:
    """Container Class for Linear-Gaussian Model Learning with MCMC"""

    def __init__(self, init_model_est, observ, A_prior_vr=None, Q_prior_scale=None, Q_prior_dof=None, giv_std=None, pad_std=None, rot_std=None):
        
        self.model = init_model_est.copy()
        self.observ = observ

        self.chain = []
        self.chain.append(self.model.copy())        
        
        self.A_prior_vr = A_prior_vr
        self.Q_prior_scale = Q_prior_scale # This is the scalar p such that the Q^{-1} is Wishart with mean nu*p*I (i.e. phi in the paper)
        self.Q_prior_dof = Q_prior_dof
            
        self.giv_std = giv_std
        self.pad_std = pad_std
        self.rot_std = rot_std
        
        self.F_acc = []
        self.pad_std_chain = []
        
        self.rot_acc = []
        self.rot_std_chain = []
        
        self.givens_acc = []
        self.giv_std_chain = []
        
        self.rank_acc = []
            
        
    def save_link(self):
        """Save the current state of the model as a link in the chain"""
        self.chain.append(self.model.copy())
        self.pad_std_chain.append(self.pad_std)
        self.rot_std_chain.append(self.rot_std)
        self.giv_std_chain.append(self.giv_std)
                
    def adapt_std(self, B):
        """Adapt proposal distribution parameters"""
        ar = np.mean(np.array(self.F_acc[-B:]))
        bn = len(self.F_acc)/B
        delta = min(0.1,1/np.sqrt(bn))
        pad_min = 1E-6
        if (ar<0.44):
            self.pad_std = np.maximum(pad_min, self.pad_std*np.exp(-delta))
        else:
            self.pad_std = np.maximum(pad_min, self.pad_std*np.exp(delta))
            
        ar = np.mean(np.array(self.rot_acc[-B:]))
        bn = len(self.rot_acc)/B
        delta = min(0.1,1/np.sqrt(bn))
        rot_min = 1E-4
        if (ar<0.44):
            self.rot_std = np.maximum(rot_min, self.rot_std*np.exp(-delta))
        else:
            self.rot_std = np.maximum(rot_min, self.rot_std*np.exp(delta))
        
        ar = np.mean(np.array(self.givens_acc[-B:]))
        bn = len(self.givens_acc)/B
        delta = min(0.1,1/np.sqrt(bn))
        if (ar<0.44):
            self.giv_std = self.giv_std*np.exp(-delta)
        else:
            self.giv_std = self.giv_std*np.exp(delta)
#        
#        for gg in range(len(self.model.givens)):
#            ar = np.mean(np.array(self.givens_acc[gg][-B:]))
#            bn = len(self.givens_acc[gg])/B
#            delta = min(0.1,1/np.sqrt(bn))
#            if (ar<0.44):
#                self.giv_std[gg] = self.giv_std[gg]*np.exp(-delta)
#            else:
#                self.giv_std[gg] = self.giv_std[gg]*np.exp(delta)
    
    ### MCMC moves ###
    
    def iterate_transition_noise_matrix(self, Mtimes):
        """Run Gibbs iterations on the transition noise matrix"""
        
        if ((self.Q_prior_dof==None) or (self.Q_prior_scale==None)):
            raise ValueError("A transition covariance matrix prior must be supplied in order to sample")
            
        for mm in range(Mtimes):

            # Joint move

            # Old likelihood
            flt,_,lhood = self.model.kalman_filter(self.observ)
            
            if self.model.rank < self.model.dp:
                
                # Make a proposal copy of the model
                ppsl_model = self.model.copy()
                
                # Sample a Cayley-distributed random matrix
                M = sysl.sample_cayley(ppsl_model.dp, self.rot_std)
                
                # Use it to rotate noise matrix
                ppsl_model.rotate_noise(M)
                
                # Likelihood
                new_flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
                
                # Accept probability
                ap = new_lhood-lhood
#                    print(ap)
                
                # Accept/Reject
                if np.log(np.random.random()) < ap:
                    self.model = ppsl_model
                    self.rot_acc.append(1)
                    flt = new_flt
                    lhood = new_lhood
                else:
                    self.rot_acc.append(0)
                
                # How many planar moves should we do?
                num_givens_moves = ppsl_model.dp
            
                # Keep track of givens acceptances
                giv_acc = np.zeros(num_givens_moves)
                
                # Planar moves
                for gg in range(num_givens_moves):
                    
                    # Make a proposal copy of the model
                    ppsl_model = self.model.copy()
                    
                    # Random plane
                    ii = np.random.random_integers(ppsl_model.dp)-1
                    jj = ii
                    while jj == ii:
                        jj = np.random.random_integers(ppsl_model.dp)-1
                        
                    # Random rotation
                    rot = np.random.normal(0,self.giv_std)
                    ppsl_model.planar_rotate_noise(ii, jj, rot)
                    
                    # Likelihood
                    new_flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
                
                    # Accept probability
                    ap = new_lhood-lhood
#                        print(ap)
                
                    # Accept/Reject
                    if np.log(np.random.random()) < ap:
                        self.model = ppsl_model
#                        self.givens_acc.append(1)
                        giv_acc[gg] = 1
                        flt = new_flt
                        lhood = new_lhood
                    else:
#                            self.givens_acc.append(0)
                        giv_acc[gg] = 0
                        
                self.givens_acc.append(np.mean(giv_acc))
                
            # Sample a new covariance matrix
            x = self.model.backward_simulation(flt)
            xcut = Series.new_sequence(self.model.dp,len(self.observ))
            for kk in range(len(self.observ)):
                xcut[kk] = x[kk][:self.model.dp]
            Fcut = np.identity(self.model.dp)
            self.model.D = sysl.sample_degenerate_transition_noise_matrix_conditional(xcut, Fcut, self.model.rank, self.model.U, self.Q_prior_dof, self.Q_prior_scale)

    def iterate_transition_noise_matrix_rank(self, Mtimes):
        """Run RJ-MCMC iterations to change the transition noise matrix rank"""
        
        if ((self.Q_prior_dof==None) or (self.Q_prior_scale==None)):
            raise ValueError("A transition covariance matrix prior must be supplied in order to sample")
            
        for mm in range(Mtimes):
            
            # Likelihood
            flt,_,old_lhood = self.model.kalman_filter(self.observ)
            
            # Copy model
            ppsl_model = self.model.copy()
            
            # Add or remove?
            if ppsl_model.dp==1:
                u = -1                          # 1D model. Cannot do anything
            elif ppsl_model.rank==1:
                u = 0
            elif ppsl_model.rank==ppsl_model.dp:
                u = 1
            else:
                u = int(np.round(np.random.random()))
            
            if u==0:
                
                # Increase rank
                dcf = ppsl_model.increase_rank(None, None, self.Q_prior_scale)
                
            elif u==1:
                # Decrease rank
                dcf,val,vec = ppsl_model.reduce_rank(self.Q_prior_scale)
            
            # Likelihood
            _,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            
            ap = (new_lhood-old_lhood) + dcf
            print(ap)
            
            # Accept/Reject
            if np.log(np.random.random()) < ap:
                self.model = ppsl_model
                self.rank_acc.append(1)
#                old_lhood = new_lhood
            else:
                self.rank_acc.append(0)
                
    def iterate_observation_variance(self, Mtimes):
        """Gibbs sample the scale on the idendity observation covariance"""
        
        x = self.model.sample_posterior(self.observ)
        Rscale = sysl.sample_observation_covariance_scale_conditional(x, self.observ, self.model.H, 0.0001, 0.0001*np.identity(1))
        self.model.Rscale = Rscale
        self.model.R_update()
                
            

            
### DISPLAY ###
def draw_chain_histogram(chain, burn_in):
    d1 = 12
    d2 = 18
    d = d2-d1    
    figs = []
    
    if len(chain)>burn_in:
                
        # Q
        f,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
        f.subplots_adjust(wspace=0.6, hspace=0.6)
        figs.append(f)
        for rr in range(d1,d2):
            for cc in range(d1,d2):
                ax[rr-d1,cc-d1].locator_params(axis='x',nbins=1)
                ax[rr-d1,cc-d1].locator_params(axis='y',nbins=2)
                ax[rr-d1,cc-d1].set_xlim((0,0.01))
                ax[rr-d1,cc-d1].set_ylim((0,0.5*len(chain[burn_in:])))
                ax[rr-d1,cc-d1].hist([mm.Q[rr,cc] for mm in chain[burn_in:]], color='0.5')
#                ax[rr,cc].set_xlim((-2,2))
                
        mode_rank = int(stats.mode([mm.rank for mm in chain[burn_in:]])[0][0])
            
        # rank
        f,ax = plt.subplots(1,1)
        figs.append(f)
        ax.locator_params(nbins=2)
        ax.set_ylim((0,len(chain[burn_in:])))
        rank_chain =np.array([mm.rank for mm in chain[burn_in:]])
        bins = np.arange(rank_chain.min()-0.5,rank_chain.max()+1.5)            
        ax.hist(rank_chain, color='0.5', bins=bins)
        
#        # D
#        d = min(3,true_model.rank)
#        f,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
#        f.subplots_adjust(wspace=0.3, hspace=0.3)
#        figs.append(f)
#        for rr in range(d):
#            for cc in range(d):
#                ax[rr,cc].locator_params(nbins=2)
#                ax[rr,cc].set_ylim((0,len(chain[burn_in:])))
#                ax[rr,cc].hist([mm.D[rr,cc] for mm in chain[burn_in:] if mm.rank==mode_rank], color='0.5')
#                ax[rr,cc].plot([true_model.D[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
#            
#        # givens
#        f,ax = plt.subplots(1,len(true_model.givens),squeeze=False,figsize=(9,5))
#        f.subplots_adjust(wspace=0.3, hspace=0.3)
#        figs.append(f)
#        for cc in range(len(true_model.givens)):
#            ax[0,cc].locator_params(nbins=2)
#            ax[0,cc].set_ylim((0,len(chain[burn_in:])))
#            ax[0,cc].hist([mm.givens[cc] for mm in chain[burn_in:] if mm.rank==mode_rank], color='0.5')
#            ax[0,cc].plot([true_model.givens[cc]]*2, [0,len(chain[burn_in:])], '-r')
##            ax[0,cc].set_xlim((-np.pi,np.pi))

    return figs


def draw_chain(chain, true_model, burn_in, fields):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        
        for ff in fields:
            if ff in ['F','Q','G','D']:
                d = getattr(true_model, ff).shape[0]
                d = min(4,d)
                fig,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
                fig.subplots_adjust(wspace=0.3, hspace=0.3)
                for rr in range(d):
                    for cc in range( getattr(chain[-1],ff).shape[1] ):
                        ax[rr,cc].locator_params(nbins=2)
                        ax[rr,cc].set_xlim((0,len(chain[1:])))
                        ax[rr,cc].plot([getattr(mm,ff)[rr,cc] for mm in chain[1:]], 'k')
                        ax[rr,cc].plot([1,len(chain)], [getattr(true_model,ff)[rr,cc]]*2, '-r')
            elif ff in ['givens']:
                d = len(getattr(true_model,ff))
                fig = plt.figure()
                for dd in range(d):
                    ax = fig.add_subplot(d,1,dd+1)
                    ax.plot([getattr(mm,ff)[dd] for mm in chain[1:]])
                    ax.plot([1,len(chain)], [getattr(true_model,ff)[dd]]*2, 'r')
                    
    return fig
    

def draw_chain_autocorrelation(chain, true_model, burn_in, fields, nlags=30):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        
        for ff in fields:
            if ff in ['F','Q','G','D']:
                d = getattr(true_model, ff).shape[0]
                d = min(4,d)
                fig,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
                fig.subplots_adjust(wspace=0.3, hspace=0.3)
                for rr in range(d):
                    for cc in range( getattr(chain[-1],ff).shape[1] ):
                        seq = [getattr(mm,ff)[rr,cc] for mm in chain[burn_in:]]
                        ac = st.acf(seq,unbiased=False,nlags=nlags,)
                        ax[rr,cc].locator_params(nbins=2)
                        ax[rr,cc].set_xlim((0,nlags))
                        ax[rr,cc].set_ylim((-0.1,1))
                        ax[rr,cc].plot(range(len(ac)),ac,'k')
                        ax[rr,cc].plot(range(len(ac)),[0]*len(ac),':k')
            elif ff in ['givens']:
                d = len(getattr(true_model,ff))
                fig = plt.figure()
                for dd in range(d):
                    ax = fig.add_subplot(d,1,dd+1)
                    seq = [getattr(mm,ff)[dd] for mm in chain[burn_in:]]
                    ac = st.acf(seq,unbiased=False,nlags=nlags,)
                    ax.plot(range(len(ac)),ac,'k')
                    ax.plot(range(len(ac)),[0]*len(ac),':k')
                    ax.set_ylim([-0.1,1])
                    
    return fig


def draw_chain_acceptance(chain_acc, burn_in):
    d = len(chain_acc)
    ax = plt.figure().add_subplot(1,1,1)
    maxval = 0
    for dd in range(d):
        acc_array = np.array(chain_acc[dd])
        ax.plot(np.cumsum(acc_array))
        maxval = max(maxval,acc_array.sum())
    ax.plot([burn_in]*2,[0,maxval])


def draw_chain_adaptation(chain_adpt, burn_in):
    d = len(chain_adpt)
    ax = plt.figure().add_subplot(1,1,1)
    maxval = 0
    for dd in range(d):
        adpt_array = np.array(chain_adpt[dd])
        ax.plot(adpt_array)
        maxval = max(maxval,adpt_array.max())
    ax.plot([burn_in]*2,[0,maxval])

     