# Standard modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as spio
from scipy import stats

# My modules
from changepoint_models import *
from changepoint_models_mcmc import *
from kalman import GaussianDensity, Series

# Close all windows
plt.close("all")

# Set random seed
np.random.seed(1)

# Some parameters
dwnsmp = 20
#H = np.identity(4)
#R = np.diag(np.asarray([1E-1,1E-1,1E-2,1E-2])**2)
H = np.array([[1,0,0,0],[0,1,0,0]])
R = np.diag(np.asarray([1E-1,1E-1])**2)

# Load data
vrs = spio.loadmat('TARGET6_legacy.mat')
time = vrs['xyz_targ'][9,::dwnsmp]
state = (vrs['xyz_targ'][(0,3,1,4),::dwnsmp].T)/1000

#time = time[:20]
#state = state[:20,:]
(K,d) = state.shape

observ = np.zeros((state.shape[0],2))
for kk in range(K):
    observ[kk] = stats.multivariate_normal.rvs(mean=np.dot(H,state[kk]),cov=R)
#observ = np.zeros(state.shape)
#(K,d) = state.shape
#for kk in range(K):
#    observ[kk] = stats.multivariate_normal.rvs(mean=state[kk],cov=R)

# Initial Prior
m0 = state[0]
P0 = 100*np.identity(d)
first_state_prior = GaussianDensity(m0, P0)

### Model priors ###
dt = 1#time[1]-time[0]
A_prior_mn = np.array([[1,0,dt,0],
                       [0,1,0,dt],
                       [0,0,1,0],
                       [0,0,0,1]])
A_prior_vr = (0.2**2)*np.identity(d)
Q_prior_dof = (d+6)
Q_prior_scale = (Q_prior_dof-d-1)*np.diag([1E-1,1E-1,1E-2,1E-2])**2
cp_prior_rate = 8./len(time)

### Algorithm parameters ###
num_iter = 2000
num_burn_in = int(num_iter/5)
warm_up = 10

### Initial Model Estimate ###
#cp_est = [25,75,150,225,275]
#F_est = [0.9*np.identity(d)]*6
#Q_est = [10*np.identity(d)]*6
#cp_est = [75,150,225]
#F_est = [0.9*np.identity(d)]*4
#Q_est = [10*np.identity(d)]*4
#cp_est = [50,250]
#F_est = [0.9*np.identity(d)]*3
#Q_est = [10*np.identity(d)]*3
#cp_est = [150]
#F_est = [0.9*np.identity(d)]*2
#Q_est = [10*np.identity(d)]*2

#cp_est = [30,70,100,120,150]
#F_est = [0.9*np.identity(d)]*6
#Q_est = [10*np.identity(d)]*6

cp_est = []
F_est = [0.9*np.identity(d)]
Q_est = [10*np.identity(d)]

###############################################################################

# Create model objects
#model = LinearChangepointModel(K, cp, first_state_prior, F, Q, H, R)
init_model_est = LinearChangepointModel(K, cp_est, first_state_prior, F_est, Q_est, H, R)

## Filter and sample
#flt,_,_ = model.kalman_filter(observ)
#smp = model.sample_posterior(observ)
#GaussianDensity.plot_sequence(flt, range(d), f, colour="g")
#Series.plot_sequence(smp, range(d), f, colour="c")
#plt.show()

# Create MCMC container object
brain = LinearChangepointModelMCMC(init_model_est, observ, 
             A_prior_mn, A_prior_vr, Q_prior_scale, Q_prior_dof, cp_prior_rate)

# Train the models for the initial guess
for ii in range(warm_up):
    brain.iterate_transition_model(1)

# MCMC loop
for ii in range(num_iter):

    print("")
    print("Iteration number %u" % (ii))

    # MCMC moves
    brain.iterate_transition_model(1)
    
#    brain.iterate_changepoints("add")
#    for nn in range(len(brain.model.point)):
#        brain.iterate_changepoints("shift", idx=nn)
#        brain.iterate_changepoints("adjust",idx=nn, half_width=3)
#    brain.iterate_changepoints("remove")
    
#    brain.iterate_changepoints_with_annealing("add", num_anneal_steps=0)
#    for nn in range(len(brain.model.point)):
#        brain.iterate_changepoints_with_annealing("shift", idx=nn, num_anneal_steps=0)
#        brain.iterate_changepoints_with_annealing("adjust",idx=nn, half_width=3, num_anneal_steps=0)
#    brain.iterate_changepoints_with_annealing("remove", num_anneal_steps=0)
    
    brain.iterate_changepoints_with_annealing("add", num_anneal_steps=30)
    for nn in range(len(brain.model.point)):
        brain.iterate_changepoints_with_annealing("shift", idx=nn, num_anneal_steps=3)
        brain.iterate_changepoints_with_annealing("adjust",idx=nn, half_width=3, num_anneal_steps=30)
    for nn in reversed(range(len(brain.model.point))):
        brain.iterate_changepoints_with_annealing("remove", idx=nn, num_anneal_steps=30)
    
#    brain.iterate_changepoints_and_parameters("add")
#    for nn in range(len(brain.model.point)):
#        brain.iterate_changepoints_and_parameters("shift", idx=nn)
#        brain.iterate_changepoints_and_parameters("adjust",idx=nn, half_width=3)
#    brain.iterate_changepoints_and_parameters("remove")
    
    # Save current state of the chain
    brain.save_link()
    
    # Print current progress
    print("Changepoint set:")
    print(brain.model.point)
    
#    print(brain.model.order)
#    print(len(brain.model.F))
#

f_data = plt.figure()
ax = f_data.add_subplot(1,1,1)
ax.plot(state[:,0],state[:,1])
ax.plot(observ[:,0],observ[:,1],'rx')
plt.show()


figs = draw_changepoint_occupation(brain.chain, init_model_est, num_burn_in)
f_chain = draw_chain_numcps(brain.chain, init_model_est, num_burn_in)

f_data.savefig('tracking_data.pdf', bbox_inches='tight')
figs[0].savefig('tracking_cp_occupation.pdf', bbox_inches='tight')
figs[1].savefig('tracking_cp_number.pdf', bbox_inches='tight')
f_chain.savefig('tracking_cp_number_chain.pdf', bbox_inches='tight')



#
## Plot state components
#f = plt.figure()
#Series.plot_sequence(state, range(d), f, 'k')
#
## Plot some MCMC output graphs
#draw_chain(brain.chain, model, num_burn_in, ['F'])
#draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['F'], nlags=10)
#draw_chain_histogram(brain.chain, model, num_burn_in)
#
