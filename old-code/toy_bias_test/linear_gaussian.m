% clup
dbstop if error

model.vr1 = 1;
model.vr2 = 1;
model.cvr = 0.9*sqrt(model.vr1*model.vr2);
model.mn = [3; -2];
model.vr = [model.vr1 model.cvr; model.cvr model.vr2];
model.x0 = [10; 0];

algo.B = 1000;
algo.N = 10000+algo.B;
algo.ppsl_vr = 0.1;

x_samples = zeros(2,algo.N);

x = model.x0;
chain_prob = loggausspdf(x, model.mn, model.vr);

for mm = 1:algo.N
    
    fprintf(1, 'Iteration %u.\n', mm);
    
    x_ppsl = zeros(2,1);
    forw_move_prob = 0;
    back_move_prob = 0;
    
    x_ppsl(1) = mvnrnd(x(1), algo.ppsl_vr);
    forw_move_prob = forw_move_prob + loggausspdf(x_ppsl(1), x(1), algo.ppsl_vr);
    back_move_prob = back_move_prob + loggausspdf(x(1), x_ppsl(1), algo.ppsl_vr);
    
    forw_ppsl_mn = model.mn(2) + model.cvr*(x_ppsl(1)-model.mn(1))/model.vr1;
    forw_ppsl_vr = model.vr2 - model.cvr^2/model.vr1;
    x_ppsl(2) = mvnrnd( forw_ppsl_mn, forw_ppsl_vr );
%     forw_move_prob = forw_move_prob + loggausspdf(x_ppsl(2), forw_ppsl_mn, forw_ppsl_vr);
    
    back_ppsl_mn = model.mn(2) + model.cvr*(x(1)-model.mn(1))/model.vr1;
    back_ppsl_vr = model.vr2 - model.cvr^2/model.vr1;
%     back_move_prob = back_move_prob + loggausspdf(x(2), back_ppsl_mn, back_ppsl_vr);
    
    ppsl_prob = loggausspdf(x_ppsl, model.mn, model.vr);
    
    ap = (ppsl_prob-chain_prob) - (forw_move_prob-back_move_prob);
    
    if log(rand) < ap
        
        x = x_ppsl;
        chain_prob = ppsl_prob;
        
    end
    
    x_samples(:,mm) = x;
    
end

%%
% close all

figure, hold on
plot(x_samples(1,:), x_samples(2,:), 'k')

dx = 0.1;
x1_range = model.mn(1)-4:dx:model.mn(1)+4;
x2_range = model.mn(2)-4:dx:model.mn(2)+4;
x1_hist = histc(x_samples(1,algo.B+1:end), x1_range);
x2_hist = histc(x_samples(2,algo.B+1:end), x2_range);

figure, hold on
bar(x1_range, x1_hist/(dx*(algo.N-algo.B)))
plot(x1_range, mvnpdf(x1_range', model.mn(1), model.vr1), 'r');
title('x1')

figure, hold on
bar(x2_range, x2_hist/(dx*(algo.N-algo.B)))
plot(x2_range, mvnpdf(x2_range', model.mn(2), model.vr2), 'r');
title('x2')