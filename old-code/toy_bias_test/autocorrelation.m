function [ ac ] = autocorrelation( samples, B, D )
%Calculate autocorrelation of Markov chain samples

% Scalar chains only here

% each column of samples is an element in the chain
% B is the burn in
% D is the maximum delay.

N = size(samples, 2);

delay = 0:D;
ac = zeros(size(delay));

for ii = 1:length(delay)
    
    dd = delay(ii);
    
    est_mn = sum(samples(B+1:end),2)/(N-B);
    est_vr = sum((samples(B+1:end)-est_mn).^2,2)/(N-B);
    ac(ii) = sum( (samples(B+dd+1:end)-est_mn).*(samples(B+1:end-dd)-est_mn) )/((N-dd-B)*est_vr);
        
end


end

