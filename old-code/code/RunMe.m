function RunMe( )
%RUNME Something to get started with

dbstop if error
data_seed = 0;
algo_seed = 0;

% addpath(fullfile(Paths.BASE, Paths.COMMON));
% addpath(fullfile(Paths.BASE, Paths.DATA_GENERATION));
% addpath(fullfile(Paths.BASE, Paths.GIBBS_RJMCMC));
addpath('common/')
addpath('Data Generation/')
addpath('Gibbs RJMCMC/')

% Make some data (2 variables, 2 transitions)
rng(data_seed)
data = MakeData( 100, 2, 0 );
% data = MakeData( 300, 10, 2 );
% data = MakeData3var0tx(300);
% data = MakeData2var2tx(300);
% data = MakeData3var2tx(300);
% data = MakeData2var2tx(300);

% Sample the system matrices, jump times, and Xs
NumSamples = 1000;
rng(algo_seed)
% [ ASamples, TauSamples ] = aisRJMCMC(data, NumSamples);
% [ ASamples, TauSamples ] = RJMCMC(data, NumSamples);
[ ASamples, BSamples ] = MCMC(data, NumSamples);


% To run GibbsTest (runs the Gibbs sampler on a single sequence), do the following:
% data = MakeData3var0tx(200);
% GibbsTest(data);

end

