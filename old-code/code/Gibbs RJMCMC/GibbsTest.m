function Asamples = GibbsTest( data )
%GIBBSTEST This is a test of the Gibbs sampler for the A matrix
%  It only works for a single transition matrix A
%  The data parameter is as follows:
%  data.A  - the A matrix (used for true values)
%  data.Cu - State transition covariance
%  data.Cv - Observation covariance
%  data.B  - Observation matrix 
%  data.y  - Observations generated from Y=BX+v, X_t=AX_{t-1}+u

NumSamples = 1000;
BURN_IN = ceil(0.2*NumSamples);

% How often to display in samples; use 0 for no display
DISPLAY_EVERY_N = 100;


N = size(data.y, 1);
T = size(data.y, 2);
Cu = data.Cu;
Atrue = data.A(:,:,1);

X0mu = zeros(N,1);
X0cov = 5*eye(N);


% Can initialize from a random sample
%Ainit = ones(N)-2*rand(N);
%Ainit = Atrue + randn(N)*0.3;

% Or initialize from the variational estimate
[ muA, SigmaA ] = VKFS(data.y, data.B, data.Cv, X0mu, X0cov, 1, 100, true );
Ainit = muA;

clf;
hold on;
figure(1);clf;hold on;
figure(2);clf;hold on;
figure(1);
plot(data.x', 'r-');

invCu = inv(Cu);

Asamples(:,:,1) = Ainit;
A = Asamples(:,:,1);



for s=2:NumSamples
    
    % Sample from the smoother distribution
    A = Asamples(:,:,s-1);
    X(:,:,s) = KFFBackwardSample( data.y, A, data.B, Cu, data.Cv, X0mu, X0cov, false );
    
    for i=1:N
        for j=1:N
            rsum = invCu(i,i) * sum(X(j,1:T-1,s).^2);
            
            for t=2:T
                ixxi = [1:i-1 i+1:N];
                ixxj = [1:j-1 j+1:N];
                
                AX = A*X(:,t-1,s);
                
                m1(t) = AX(ixxi)' * invCu(ixxi, i);
                m2(t) = invCu(i,i) * X(j,t-1,s) * A(i,ixxj)*X(ixxj,t-1,s);
                
                bi = invCu(i,:) * X(:,t,s);
                
                m4(t) = bi * X(j,t-1,s);
            end
            
            sigma2 = 1 / rsum;
            mu = - sigma2 * (sum(m1) + sum(m2) - sum(m4));
            
            A(i,j) = randn * sqrt(sigma2) + mu;
            
        end
    end
    
    Asamples(:,:,s) = A;
    
    if(DISPLAY_EVERY_N > 0 && mod(s,DISPLAY_EVERY_N)==0)
        figure(1);
        plot(X(:,:,s)');
        figure(2);
        
        if(s>BURN_IN), retained = s-BURN_IN;
        else retained = s;
        end
        
        for i=1:N
            for j=1:N
                h = subplot(N,N,(i-1)*N+j);
                cla;
                hold on;
                binwidth = 0.02;
                [n, xout] = hist(squeeze(Asamples(i,j,max(1,end-retained):end)), Atrue(i,j)-0.3:binwidth:Atrue(i,j)+0.3);
                bar(xout, n/(binwidth*retained));
                plot([Atrue(i,j) Atrue(i,j)], [0 max(n/(binwidth*retained))], 'r-');
                axis(h, [Atrue(i,j)-0.3 Atrue(i,j)+0.3 0 max(n/(binwidth*retained))]);
                xs = Atrue(i,j)-0.3:0.01:Atrue(i,j)+0.3;
                plot(xs, normpdf(xs, muA(i,j), sqrt(SigmaA(i,i))), 'g-');
            end
        end
        suptitle('A elements (red = true, green = variational)');
    end
end

figure(1);
plot(data.x', 'r-');



end

