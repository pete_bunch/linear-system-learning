function [ output_args ] = TestVKFSEstimate( input_args )
%TESTVKFSESTIMATE Summary of this function goes here
%   Detailed explanation goes here

T = 250;

Tau = [60 120 180 260];
A(:,:,1) = [0.8 0 0;   0 0.9  0; 0.6 0   0.9]; 
A(:,:,2) = [0.9 0 0.1; 0 0.9  0; 0   0.6 0.9];
A(:,:,3) = [0.9 0 0.1; 0 0.9  0; 0.3 0   0.7];
A(:,:,4) = zeros(3);


data = GenerateData(A,Tau,T);


X0mu = zeros(size(data.x,1),1);
X0cov = 5*eye(size(data.x,1));

VKFS(data.y(:,61:120), data.B, data.Cv, X0mu, X0cov, 1, 100, true);
%VKFS(y,             B,      Cv,     X0mu, X0cov, alpha, MaxIterations, DoVis )
end

