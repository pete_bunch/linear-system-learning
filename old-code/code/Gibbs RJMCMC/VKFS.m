function [ muA SigmaA ] = VKFS(y, B, Cv, X0mu, X0cov, alpha, MaxIterations, DoVis )
%VKFS Summary of this function goes here
%   Detailed explanation goes here

N = size(y,1);
T = size(y,2);
k = N;

% For notational consistency with text
C = B;
R = Cv;

% Initialization

CTRC = C' * R \ C;
CTR  = C' / R;

EA = zeros(N);
EATA = k*alpha*eye(N);


if(DoVis)
    clf;
    hold on;
end

muAold = Inf*ones(N);
i=0;
convergence = false;
while(~convergence)

    % VBE step
    % Forward pass
    
    mu = zeros(N,T+1);
    InvSigma = zeros(N,N,T+1);
    InvSigmaStar = zeros(N,N,T);
    mu(:,1) = X0mu;
    InvSigma(:,:,1) = inv(X0cov);
    
    for t=2:T+1
    
        InvSigmaStar(:,:,t-1) = InvSigma(:,:,t-1) + EATA;
        InvSigma(:,:,t) = eye(N) + CTRC - EA/InvSigmaStar(:,:,t-1)*EA';
        mu(:,t)      = InvSigma(:,:,t) \ ( CTR*y(:,t-1) + EA/InvSigmaStar(:,:,t-1)*InvSigma(:,:,t-1)*mu(:,t-1) );
        
    end
    
    % Backward pass
    eta = zeros(N,T+1);
    InvPhi = zeros(N,N,T+1);
    InvPhiStar = zeros(N,N,T+1);
    
    for t=T+1:-1:2
    
        InvPhiStar(:,:,t) = eye(N) + CTRC + InvPhi(:,:,t);
        InvPhi(:,:,t-1)   = EATA - (EA'/InvPhiStar(:,:,t))*EA;
        eta(:,t-1) = InvPhi(:,:,t-1) \ ( EA'/InvPhiStar(:,:,t)*(CTR*y(:,t-1) + InvPhi(:,:,t)*eta(:,t)));
        
    end
    
    % Combination + sufficient statistics
    Ytt = zeros(N,N,T+1);
    Yttp1  = zeros(N,N,T+1);
    w      = zeros(N,T+1);
    WA = zeros(N);
    SA = zeros(N);
    for t=1:T+1
    
        Ytt(:,:,t) = inv(InvSigma(:,:,t) + InvPhi(:,:,t));
        w(:,t) = Ytt(:,:,t) * (InvSigma(:,:,t)*mu(:,t) + InvPhi(:,:,t)*eta(:,t));
        if(t<T+1), Yttp1(:,:,t) = (InvSigmaStar(:,:,t)\EA')/(eye(N) + CTRC + InvPhi(:,:,t+1) - EA/InvSigmaStar(:,:,t)*EA'); end
    
        if(t>=2)
            WA = WA + Ytt(:,:,t-1) + w(:,t-1)*w(:,t-1)'; 
            SA = SA + Yttp1(:,:,t-1) + w(:,t-1)*w(:,t)'; 
        end
    end
    
    if(DoVis)
        for j=1:N
            subplot(ceil(sqrt(N)), ceil(sqrt(N)),j);
            cla;
            hold on;
            plot(w(j,1:T+1)','b-');
            plot(w(j,1:T+1)'+3*squeeze(sqrt(Ytt(j,j,1:T+1))),'b--');
            plot(w(j,1:T+1)'-3*squeeze(sqrt(Ytt(j,j,1:T+1))),'b--');
            plot(2:T+1,y(j,1:T)','r-');

            drawnow
        end
    end
    
    
    % VBM step
    InvSigmaA = alpha*eye(N) + WA;
    EA = SA / InvSigmaA;
    
    SigmaA = inv(InvSigmaA);
    EATA = EA'*EA + k*SigmaA;

    for j=1:N
        muA(:,j) = InvSigmaA\SA(:,j);
    end
    
    if(DoVis)
        fprintf('\nstep %d:\n',i);
        for k=1:N
            fprintf('[  ');
            for j=1:N
                fprintf('%1.3f +/- %1.3f,  ', muA(k,j), 3*sqrt(SigmaA(k,k)));
            end
            fprintf('  ]\n');
        end
    end
    
    i=i+1;
    
    % Convergence test
    if(sum(sum(abs(muA-muAold)))<1e-6 || (i>0 && i>MaxIterations))
        convergence = true;
    end
    muAold = muA;
    
end

muA = muA';


end

