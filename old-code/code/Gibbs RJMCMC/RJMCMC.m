function [ ASamples, TauSamples ] = RJMCMC( data, NumSamples )
%RJMCMC Reversible Jump MCMC sampler for transition times and system
%matrices

% How many samples to display in the visualization
DISPLAY_LATEST = 8000;

% Visualization every n frames (use 0 for no vis)
VIS_EVERY = 50;

% Parameters for transition process
TauMu     = 100;
TauLambda = 1/TauMu;

% Birth and death PROPOSAL probabilities
qBirthAIM = 0.3;
qDeathAIM = 0.1;

SampleTau = true;

NumIntGibbs = 10;

N = size(data.y, 1);
T = size(data.y, 2);
Cu = data.Cu;

% Initial priors
X0mu = zeros(N,1);
X0cov = 5*eye(N);

mus(:,1) = X0mu;
Sigmas(:,:,1) = X0cov;

Atrue = data.A;
Tautrue = data.Tau;


ASamples = {};
TauSamples = {};


% Initial samples for Tau and A
Tau = [];%data.Tau;
% Tau = [0 0];
% while any(diff(Tau)==0)
%     Tau = sort(unidrnd(T,[1,6]),'ascend');
% end

A = randn(size(data.A,1),size(data.A,2),numel(Tau)+1)*0.5;

% Some precalculation
invCu = inv(Cu);
      
% Some initial Gibbs Sampling
ExtendedTau = [0 Tau T];
for mm = 1:20
    X = KFFBackwardSampleSequence( data.y, A, Tau, data.B, data.Cu, data.Cv, X0mu, X0cov );
    for kk = 1:numel(Tau)+1
        A(:,:,kk) = GibbsTransitionMatrix(X(:,ExtendedTau(kk)+1:ExtendedTau(kk+1)), invCu);
    end
    [~,~,logpCur]  = KFSequence(data.y,A,[0 Tau T],data.B,data.Cu,data.Cv,X0mu,X0cov);
end

for s=2:NumSamples
    
    % Sample from the smoother distribution over the entire time range
    X = KFFBackwardSampleSequence( data.y, A, Tau, data.B, data.Cu, data.Cv, X0mu, X0cov );
    
    % Sampling the A matrices given the Tau sequence
    for section = 1:numel(Tau)+1 
    
        % Calculate the start and end times of this section
        % Note that the start time is the time of the priors, the first
        % observation in this section is at time tstart+1
        
        if(section==1), tstart = 1;
        else tstart = Tau(section-1);
        end
        
        if(section == numel(Tau)+1), tend = T;
        else tend = Tau(section);
        end
        
        % This is the Gibbs sampler for the matrix AA
        A(:,:,section) = GibbsTransitionMatrix(X(:,tstart:tend), invCu);
    
    end
    
    % Sample the Tau sequence
    % doing this using Metropolis-within-Gibbs sampling
    if(SampleTau)
        ExtendedTau = [0 Tau T];

        % Make a proposal -- no point proposing death/move when don't have
        % transitions, though
        if(numel(Tau)==0), qBirth=1; qDeath=0;
%         elseif s<100, qBirth = qBirthAIM; qDeath = 0;
        else qBirth = qBirthAIM; qDeath = qDeathAIM;
        end
        
        nochange = false;
        u = rand;
        if(u<qDeath) %death
            fprintf('d');
            if(numel(Tau)>0)
                % choose a jump to kill
                i = ceil(rand * numel(Tau));
                ixxi = [1:i-1 i+1:numel(Tau)];
                TauProp = Tau(ixxi);
                
                % Set initial A matrix
%                 A_ppsl = A(:,:,i);
                A_ppsl = zeros(size(A(:,:,i)));
                Aprop   = cat(3, A(:,:,1:i-1), A_ppsl, A(:,:,i+2:end));
                
                % Gibbs sample to a good choice
                for mm = 1:NumIntGibbs
                    
                    % Sample from the smoother distribution over the entire time range
                    X = KFFBackwardSampleSequence( data.y, Aprop, TauProp, data.B, data.Cu, data.Cv, X0mu, X0cov );
                    
                    % Sample a new A matrix for each of the new sections
                    A_ppsl = GibbsTransitionMatrix(X(:,ExtendedTau(i)+1:ExtendedTau(i+2)), invCu);
                    Aprop   = cat(3, A(:,:,1:i-1), A_ppsl, A(:,:,i+2:end));
                    
                end
                
                logqProp = log(qDeath) + log(1/numel(Tau));
                
                intervalwidth = ExtendedTau(i+2)-ExtendedTau(i)-4;
                logqOld  = log(1/numel(Tau)) + log(1/intervalwidth);
                if(numel(Tau)==1), logqOld = logqOld + log(1);  % use the correct value if there is only 1 transition
                else logqOld = logqOld + log(qBirth);
                end
                
            else
                nochange = true;
            end
        elseif(u<qDeath+qBirth) %birth
            fprintf('b');
            % choose a jump to have a birth after (can be between 0 and numel(Tau))
            i = floor(rand * (numel(Tau)+1));
            % propose with a bit of a gap from the existing jumps
            if(i==0), tstart = 2;  
            else tstart = Tau(i)+2;
            end

            if(i==numel(Tau)), tend = T-2;
            else tend = Tau(i+1)-2;
            end

            if(tend-tstart>0)
                tnew = floor(rand * (tend-tstart+1)) + tstart;
                TauProp = [Tau(1:i) tnew Tau(i+1:end)];
                
                ExtendedTauProp = [0 TauProp T];
                
                % Set initial A matrix
%                 A1_ppsl = PriorSampleTransitionMatrix(1, size(A,1));%A(:,:,i+1);
%                 A2_ppsl = PriorSampleTransitionMatrix(1, size(A,1));
                A1_ppsl = zeros(size(A,1));
                A2_ppsl = zeros(size(A,1));
                Aprop   = cat(3, A(:,:,1:i), A1_ppsl, A2_ppsl, A(:,:,i+2:end));
                
                % Gibbs sample to a good choice
                for mm = 1:NumIntGibbs
                    
                    % Sample from the smoother distribution over the entire time range
                    X = KFFBackwardSampleSequence( data.y, Aprop, TauProp, data.B, data.Cu, data.Cv, X0mu, X0cov );
                    
                    % Sample a new A matrix for each of the new sections
                    A1_ppsl = GibbsTransitionMatrix(X(:,ExtendedTauProp(i+1)+1:tnew), invCu);
                    A2_ppsl = GibbsTransitionMatrix(X(:,tnew+1:ExtendedTauProp(i+3)), invCu);
                    Aprop   = cat(3, A(:,:,1:i), A1_ppsl, A2_ppsl, A(:,:,i+2:end));
                    
                end
                
                intervalwidth = tend-tstart;
                logqProp = log(qBirth) + log(1/(numel(Tau)+1)) + log(1/intervalwidth);
                logqOld  = log(qDeathAIM) + log(1/(numel(Tau)+1));
                
            else
                nochange = true;
            end

        else %move
            fprintf('m');
            if(numel(Tau)>0)
                % choose a jump time to move
                i = ceil(rand * numel(Tau));

                tstart = ExtendedTau(i);
                tend   = ExtendedTau(i+2);
                
                which_window = rand;
                if which_window < (1/4)
                    window_width = 10;
                elseif which_window < (2/4)
                    window_width = 3;
                else
                    window_width = 1;
                end

                if(tend-tstart>0)
                    tmin = max(tstart+1, Tau(i)-window_width);
                    tmax = min(tend-1, Tau(i)+window_width);

                    tnew = floor(rand * (tmax-tmin+1)) + tmin;
                    TauProp = [Tau(1:i-1) tnew Tau(i+1:end)];
%                     Aprop = A;

                    % Sample from the smoother distribution over the entire time range
                    Aprop = A;
                    Aprop(:,:,i+1) = zeros(size(A(:,:,i)));
                    Aprop(:,:,i) = zeros(size(A(:,:,i)));
                    for mm = 1:NumIntGibbs
                        X = KFFBackwardSampleSequence( data.y, Aprop, TauProp, data.B, data.Cu, data.Cv, X0mu, X0cov );
                        Aprop(:,:,i+1) = GibbsTransitionMatrix(X(:,tnew:ExtendedTau(i+2)), invCu);
                        Aprop(:,:,i) = GibbsTransitionMatrix(X(:,max(1,ExtendedTau(i)):tnew), invCu);
                    end
                    
                    % log q(Tau'|Tau)
                    logqProp = log(1-qDeath-qBirth) + log(1/(tmax-tmin));

                    % log q(Tau|Tau')
                    intervalwidth = min(tend-1, tnew+window_width) - max(tstart+1, tnew-window_width);
                    logqOld  = log(1-qDeath-qBirth) + log(1/intervalwidth);
                else
                    nochange = true;
                end
            else
                nochange = true;
            end
        end

        if(~nochange)
            PropIntervals = [TauProp T] - [0 TauProp];
            kProp = numel(TauProp);
            kOld  = numel(Tau);
            
            % Calculate the prior for the Tau sequences (Exponential prior)
            % Might want to consider a prior here that stops transitions
            % being too close together
            logpTauProp = kProp*log(TauLambda) - TauLambda * T;   %sum(PropIntervals(1:end-1)) + log(exp(-TauLambda*PropIntervals(end)));
            logpTauOld  = kOld*log(TauLambda)  - TauLambda * T;   %sum(OldIntervals(1:end-1))  + log(exp(-TauLambda*OldIntervals(end)));
            
            logAProp = 0;
            for kk = 1:size(Aprop,3)
                [~, tmp] = PriorSampleTransitionMatrix(1, size(Aprop,1), squeeze(Aprop(:,:,kk)));
                logAProp = logAProp + tmp;
            end
            logAOld = 0;
            for kk = 1:size(A,3)
                [~, tmp] = PriorSampleTransitionMatrix(1, size(A,1), squeeze(A(:,:,kk)));
                logAOld = logAOld + tmp;
            end
            
            % Do not accept proposals with two jumps at the same time, jumps at 1 or T, or
            % any NaNs in A (ideally they wouldn't be getting there...)
            if( (numel(TauProp)>0 && (min(TauProp)<2 || max(TauProp)>=T)) || min(PropIntervals)==0 || any(isnan(Aprop(:))))
                logpProp = -inf;
                logpTauProp = -inf;
            else
                % need to run Kalman filter to calculate likelihood
                [~,~,logpProp] = KFSequence(data.y,Aprop,[0 TauProp T],data.B,data.Cu,data.Cv,X0mu,X0cov);
            end
            
            % Acceptance probability
            logpAccept =   logpTauProp + logAProp + logpProp + logqOld...
                         - logpTauOld  - logAOld  - logpCur  - logqProp  ;
            %fprintf('d logpTau = %5.3f,  d logp = %5.3f,  d logq = %5.3f\n', logpTauProp-logpTauOld, logpProp-logpCur, logqOld-logqProp);

            if(rand < exp(logpAccept))
                A = Aprop;
                Tau = TauProp;
                fprintf('+');  % indicate acceptance
                logpCur = logpProp; % store likelihood
            else
                fprintf('-');  % indicate rejection
            end
        else
            fprintf('=');      % indicate no change (kernel not applied) 
        end
        
    end
    
    if(mod(s,50)==0)
        fprintf(' %d\n', s);
    end
    
    
    % ---- Visualization --------------------------------------------------
    if(mod(s,VIS_EVERY)==0)
        
        retained = min(DISPLAY_LATEST, s-1);
        
        % Transition Times
        figure(1); clf;
        subplot(2,1,1)
        TauSamplesY = TauSamples(end-retained+1:end);
        hist(squeeze([TauSamplesY{:}]), 1:T);
        title('Transition Times');
        subplot(2,1,2)
        hist(NumTransitions(end-retained+1:end),0:max(NumTransitions(end-retained+1:end)));
        title('Number of Transitions');
        
        % System matrices
        figure(2); 
        clf;
        hold on;
        for t=1:T
            for i=s-retained+1:s-1
                TauExtend = [0 TauSamples{i} T];
                tindex(t,i) = find(t>TauExtend, 1, 'last');
            end
        end
        
        Ng = min(10,N);
        for m=1:Ng
            for n=1:Ng
                for t=1:T
                    for i=s-retained+1:s-1
                        seq(i-s+retained,t) = ASamples{i}(m,n,tindex(t,i));
                    end
                end
                subplot(Ng,Ng,(m-1)*Ng+n);
                cla; hold on;
                visFillBetweenLines(1:T, quantile(seq,0.01), quantile(seq,0.99), [0.9 0.9 1], false);
                visFillBetweenLines(1:T, quantile(seq,0.1), quantile(seq,0.9), [0.7 0.7 1], false);
                visFillBetweenLines(1:T, quantile(seq,0.25), quantile(seq,0.75), [0.3 0.3 1], false);
                visFillBetweenLines(1:T, quantile(seq,0.375), quantile(seq,0.625), [0.2 0.2 1], false);
                plot(1:T, median(seq), 'b-');
                plot(1:T, squeeze(data.thetasequence(m,n,1:T)), 'r-');
                ylim([min(data.A(m,n,:))-0.3 max(data.A(m,n,:))+0.3]);
%                 suptitle('A elements')
            end
        end

        drawnow;
    end
    
    ASamples{s} = A;
    TauSamples{s} = Tau;
    NumTransitions(s) = numel(Tau);
end




end



%%%% Cut visualization (was slow)
%         for m=1:N
%             for n=1:N
%                 figure(2);
%                 subplot(N,N,(m-1)*N+n);
%                 cla; hold on;
%                 for t=1:T
%                     for i=s-retained+1:s-1
%                         TauExtend = [0 TauSamples{i} T];
%                         tindex = find(t>TauExtend, 1, 'last');
%                         pvals(i-s+retained) = ASamples{i}(m,n,tindex);
%                         seq(i-s+retained,t) = ASamples{i}(m,n,tindex(t,i));
%                     end
%                     weights = ones(size(pvals))/numel(pvals);
%                     [f,xi] = ksdensity(pvals, 'weights', weights, 'width', 0.01);
%                     visColorDensityBar(xi,f/max(f),t-0.5,1);
%                     colormap(flipud(bone))
%                 end
%                 plot3(1:T, squeeze(data.thetasequence(m,n,1:T)), 1.1*ones(size(1:T)), 'r-');
%                 ylim([min(data.A(m,n,:))-0.3 max(data.A(m,n,:))+0.3]);



