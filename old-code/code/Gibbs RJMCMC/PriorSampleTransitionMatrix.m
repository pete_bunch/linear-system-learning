function [ A, prob ] = PriorSampleTransitionMatrix( ppsl_vr, d, A )
%PRIORSAMPLETRANSITIONMATRIX Sample a transition matrix from an
%uninformative prior

Amn = zeros(d^2,1);
Avr = ppsl_vr*eye(d^2);

if nargin < 3
    Avec = mvnrnd(Amn', Avr);
    A = reshape( Avec, d, d)';
else
    Avec = A';
    Avec = Avec(:);
end

if nargout > 1
    prob = logmvnpdf(Avec', Amn', Avr);
end

end

