function [ A ] = GibbsSparseTransitionMatrix( X, B, invCu )
%GIBBSTRANSITIONMATRIX Run a Gibbs step to sample the transition matrix

prior_vr = 1;

[d,N] = size(X);

XCX = zeros(d^2,d^2);
XCx = zeros(d^2,1);
for t=2:N
    tmp = cell(d,1);
    for ii = 1:d
        tmp{ii} = (B(ii,:)'.*X(:,t-1))';
    end
    Xmat = blkdiag(tmp{:});
    XCX = XCX + Xmat'*invCu*Xmat;
    XCx = XCx + Xmat'*invCu*X(:,t);
end
Avr = inv(prior_vr*eye(d^2) + XCX);
Avr = (Avr+Avr')/2;
Amn = Avr*XCx;
Avec = mvnrnd(Amn', Avr);
A = reshape( Avec, d, d)';


end

