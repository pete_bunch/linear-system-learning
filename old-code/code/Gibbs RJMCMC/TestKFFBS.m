function TestKFFBS(  )
%TESTKFFBS Summary of this function goes here
%   Detailed explanation goes here

Taus = [100];
A(:,:,1) = 0.1*eye(2) + [0 0.99; 0 0.7]; 
A(:,:,2) = [0.99 0.1; 0 0.99];


data = GenerateData(A, Taus, 250);

X0mu = zeros(2,1);
X0cov = 5*eye(2);

hold on;
for i=1:100
    X = KFFBackwardSampleSequence( data.y, data.A, data.Tau, data.B, data.Cu, data.Cv, X0mu, X0cov );
    plot(X');
end
plot(data.x', 'r-');

title('The red lines should be largely within the area of the other coloured lines');

end

