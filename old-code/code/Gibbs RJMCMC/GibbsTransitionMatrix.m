function [ AA, prob ] = GibbsTransitionMatrix( X, invCu, AA )
%GIBBSTRANSITIONMATRIX Run a Gibbs step to sample the transition matrix

prior_vr = 1;

[d,N] = size(X);
XCX = zeros(d^2,d^2);
XCx = zeros(d^2,1);
for t=2:N
    tmp = repmat({X(:,t-1)'},d,1);
    Xmat = blkdiag(tmp{:});
    XCX = XCX + Xmat'*invCu*Xmat;
    XCx = XCx + Xmat'*invCu*X(:,t);
end
Avr = inv(prior_vr*eye(d^2) + XCX);
Avr = (Avr+Avr')/2;
Amn = Avr*XCx;

if nargin < 3
    Avec = mvnrnd(Amn', Avr);
    AA = reshape( Avec, d, d)';
else
    Avec = AA';
    Avec = Avec(:);
end

if nargout > 1
    prob = logmvnpdf(Avec', Amn', Avr);
end


end

