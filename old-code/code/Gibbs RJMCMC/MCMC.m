function [ ASamples, BSamples ] = MCMC( data, NumSamples )
%MCMC MCMC sampler for sparse system matrices

% How many samples to display in the visualization
DISPLAY_LATEST = 300;

% Visualization every n frames (use 0 for no vis)
VIS_EVERY = 50;

N = size(data.y, 1);
T = size(data.y, 2);
Cu = data.Cu;

% Initial priors
X0mu = zeros(N,1);
X0cov = 5*eye(N);

mus(:,1) = X0mu;
Sigmas(:,:,1) = X0cov;

ASamples = {};


A = PriorSampleTransitionMatrix(1,size(data.A,1));
B = zeros(size(A,1));

% Some precalculation
invCu = inv(Cu);
      
% Gibbs Sampling
for mm = 1:NumSamples
    for aa = 1:3
        X = KFFBackwardSampleSequence( data.y, A.*B, [], data.B, data.Cu, data.Cv, X0mu, X0cov );
        A = GibbsSparseTransitionMatrix(X, B, invCu);
    end
    B = GibbsSparseIndicators(X, A, B, invCu);
    fprintf('.');
    
    if(mod(mm,50)==0)
        fprintf(' %d\n', mm);
    end


    
    
    % ---- Visualization --------------------------------------------------
    if(mod(mm,VIS_EVERY)==0)
        
        retained = min(DISPLAY_LATEST, mm-1);
        
        % System matrices
        figure(1); 
        clf;
        hold on;

        for t=1:T
            for i=mm-retained+1:mm-1
                TauExtend = [0 T];
                tindex(t,i) = find(t>TauExtend, 1, 'last');
            end
        end
        
        Ng = min(10,N);
        for m=1:Ng
            for n=1:Ng
                for t=1:T
                    for i=mm-retained+1:mm-1
                        if BSamples{i}(m,n,tindex(t,i))
                            seq(i-mm+retained,t) = ASamples{i}(m,n,tindex(t,i));
                        end
                    end
                end
                subplot(Ng,Ng,(m-1)*Ng+n);
                cla; hold on;
                visFillBetweenLines(1:T, quantile(seq,0.01), quantile(seq,0.99), [0.9 0.9 1], false);
                visFillBetweenLines(1:T, quantile(seq,0.1), quantile(seq,0.9), [0.7 0.7 1], false);
                visFillBetweenLines(1:T, quantile(seq,0.25), quantile(seq,0.75), [0.3 0.3 1], false);
                visFillBetweenLines(1:T, quantile(seq,0.375), quantile(seq,0.625), [0.2 0.2 1], false);
                plot(1:T, median(seq), 'b-');
                plot(1:T, squeeze(data.thetasequence(m,n,1:T)), 'r-');
                ylim([min(data.A(m,n,:))-0.3 max(data.A(m,n,:))+0.3]);
%                 suptitle('A elements')
            end
        end
        
        % Indicator matrixes
        figure(2)
        clf; hold on
        for m = 1:Ng
            for n = 1:Ng
                for i=mm-retained+1:mm-1
                    seqB(i-mm+retained) = BSamples{i}(m,n);
                end
                subplot(Ng,Ng,(m-1)*Ng+n);
                cla; hold on;
                hist(seqB);
                xlim([0 1])
            end
        end
                

        drawnow;
    end
    
    ASamples{mm} = A;
    BSamples{mm} = B;
end




end



%%%% Cut visualization (was slow)
%         for m=1:N
%             for n=1:N
%                 figure(2);
%                 subplot(N,N,(m-1)*N+n);
%                 cla; hold on;
%                 for t=1:T
%                     for i=s-retained+1:s-1
%                         TauExtend = [0 TauSamples{i} T];
%                         tindex = find(t>TauExtend, 1, 'last');
%                         pvals(i-s+retained) = ASamples{i}(m,n,tindex);
%                         seq(i-s+retained,t) = ASamples{i}(m,n,tindex(t,i));
%                     end
%                     weights = ones(size(pvals))/numel(pvals);
%                     [f,xi] = ksdensity(pvals, 'weights', weights, 'width', 0.01);
%                     visColorDensityBar(xi,f/max(f),t-0.5,1);
%                     colormap(flipud(bone))
%                 end
%                 plot3(1:T, squeeze(data.thetasequence(m,n,1:T)), 1.1*ones(size(1:T)), 'r-');
%                 ylim([min(data.A(m,n,:))-0.3 max(data.A(m,n,:))+0.3]);



