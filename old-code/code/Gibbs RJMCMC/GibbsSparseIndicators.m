function [ B ] = GibbsSparseIndicators( X, A, B, invCu )
%GIBBSSPARSEINDICATORS Run a Gibbs step to sample the sparse indicators

[d,N] = size(X);

prior_on = 0.5;

Bvec = B';
Bvec = Bvec(:);

XCX = zeros(d^2,d^2);
XCx = zeros(d^2,1);
for t=2:N
    tmp = cell(d,1);
    for ii = 1:d
        tmp{ii} = (A(ii,:)'.*X(:,t-1))';
    end
    Xmat = blkdiag(tmp{:});
    XCX = XCX + Xmat'*invCu*Xmat;
    XCx = XCx + Xmat'*invCu*X(:,t);
end

order = randperm(d^2);

for ii = order
    
    p(1) = log(prior_on) -0.5*( 2*XCX(ii,:)*[Bvec(1:ii-1); 0.5; Bvec(ii+1:end)] - 2*XCx(ii) );
    p(2) = log(1-prior_on);
    
    p = p - max(p);
    p = p - logsumexp(p');
    
    Bvec(ii) = (log(rand)<p(1));
    
end

B = reshape( Bvec', d, d)';

end

