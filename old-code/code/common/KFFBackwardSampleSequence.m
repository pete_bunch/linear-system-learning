function X = KFFBackwardSampleSequence( Y, Aseq, Tau, B, Cu, Cv, X0mu, X0cov )
%KFFBACKWARDSAMPLE Summary of this function goes here
%   Detailed explanation goes here

% CONVENTION: Priors are one time step before first observation

T = size(Y,2);
[XhatKK, CovhatKK, ~] = KFSequence(Y, Aseq, [0 Tau T], B, Cu, Cv, X0mu, X0cov );
N = size(XhatKK,1);

X(:,T) = mvnrnd(XhatKK(:,T), max(CovhatKK(:,:,T), CovhatKK(:,:,T)'));

curA = size(Aseq,3);

for t=T-1:-1:1
    
    % A here is the A that goes from t to t+1
    % therefore, move on if (floor of) jump time strictly greater than t
    if(curA-1>0 && floor(Tau(curA-1))>t), curA = curA-1; end
    A = Aseq(:,:,curA);

    invVtt = CovhatKK(:,:,t)\eye(N);% inv(CovhatKK(:,:,t));
    
    InvSigmat = invVtt + A'/Cu*A;
    mut = InvSigmat \ (CovhatKK(:,:,t)\XhatKK(:,t) + A'/Cu*X(:,t+1));
    
    Sigmat = InvSigmat\eye(N); %inv(InvSigmat);
    Sigmat = (Sigmat+Sigmat')/2;
    
    X(:,t) = mvnrnd(mut, Sigmat);
    
end



end

