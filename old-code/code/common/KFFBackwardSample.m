function [X, mut, Sigmat] = KFFBackwardSample( Y, A, B, Cu, Cv, X0mu, X0cov, PriorAtFirstObservation )
%KFFBACKWARDSAMPLE Summary of this function goes here
%  See KF.m for meaning of parameters

T = size(Y,2);
[XhatKK, CovhatKK, ~] = KF(Y, A, B, Cu, Cv, X0mu, X0cov, PriorAtFirstObservation );

X(:,T) = mvnrnd(XhatKK(:,T), max(CovhatKK(:,:,T), CovhatKK(:,:,T)'));
mut(:,T) = XhatKK(:,T);
Sigmat(:,:,T) = max(CovhatKK(:,:,T), CovhatKK(:,:,T)');

for t=T-1:-1:1
    
    InvSigmat = inv(CovhatKK(:,:,t)) + A'/Cu*A;
    mut(:,t) = InvSigmat \ (CovhatKK(:,:,t)\XhatKK(:,t) + A'/Cu*X(:,t+1));
    
    Sigmatt = inv(InvSigmat);
    Sigmat(:,:,t) = max(Sigmatt, Sigmatt');  % can go non-symmetric due to numerical errors

    X(:,t) = mvnrnd(mut(:,t), Sigmat(:,:,t));
end



end

