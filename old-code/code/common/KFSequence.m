function [ Xhat, Covhat, logLikelihood ] = KFSequence( Y, A, Tau, B, Cu, Cv, X0mu, X0cov)
%KFSEQUENCE Sequence of Kalman filters
% As KF but,
%   A is a sequence of matrices
%   Tau gives the times at which each of these matrices starts/stops applying
%       i.e. A(:,:,1) applies from Tau(1) to Tau(2)
%            A(:,:,2) applies from Tau(2) to Tau(3)
%
% CONVENTION: Jumps (Tau) between observations are considered to occur just
%    after the observation at floor(Tau(i))
% CONVENTION 2:  Priors are always considered to be one step before first
%    observation

% Split up the observations according to the tau

T = size(Y,2);

if(numel(Tau)>0 && Tau(end)>T )
    error('last Tau should not be beyond T');
elseif(numel(Tau)>0 && Tau(1)<0 )
    error('first Tau should not be below 0');
end

MuPrior = X0mu;
CovPrior = X0cov;

logLikelihood = 0;

Xhat = zeros(0,0);
Covhat = zeros(0,0,0);

for i=1:numel(Tau)-1
    YY = Y(:, floor(Tau(i))+1:floor(Tau(i+1)) );
    
    [mus, covs, logp] = KF(YY,A(:,:,i),B,Cu,Cv,MuPrior,CovPrior,false);
    
    Xhat(:,end+1:end+size(mus,2)) = mus;
    Covhat(:,:,end+1:end+size(covs,3)) = covs;
    logLikelihood = logLikelihood + logp;
    
    MuPrior = mus(:,end);
    CovPrior = covs(:,:,end);
end




end

