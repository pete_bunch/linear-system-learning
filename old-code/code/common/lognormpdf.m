function logp = lognormpdf( x, mu, sigma )
%LOGNORMPDF Summary of this function goes here
%   Detailed explanation goes here
    twosigma2 = 2*sigma*sigma;
    logp = -0.5*log(pi*twosigma2) - ((x-mu).^2)/twosigma2;
    
    if(imag(logp)~=0)
        warning('Imaginary number generated.  Is this what you wanted?');
    end

end

