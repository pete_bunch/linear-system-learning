function [ Xhat, Covhat, logLikelihood ] = KF( Y, A, B, Cu, Cv, X0mu, X0cov, PriorAtFirstObservation)
%KF Kalman filter for system X_t = A X_t-1 + u,  Y_t = X_t + v
% INPUT:
%  Y is observations s.t. Y(:,t) is the set of observations at time t
%  A is the state transition matrix
%  B is the observation matrix  (so that Y_t = B * X_t)
%  Cu is the state transition noise covariance
%  Cv is the observation noise covariance
%  X0mu is the prior mean for the initial state
%  X0cov is the prior covariance for the initial state
%     note: the priors give the predicted distribution of the state at the
%           first observation time
%  PriorAtFirstObservation is a boolean that indicates whether the supplied
%    prior is at same time as first observation or whether it is one step
%    before the first observation, requiring a preceding predict step.

% OUTPUT:
%  Xhat is the filtered states
%  Covhat is the covariance matrices  Cov(X_t | Y_1:t)
%  LogLikelihood is the log likelihood from the p.e.d.


addpath(genpath('C:\Users\jm362\Documents\MATLAB\useful\'), '-begin');


N = size(A,1);
T = size(Y,2);

% state and observation transition noise covariance
covStNoise = Cu;
covObsNoise = Cv;

% set log likelihood to 0
logp = 0;

% zero estimate storage
Xhat = zeros(N,T);
Covhat = zeros(N,N,T);

% Set X and Cov predictions to the priors 
if(PriorAtFirstObservation)
    Xpredict = X0mu;
    Covpredict = X0cov;
else
    Xpredict = A * X0mu;
    Covpredict = A*X0cov*A' + covStNoise;
end


% if(visualize)
%     clf;
%     hold on;
% end

for t=1:T

    % predicition error decomposition
    logp = logp + logmvnpdf(Y(:,t)', (B*Xpredict)', B*Covpredict*B' + covObsNoise);
    
    % correct
    yt = Y(:,t) - B*Xpredict;
    St = B*Covpredict*B' + covObsNoise;
    Kt = Covpredict*B'/St;
    Xcor = Xpredict + Kt*yt;
    Covcor = (eye(N) - Kt*B)*Covpredict;
    
    % store the filtered states
    Xhat(:,t) = Xcor;
    Covhat(:,:,t) = Covcor;
    
%     if(visualize)
%         % plot them
%         plot(Y(1:2:end,t), Y(2:2:end,t), 'g.');
%         plot(Xhat(xp,t), Xhat(yp,t), 'b.');
%         plot(Xin(1,1,t), Xin(1,2,t), 'r.');
%         error_ellipse(Covcor([1 3],[1 3])*10, [Xhat(xp,t) Xhat(yp,t)]);
%     end    
    
    % predict
    Xpredict = A * Xcor;
    Covpredict = A*Covcor*A' + covStNoise;

end

logLikelihood = logp;

end

