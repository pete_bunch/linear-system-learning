function [ XhatKT, CovhatKT, CrossCovKT, XhatKK, CovhatKK, logLikelihood ] = KS( Y, A, B, Cu, Cv, X0mu, X0cov, PriorAtFirstObservation )
%KS Kalman Smoother (RTS) for X_t = A X_t-1 + u,  Y_t = X_t + v
%   Detailed explanation goes here

if(sum(sum(B-eye(size(B)))) > 0)
    error('Cannot cope with non-identity B matrix yet');
end

% RTS Kalman smoother

[XhatKK, CovhatKK, logLikelihood] = KF(Y,A,B, Cu, Cv, X0mu, X0cov, PriorAtFirstObservation );


% Number of observation times
T = size(Y, 2);

CovhatKT = zeros(size(CovhatKK));
XhatKT   = zeros(size(XhatKK));

% Xhat_K|T = Xhat_K|K for K=T, so we can start the back pass here
XhatKT(:,T) = XhatKK(:,T);
CovhatKT(:,:,T) = CovhatKK(:,:,T);

if(T==1)
    CrossCovKT=[];
    return;
end


% Cross covariance, between X_t-1 and X_t, given observations to T (needed for EM algorithm)
% from Shumway + Stoffer 1982
CovhatTTm1 = A*CovhatKK(:,:,T-1)*A' + Cu;
KT = CovhatTTm1*B'/(B*CovhatTTm1*B'+Cv); 
CrossCovKT(:,:,T) = (eye(size(KT*B))-KT*B)*A*CovhatKK(:,:,T-1);


% backward pass over the observations
CovhatKp1K = A*CovhatKK(:,:,T-1)*A' + Cu;
Ak = CovhatKK(:,:,T-1)*A'/CovhatKp1K;


for k = T-1:-1:1
    XhatKT(:,k) = XhatKK(:,k) + Ak*(XhatKT(:,k+1)-A*XhatKK(:,k));
    CovhatKp1K = A*CovhatKK(:,:,k)*A' + Cu;
    CovhatKT(:,:,k) = CovhatKK(:,:,k) + Ak*(CovhatKT(:,:,k+1)-CovhatKp1K)*Ak';
    
    if(k>1)
        CovhatKKm1 = A*CovhatKK(:,:,k-1)*A' + Cu;
        Akm1 = CovhatKK(:,:,k-1)*A'/CovhatKKm1;
        CrossCovKT(:,:,k) = CovhatKK(:,:,k)*Akm1' + Ak*(CrossCovKT(:,:,k+1) - A*CovhatKK(:,:,k))*Akm1';
        Ak = Akm1;
    end
    
end



end

