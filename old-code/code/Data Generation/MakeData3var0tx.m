function data = MakeDataNoTransitions1( T )
%MAKEDATANOTRANSITIONS Summary of this function goes here
%   Detailed explanation goes here

A(:,:,1) = [0.8   0    0  ;...   
             0   0.9   0  ;... 
            0.6   0   0.9];

Tau = [];

data = GenerateData(A,Tau,T);


end

