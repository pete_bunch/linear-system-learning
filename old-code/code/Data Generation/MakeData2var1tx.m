function data = MakeData2var1tx( T )
%MAKEDATA2VAR1TX Summary of this function goes here
%   Detailed explanation goes here

Tau = ceil(T/2);
A(:,:,1) = 0.1*eye(2) + [0 0.99; 0 0.7]; 
A(:,:,2) = [0.99 0.1; 0 0.99];

data = GenerateData(A,Tau,T);



end

