function data = MakeData2var1tx( T )
%MAKEDATA2VAR1TX Summary of this function goes here
%   Detailed explanation goes here

Tau = [ceil(T/3) ceil(2*T/3)];
A(:,:,1) = 0.1*eye(2) + [0 0.99; 0 0.7]; 
A(:,:,2) = [0.99 0.1; 0 0.99];
A(:,:,3) = 0.1*eye(2) + [0 0.99; 0 0.7]; 

data = GenerateData(A,Tau,T);



end

