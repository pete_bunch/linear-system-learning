function data = GenerateData( A, Tau, T )
%GENERATEDATA Summary of this function goes here
%   Detailed explanation goes here

N = size(A,2);


data.Cv = 0.1*eye(N);
data.Cu = 1*eye(N);
data.B  = eye(N);

data.x(:,1) = zeros(N,1);
data.y(:,1) = data.B*data.x(:,1) + mvnrnd(zeros(1,N), data.Cv)';

curA = 1;
data.A(:,:,curA) = A(:,:,curA);
data.Tau = [];

data.thetasequence(:,:,1) = data.A(:,:,1);

for t=2:T

    if(numel(Tau)>0 && curA<=numel(Tau) && t >= Tau(curA))
        data.Tau(curA) = Tau(curA);
        curA=curA+1;
        data.A(:,:,curA) = A(:,:,curA);
    end
    
    data.thetasequence(:,:,t) = data.A(:,:,curA);
    
    data.x(:,t) = data.A(:,:,curA)*data.x(:,t-1) + mvnrnd(zeros(N,1), data.Cu)';
    data.y(:,t) = data.B*data.x(:,t) + mvnrnd(zeros(size(data.B,1),1),data.Cv)';
    
end

clf;
hold on;
plot(data.y','g-');
plot(data.x','r-');

end

