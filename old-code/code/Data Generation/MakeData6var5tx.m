function data = MakeData6var5tx( T )
%MAKEDATA2VAR1TX Summary of this function goes here
%   Detailed explanation goes here

Tau = [ceil(T/6) ceil(2*T/6) ceil(3*T/6) ceil(4*T/6) ceil(5*T/6)];
A(:,:,1) = RandomTransitionMatrix(6);
A(:,:,2) = RandomTransitionMatrix(6);
A(:,:,3) = RandomTransitionMatrix(6);
A(:,:,4) = RandomTransitionMatrix(6);
A(:,:,5) = RandomTransitionMatrix(6);
A(:,:,6) = RandomTransitionMatrix(6);

data = GenerateData(A,Tau,T);

end

function A = RandomTransitionMatrix(N)

A = randn(6);
A = (A+A')/2;
[V, D] = eig(A);
D = abs(D);
D(D>1) = rand(sum(diag(D)>1),1);
A = V*D*V';
A = round(A*10)/10;

end