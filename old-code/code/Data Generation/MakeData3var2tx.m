function data = MakeData3var2tx( T )
%MAKEDATA2VAR1TX Summary of this function goes here
%   Detailed explanation goes here

Tau = [ceil(T/3) ceil(2*T/3)];
A(:,:,1) = [0.8 0 0.1; 0.4 0.2 0.3; 0.1 0.2 0.8]; 
A(:,:,2) = [0.99 0 0; 0.0 0.99 0.0; 0.0 0.99 0.0]; 
A(:,:,3) = [0.1 0.5 0; 0.4 0.4 0.0; 0.3 0.0 0.6]; 

data = GenerateData(A,Tau,T);



end

