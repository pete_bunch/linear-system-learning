function data = MakeData( T, N, M )
%MAKEDATA2VAR1TX Summary of this function goes here
%   Detailed explanation goes here

Tau = ceil( T*[1:M]/(M+1) );
for ii = 1:M+1
    A(:,:,ii) = RandomTransitionMatrix(N);
end

A = [0.95 0.95; 0 1]

data = GenerateData(A,Tau,T);

end

function A = RandomTransitionMatrix(N)

A = randn(N);
A = (A+A')/2;
[V, D] = eig(A);
D(abs(D)>1) = 2*rand(sum(abs(diag(D))>1),1)-1;
A = V*D*V';
% B = binornd(1, 0.5, [N N]);
B = ones(N);

A = round(B.*A*10)/10;

while any(abs(eig(A))>1)
    [V, D] = eig(A);
    D(abs(D)>1) = 2*rand(sum(abs(diag(D))>1),1)-1;
    A = B.*(V*D*V');
    A = round(A*10)/10;
end

end