import matplotlib.pyplot as plt
import numpy as np
import pendulum_models_sampling as sysl
from scipy import linalg as la
from scipy import stats
from scipy.stats import multivariate_normal as mvn
import statsmodels.tsa.stattools as st
from linear_models import LinearModel, SparseLinearModel, DegenerateLinearModel

class LinearModelMCMC:
    """Container Class for Linear-Gaussian Model Learning with MCMC"""

    def __init__(self, model_type, init_model_est, observ, A_prior_vr=None, B_prior_prob=None, Q_prior_scale=None, Q_prior_dof=None, giv_std=None, pad_std=None, rot_std=None):
        self.model_type = model_type
        
        self.model = init_model_est.copy()
        self.observ = observ

        self.chain = []
        self.chain.append(self.model.copy())        
        
        self.A_prior_vr = A_prior_vr
        self.B_prior_prob = B_prior_prob
        self.Q_prior_scale = Q_prior_scale # This is the scalar p such that the Q^{-1} is Wishart with mean nu*p*I (i.e. phi in the paper)
        self.Q_prior_dof = Q_prior_dof
        
        if model_type==2:
            
#            worst_rank = int(self.model.ds/2)
#            most_givens = worst_rank*self.model.ds-worst_rank
            
#            self.giv_std = [giv_std]*(most_givens)
            self.giv_std = giv_std
            self.pad_std = pad_std
            self.rot_std = rot_std
            
            self.F_acc = []
            self.pad_std_chain = []
            
            self.rot_acc = []
            self.rot_std_chain = []
            
            self.givens_acc = []
            self.giv_std_chain = []
#            self.givens_acc = [[] for gg in range(most_givens)]
#            self.giv_std_chain = [[] for gg in range(most_givens)]
#            for gg in range(most_givens):
#                self.giv_std_chain[gg].append(self.giv_std[gg])
                
            self.rank_acc = []
            
        
    def save_link(self):
        """Save the current state of the model as a link in the chain"""
        self.chain.append(self.model.copy())
        if self.model_type==2:
            self.pad_std_chain.append(self.pad_std)
            self.rot_std_chain.append(self.rot_std)
            self.giv_std_chain.append(self.giv_std)
#            for gg in range(len(self.model.givens)):
#                self.giv_std_chain[gg].append(self.giv_std[gg])
                
    def adapt_std(self, B):
        """Adapt proposal distribution parameters"""
        ar = np.mean(np.array(self.F_acc[-B:]))
        bn = len(self.F_acc)/B
        delta = min(0.1,1/np.sqrt(bn))
        pad_min = 1E-6
        if (ar<0.44):
            self.pad_std = np.maximum(pad_min, self.pad_std*np.exp(-delta))
        else:
            self.pad_std = np.maximum(pad_min, self.pad_std*np.exp(delta))
            
        ar = np.mean(np.array(self.rot_acc[-B:]))
        bn = len(self.rot_acc)/B
        delta = min(0.1,1/np.sqrt(bn))
        rot_min = 1E-4
        if (ar<0.44):
            self.rot_std = np.maximum(rot_min, self.rot_std*np.exp(-delta))
        else:
            self.rot_std = np.maximum(rot_min, self.rot_std*np.exp(delta))
        
        ar = np.mean(np.array(self.givens_acc[-B:]))
        bn = len(self.givens_acc)/B
        delta = min(0.1,1/np.sqrt(bn))
        if (ar<0.44):
            self.giv_std = self.giv_std*np.exp(-delta)
        else:
            self.giv_std = self.giv_std*np.exp(delta)
#        
#        for gg in range(len(self.model.givens)):
#            ar = np.mean(np.array(self.givens_acc[gg][-B:]))
#            bn = len(self.givens_acc[gg])/B
#            delta = min(0.1,1/np.sqrt(bn))
#            if (ar<0.44):
#                self.giv_std[gg] = self.giv_std[gg]*np.exp(-delta)
#            else:
#                self.giv_std[gg] = self.giv_std[gg]*np.exp(delta)
    
    ### MCMC moves ###
    
    def iterate_transition_matrix(self, Mtimes):
        """Run Gibbs iterations on transition matrix"""
        
        if (self.A_prior_vr is None):
            raise ValueError("A transition matrix prior must be supplied in order to sample")
        if (self.model_type==1) and (self.B_prior_prob is None):
            raise ValueError("A mask prior must be supplied in order to sample")
        
        ds = self.model.ds
        
        for mm in range(Mtimes):
        
            # First sample the state sequence
            x = self.model.sample_posterior(self.observ)
            
            if self.model_type == 0:
            
                # Sample a new transition matrix
                self.model.F,_ = sysl.sample_transition_matrix_conditional(x, self.model.Q, self.A_prior_vr)
                
            elif self.model_type == 1:
                
                # Sample a new transition matrix (all elements jointly)
                self.model.A,_ = sysl.sample_transition_matrix_conditional(x, self.model.Q, self.A_prior_vr, self.model.B)
                
#                # Sample binary mask
#                self.model.B = sysl.sample_transition_matrix_mask_conditional(x, self.model.A, self.model.B, self.model.Q, self.B_prior_prob)
            
                # Sample transition matrix with binary mask (A,B pairs jointly)
                self.model.A,self.model.B = sysl.sample_transition_matrix_and_mask_conditional(x, self.model.A, self.model.B, self.model.Q, self.A_prior_vr, self.B_prior_prob)
                
            elif self.model_type == 2:
                
                # Keep a copy of the current F
                Fold = self.model.F.copy()
                
#                try:
                
                # KF to get the likelihood
                flt,_,old_lhood = self.model.kalman_filter(self.observ)
                
                # Sample a state trajectory
                x = self.model.backward_simulation(flt)
                
                # Pad the degenerate covariance matrix
                padding = (self.pad_std**2)*np.identity(ds)
                Qpad = self.model.Q + padding
                
                # Sample a new transition matrix
                Fppsl,F_fwd_ppsl_prob = sysl.sample_transition_matrix_conditional(x, Qpad, self.A_prior_vr)
                self.model.F = Fppsl
                
                # New likelihood
                ppsl_flt,_,new_lhood = self.model.kalman_filter(self.observ)            
                
                # Sample a new state sequence
                xppsl = self.model.backward_simulation(ppsl_flt)
                
                # Reverse proposal
                _,F_bwd_ppsl_prob = sysl.sample_transition_matrix_conditional(xppsl, Qpad, self.A_prior_vr, A=Fold)
                
                # Priors
                new_prior = mvn.logpdf( Fppsl.flatten(), np.zeros((ds**2)), self.A_prior_vr*np.identity(ds**2) )
                old_prior = mvn.logpdf( Fold.flatten(),  np.zeros((ds**2)), self.A_prior_vr*np.identity(ds**2) )
                
                # Accept?
                ap = (new_lhood+new_prior) - (old_lhood+old_prior) + (F_bwd_ppsl_prob-F_fwd_ppsl_prob)
#                print(ap)
#                except:
#                    
#                    # If move fails (which very occasonally happens due to very nearly singular covariances) then just leave unchanged
#                    ap = -np.inf                

                if np.log(np.random.random()) < ap:
                    self.F_acc.append(1)
                else:
                    self.model.F = Fold
                    self.F_acc.append(0)
                
                # Sample within subspace
                if sum(self.F_acc)>1:
                    x = self.model.sample_posterior(self.observ)
                    F = sysl.sample_degenerate_transition_matrix_conditional(x, self.model.F, np.dot(self.model.D,self.model.D.T), self.model.U, self.A_prior_vr)
                    self.model.F = F
                
            else:
                raise ValueError("Invalid model type")
    
    def iterate_transition_noise_matrix(self, Mtimes):
        """Run Gibbs iterations on the transition noise matrix"""
        
        if ((self.Q_prior_dof==None) or (self.Q_prior_scale==None)):
            raise ValueError("A transition covariance matrix prior must be supplied in order to sample")
            
        for mm in range(Mtimes):
            
            if (self.model_type==0) or (self.model_type==1):
                
                # First sample the state sequence
                x = self.model.sample_posterior(self.observ)
                
                # Sample a new covariance matrix
                Q,_ = sysl.sample_transition_covariance_conditional(x, self.model.F, self.Q_prior_dof, self.Q_prior_scale)
                
                # Square root it
                G = la.cholesky(Q,lower=True)
                self.model.G = G
                
            elif self.model_type==2:

                # Joint move
                
                # Old likelihood
                flt,_,lhood = self.model.kalman_filter(self.observ)
                
                if self.model.rank < self.model.ds:
                    
                    # Make a proposal copy of the model
                    ppsl_model = self.model.copy()
                    
                    # Sample a Cayley-distributed random matrix
                    M = sysl.sample_cayley(ppsl_model.ds, self.rot_std)
                    
                    # Use it to rotate noise matrix
                    ppsl_model.rotate_noise(M)
                    
                    # Likelihood
                    new_flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
                    
                    # Accept probability
                    ap = new_lhood-lhood
#                    print(ap)
                    
                    # Accept/Reject
                    if np.log(np.random.random()) < ap:
                        self.model = ppsl_model
                        self.rot_acc.append(1)
                        flt = new_flt
                        lhood = new_lhood
                    else:
                        self.rot_acc.append(0)
                    
                    # How many planar moves should we do?
                    num_givens_moves = ppsl_model.ds
                    
                    # Keep track of givens acceptances
                    giv_acc = np.zeros(num_givens_moves)
                    
                    # Planar moves
                    for gg in range(num_givens_moves):
                        
                        # Make a proposal copy of the model
                        ppsl_model = self.model.copy()
                        
                        # Random plane
                        ii = np.random.random_integers(ppsl_model.ds)-1
                        jj = ii
                        while jj == ii:
                            jj = np.random.random_integers(ppsl_model.ds)-1
                            
                        # Random rotation
                        rot = np.random.normal(0,self.giv_std)
                        ppsl_model.planar_rotate_noise(ii, jj, rot)
                        
                        # Likelihood
                        new_flt,_,new_lhood = ppsl_model.kalman_filter(self.observ)
                    
                        # Accept probability
                        ap = new_lhood-lhood
#                        print(ap)
                    
                        # Accept/Reject
                        if np.log(np.random.random()) < ap:
                            self.model = ppsl_model
#                            self.givens_acc.append(1)
                            giv_acc[gg] = 1
                            flt = new_flt
                            lhood = new_lhood
                        else:
#                            self.givens_acc.append(0)
                            giv_acc[gg] = 0
                        
                    self.givens_acc.append(np.mean(giv_acc))
                
#                # Prior givens probability
#                old_givens_prior = self.model.givens_prior()
#                
#                # First sample the state sequence
#                flt,_,old_lhood = self.model.kalman_filter(self.observ)
#                x = self.model.backward_simulation(flt)
#                
#                if self.model.rank < self.model.ds:
#                    
#                    # Keep track of givens acceptances
#                    giv_acc = np.zeros(len(self.model.givens))
#                    
#                    for gg in range(len(self.model.givens)):
#                        
#                        # Store current
#                        #old_ga = self.model.givens[gg]
#                        ppsl_model = self.model.copy()
#                        
##                        print(self.model.order)
##                        print(ppsl_model.order)
#                        
#                        # Proposal standard deviation
##                        gstd = self.giv_std[gg]
#                        gstd = self.giv_std
#                        
#                        # Propose a random walk change and correct if it wraps
#                        new_ga = ppsl_model.givens[gg]+np.random.normal(0,gstd)
#                        wrap = ppsl_model.set_givens(gg, new_ga)
#                        
#                        # Probabilities
#                        _,_,new_lhood = ppsl_model.kalman_filter(self.observ)
#                        new_givens_prior = ppsl_model.givens_prior()
#                        
#                        ap = (new_lhood+new_givens_prior)-(old_lhood+old_givens_prior)
##                        print(ap)
#                        
#                        # Accept/Reject
#                        if np.log(np.random.random()) < ap:
#                            self.model = ppsl_model
##                            self.givens_acc[gg].append(1)
#                            giv_acc[gg] = 1
#                            old_lhood = new_lhood
#                            pass
#                        else:
##                            self.givens_acc[gg].append(0)
#                            giv_acc[gg] = 0
#                            
#                    self.givens_acc.append(np.mean(giv_acc))
                    
                # Sample a new covariance matrix
                x = self.model.backward_simulation(flt)
                self.model.D = sysl.sample_degenerate_transition_noise_matrix_conditional(x, self.model.F, self.model.rank, self.model.U, self.Q_prior_dof, self.Q_prior_scale)

    def iterate_transition_noise_matrix_rank(self, Mtimes):
        """Run RJ-MCMC iterations to change the transition noise matrix rank"""
        
        if not(type(self.model)==DegenerateLinearModel):
            raise TypeError("Q is full rank. Cannot do this sort of move.")
        
        if ((self.Q_prior_dof==None) or (self.Q_prior_scale==None)):
            raise ValueError("A transition covariance matrix prior must be supplied in order to sample")
            
        for mm in range(Mtimes):
            
            # Likelihood
            flt,_,old_lhood = self.model.kalman_filter(self.observ)
            
            # Copy model
            ppsl_model = self.model.copy()
            
            # Add or remove?
            if ppsl_model.ds==1:
                u = -1                          # 1D model. Cannot do anything
            elif ppsl_model.rank==1:
                u = 0
            elif ppsl_model.rank==ppsl_model.ds:
                u = 1
            else:
                u = int(np.round(np.random.random()))
            
            if u==0:
                
                # Increase rank
                dcf = ppsl_model.increase_rank(None, None, self.Q_prior_scale)
                
            elif u==1:
                # Decrease rank
                dcf,val,vec = ppsl_model.reduce_rank(self.Q_prior_scale)
            
            # Likelihood
            _,_,new_lhood = ppsl_model.kalman_filter(self.observ)
            
            ap = (new_lhood-old_lhood) + dcf
            
            # Accept/Reject
            if np.log(np.random.random()) < ap:
                self.model = ppsl_model
                self.rank_acc.append(1)
#                old_lhood = new_lhood
            else:
                self.rank_acc.append(0)
                
    def iterate_observation_variance(self, Mtimes):
        """Gibbs sample the scale on the idendity observation covariance"""
        
        x = self.model.sample_posterior(self.observ)
        R = sysl.sample_observation_covariance_conditional(x, self.observ, self.model.H, 0.0001, 0.0001)
        self.model.R = R
                
            

            
### DISPLAY ###
def draw_chain_histogram(chain, true_model, burn_in):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    figs = []
    
    if len(chain)>burn_in:
        # F
        f,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
        f.subplots_adjust(wspace=0.3, hspace=0.3)
        figs.append(f)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].locator_params(nbins=2)
                ax[rr,cc].set_ylim((0,len(chain[burn_in:])))
                ax[rr,cc].hist([mm.F[rr,cc] for mm in chain[burn_in:]], color='0.5')
#                ax[rr,cc].plot([true_model.F[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
#                ax[rr,cc].set_xlim((-2,2))
                
        # Q
        f,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
        f.subplots_adjust(wspace=0.3, hspace=0.3)
        figs.append(f)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].locator_params(nbins=2)
                ax[rr,cc].set_ylim((0,len(chain[burn_in:])))
                ax[rr,cc].hist([mm.Q[rr,cc] for mm in chain[burn_in:]], color='0.5')
#                ax[rr,cc].plot([true_model.Q[rr,cc]]*2, [0,len(chain[burn_in:])], '-r')
#                ax[rr,cc].set_xlim((-2,2))
        
        # |Q|
        f,ax = plt.subplots(1,1)
        figs.append(f)
        ax.locator_params(nbins=2)
        ax.set_ylim((0,len(chain[burn_in:])))
        ax.hist([la.det(mm.Q) for mm in chain[burn_in:]], color='0.5')
#        ax.plot([la.det(true_model.Q)]*2, [0,len(chain[burn_in:])], '-r')
        
        # B
        f,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
        f.subplots_adjust(wspace=0.3, hspace=0.3)
        figs.append(f)
        for rr in range(d):
            for cc in range(d):
                ax[rr,cc].locator_params(nbins=2)
                ax[rr,cc].set_ylim((0,len(chain[burn_in:])))
                ax[rr,cc].hist([mm.B[rr,cc] for mm in chain[burn_in:]],bins=[-0.1,0.1,0.9,1.1], color='0.5')
                ax[rr,cc].set_xlim((-0.1,1.1))
#                ax[rr,cc].plot([true_model.B[rr,cc]]*2, [0,1.2*len(chain[burn_in:])], '-r')
    
    return figs


def draw_chain(chain, true_model, burn_in, fields):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        
        for ff in fields:
            if ff in ['F','Q','G','D']:
                d = getattr(true_model, ff).shape[0]
                d = min(4,d)
                fig,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
                fig.subplots_adjust(wspace=0.3, hspace=0.3)
                for rr in range(d):
                    for cc in range( getattr(chain[-1],ff).shape[1] ):
                        ax[rr,cc].locator_params(nbins=2)
#                        ax[rr,cc].set_xlim((0,len(chain[1:])))
                        ax[rr,cc].plot([getattr(mm,ff)[rr,cc] for mm in chain[1:]], 'k')
#                        ax[rr,cc].plot([1,len(chain)], [getattr(true_model,ff)[rr,cc]]*2, '-r')
            elif ff in ['givens']:
                d = len(getattr(true_model,ff))
                fig = plt.figure()
                for dd in range(d):
                    ax = fig.add_subplot(d,1,dd+1)
                    ax.plot([getattr(mm,ff)[dd] for mm in chain[1:]])
#                    ax.plot([1,len(chain)], [getattr(true_model,ff)[dd]]*2, 'r')
                    
    return fig
    

def draw_chain_autocorrelation(chain, true_model, burn_in, fields, nlags=30):
    d = min(4,true_model.ds) # Don't plot too many components - too small
    
    if len(chain)>burn_in:
        
        for ff in fields:
            if ff in ['F','Q','G','D']:
                d = getattr(true_model, ff).shape[0]
                d = min(4,d)
                fig,ax = plt.subplots(d,d,squeeze=False,figsize=(9,9))
                fig.subplots_adjust(wspace=0.3, hspace=0.3)
                for rr in range(d):
                    for cc in range( getattr(chain[-1],ff).shape[1] ):
                        seq = [getattr(mm,ff)[rr,cc] for mm in chain[burn_in:]]
                        ac = st.acf(seq,unbiased=False,nlags=nlags,)
                        ax[rr,cc].locator_params(nbins=2)
                        ax[rr,cc].set_xlim((0,nlags))
                        ax[rr,cc].set_ylim((-0.1,1))
                        ax[rr,cc].plot(range(len(ac)),ac,'k')
                        ax[rr,cc].plot(range(len(ac)),[0]*len(ac),':k')
            elif ff in ['givens']:
                d = len(getattr(true_model,ff))
                fig = plt.figure()
                for dd in range(d):
                    ax = fig.add_subplot(d,1,dd+1)
                    seq = [getattr(mm,ff)[dd] for mm in chain[burn_in:]]
                    ac = st.acf(seq,unbiased=False,nlags=nlags,)
                    ax.plot(range(len(ac)),ac,'k')
                    ax.plot(range(len(ac)),[0]*len(ac),':k')
                    ax.set_ylim([-0.1,1])
                    
    return fig


def draw_chain_acceptance(chain_acc, burn_in):
    d = len(chain_acc)
    ax = plt.figure().add_subplot(1,1,1)
    maxval = 0
    for dd in range(d):
        acc_array = np.array(chain_acc[dd])
        ax.plot(np.cumsum(acc_array))
        maxval = max(maxval,acc_array.sum())
    ax.plot([burn_in]*2,[0,maxval])


def draw_chain_adaptation(chain_adpt, burn_in):
    d = len(chain_adpt)
    ax = plt.figure().add_subplot(1,1,1)
    maxval = 0
    for dd in range(d):
        adpt_array = np.array(chain_adpt[dd])
        ax.plot(adpt_array)
        maxval = max(maxval,adpt_array.max())
    ax.plot([burn_in]*2,[0,maxval])

     