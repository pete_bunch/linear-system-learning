# Standard modules
import numpy as np
import matplotlib.pyplot as plt
import sys

# Add to path
sys.path.append( '../code/' )

# My modules
from linear_models import *
from pendulum_models_mcmc import *
from kalman import GaussianDensity, Series

# Close all windows
plt.close("all")

# Set random seed
np.random.seed(1)

### SPECIFY A LINEAR MODEL ###
model_type = 1      # 0=basic, 1=sparse, 2=degenerate

# Import data
observ = np.genfromtxt('../oscillator-data/processed-data.csv', delimiter=',')
K = len(observ)
d = 2

# Observation matrices
#H = np.array([[1,0,1],[0,1,0]])
H = np.identity(d)
#R = 0.001*np.identity(d)
R = np.diag([0.01,0.0001])

#A = np.array([[0.97,-0.2,0],[0.2,0.97,0],[0,0,0.99]])
#B = np.ceil(np.abs(A))
#G = np.diag([0.001,0.001,0.1])
#P0 = GaussianDensity(np.array([1,0,1]),np.array([[0.1,0,0],[0,0.1,0],[0,0,0.1]]))
#model = SparseLinearModel(P0, A, B, G, H, R)
#K = 200
#state,observ = model.simulate_data(K)
## Plot state components
#f = plt.figure()
#Series.plot_sequence(state, range(d), f, 'k')
#f = plt.figure()
#Series.plot_sequence(observ, range(2), f, 'r')

# Initial state prior 
m0 = np.zeros(d)
P0 = 2*np.identity(d)

### Model priors ###
A_prior_vr = 1
B_prior_prob = 0.5 * np.ones((d,d))# np.array([[1,1,0],[1,1,0],[0,0,1]])
Q_prior_scale = 1E6
Q_prior_dof = d-1

### Algorithm parameters ###
num_iter = 600
num_burn_in = 100 #int(num_iter/2)

### Initial Model Estimate ###
A_est = 0.9*np.identity(d)
B_est = np.ones((d,d))
G_est = 10*np.identity(d)

###############################################################################

# Create model objects
first_state_prior = GaussianDensity(m0,P0)
init_model_est = SparseLinearModel(first_state_prior, A_est, B_est, G_est, H, R)

# Create MCMC container object
brain = LinearModelMCMC(model_type, init_model_est, observ,
                        A_prior_vr=A_prior_vr, B_prior_prob=B_prior_prob,
                        Q_prior_scale=Q_prior_scale, Q_prior_dof=Q_prior_dof)
                        
flt,prd,lhood = brain.model.kalman_filter(observ)
smt = brain.model.rts_smoother(flt,prd)
fig = plt.figure()
GaussianDensity.plot_sequence(smt, range(d), fig, 'b')

# Gibbs sample the transition matrix
for ii in range(num_iter):

    print("")
    print("Iteration number %u" % (ii))
    
    # MCMC moves
    brain.iterate_transition_matrix(1)
    brain.iterate_transition_noise_matrix(1)
    brain.iterate_observation_variance(1)
    
    # Save current state of the chain
    brain.save_link()
    
    # Print current state
    print("")
    print("Transition matrix:")
    print(brain.model.F)
    print("Transition matrix eigenvalue magnitudes:")
    print(abs(np.linalg.eig(brain.model.F)[0]))
    print("Transition variance:")
    print(brain.model.Q)
    print("Transition variance eigenvalues:")
    print(np.linalg.eigh(brain.model.Q)[0])
    print("Observation variance:")
    print(brain.model.R)

model = init_model_est.copy()
model.B = np.array([[1,1],[0,1]])
model.A = np.mean(np.array([mm.A for mm in brain.chain if np.all(mm.B==np.array([[1,1],[0,1]]))] ),axis=0)
Q = np.mean(np.array([mm.Q for mm in brain.chain if np.all(mm.B==np.array([[1,1],[0,1]]))] ),axis=0)
model.G = la.cholesky(Q, lower=True)
model.R = np.mean(np.array([mm.R for mm in brain.chain if np.all(mm.B==np.array([[1,1],[0,1]]))] ),axis=0)

flt,prd,lhood = model.kalman_filter(observ)
smt = model.rts_smoother(flt,prd)
f_data = plt.figure()
GaussianDensity.plot_sequence(smt, range(d), f_data, 'b')
Series.plot_sequence(observ, range(d), f_data, 'r.')

# Plot some MCMC output graphs
draw_chain(brain.chain, model, num_burn_in, ['F'])
draw_chain_autocorrelation(brain.chain, model, num_burn_in, ['F'], nlags=10)
figs = draw_chain_histogram(brain.chain, model, num_burn_in)

f_data.savefig('rotate_data.pdf', bbox_inches='tight')
figs[0].savefig('rotate_Fhist.pdf', bbox_inches='tight')
figs[3].savefig('rotate_Bhist.pdf', bbox_inches='tight')
    

